package tw.com.SurveyApp.UI;

import tw.com.SurveyApp.R;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.RadioButton;

public class RadioButtonCenter extends RadioButton {
	Drawable buttonDrawable;

	public RadioButtonCenter(Context context, AttributeSet attrs) {
		super(context, attrs);
		Resources res = this.getResources();
		buttonDrawable = res.getDrawable(R.drawable.radiobutton);

		setButtonDrawable(R.drawable.radiobuttont);
	}
	
	public void setChecked(boolean b){
		Resources res = this.getResources();
		if(b){
			buttonDrawable = res.getDrawable(R.drawable.radiobutton_press);
		}else{
			buttonDrawable = res.getDrawable(R.drawable.radiobutton);
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (buttonDrawable != null) {
			buttonDrawable.setState(getDrawableState());
			final int verticalGravity = getGravity()
					& Gravity.VERTICAL_GRAVITY_MASK;
			final int height = buttonDrawable.getIntrinsicHeight();

			int y = 0;

			switch (verticalGravity) {
			case Gravity.BOTTOM:
				y = getHeight() - height;
				break;
			case Gravity.CENTER_VERTICAL:
				y = (getHeight() - height) / 2;
				break;
			}

			int buttonWidth = buttonDrawable.getIntrinsicWidth();
			int buttonLeft = (getWidth() - buttonWidth) / 2;
			buttonDrawable.setBounds(buttonLeft, y, buttonLeft + buttonWidth, y
					+ height);
			buttonDrawable.draw(canvas);
		}
	}

}
