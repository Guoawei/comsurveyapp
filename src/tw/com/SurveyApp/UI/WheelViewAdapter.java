package tw.com.SurveyApp.UI;

import android.view.View;

/**
 * Wheel items adapter interface
 */
public interface WheelViewAdapter {
	/**
	 * Gets items count
	 * 
	 * @return the count of wheel items
	 */
	public int getItemsCount();

	/**
	 * Get a View that displays the data at the specified position in the data
	 * set
	 * 
	 * @param index
	 *            the item index
	 * @param cachedView
	 *            the old view to reuse if possible
	 * @return the wheel item View
	 */
	public View getItem(int index, View cachedView);

	/**
	 * Get a View that displays an empty part of wheel
	 * 
	 * @param cachedView
	 *            the old view to reuse if possible
	 * @return the empty wheel item View
	 */
	public View getEmptyItem(View cachedView);

	/**
	 * Get a View that displays an empty part of wheel
	 * 
	 * @param cachedView
	 *            the old view to reuse if possible
	 * @return the wheel item View
	 */
	public View getEmptyExtendedItem(View cachedView);

	/**
	 * Get a View that displays the wheel label
	 * 
	 * @param label
	 *            the wheel label
	 * @param cachedView
	 *            the old view to reuse if possible
	 * @return the wheel item View
	 */
	public View getLabelItem(String label, View cachedView);
}
