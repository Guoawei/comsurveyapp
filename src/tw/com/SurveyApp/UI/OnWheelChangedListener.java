package tw.com.SurveyApp.UI;

public interface OnWheelChangedListener {
    void onChanged(WheelView wheel, int oldValue, int newValue);
}
