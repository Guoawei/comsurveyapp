package tw.com.SurveyApp.service;

import tw.com.SurveyApp.dto.UpdataLogDto;
import android.content.ContentValues;
import android.content.Context;

public class UpdataLogService {

	/**
	 * 新增上傳記錄
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdInsert(UpdataLogDto dto, Context context) {
		ContentValues Data = new ContentValues();
		Data.put("SurveyGuid", dto.getSurveyGuid());
		Data.put("SampleGuid", dto.getSampleGuid());

		DBService.insertTableData("UpdataLog", Data, context);
	}

	/**
	 * 查詢是否上傳過
	 * 
	 * @param SurveyGuid
	 * @param SampleGuid
	 * @param context
	 * @return
	 */
	public static boolean cmdIsUpdate(String SurveyGuid, String SampleGuid,
			Context context) {
		StringBuilder selection = new StringBuilder();

		selection.append("SurveyGuid = ? and ");
		selection.append("SampleGuid = ?");

		String selectionArgs[] = { SurveyGuid, SampleGuid };

		int Count = DBService.getLength("UpdataLog", null,
				selection.toString(), selectionArgs, null, null, null, context);

		if (Count == 0) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 取得記錄檔樣本數
	 * 
	 * @param SurveyGuid
	 * @param context
	 * @return
	 */
	public static int getLength(String SurveyGuid, Context context) {
		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ?");

		String selectionArgs[] = { SurveyGuid };

		return DBService.getLength("UpdataLog", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 刪除上傳記錄
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 */
	public static void cmdDelete(String SurveyGuid, String SampleGuid,
			Context context) {

		StringBuilder whereClause = new StringBuilder();
		whereClause.append("SurveyGuid = ? and ");
		whereClause.append("SampleGuid = ?");

		String whereArgs[] = { SurveyGuid, SampleGuid };

		DBService.deleteTableData("UpdataLog", whereClause.toString(),
				whereArgs, context);
	}
}
