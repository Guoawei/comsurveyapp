package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.dto.InterviewDto;
import android.content.ContentValues;
import android.content.Context;

public class InterviewService {

	/**
	 * 新增訪問者資料
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdInsert(InterviewDto dto, Context context) {
		ContentValues Data = new ContentValues();
		Data.put("Guid", dto.getGuid());
		Data.put("Name", dto.getName());
		Data.put("Email", dto.getEmail());
		Data.put("Password", dto.getPassword());

		DBService.insertTableData("Interview", Data, context);
	}

	/**
	 * 查詢 訪員 Guid
	 * 
	 * @param context
	 * @return
	 */
	public static String getInterviewGuid(Context context) {
		String columns[] = { "Guid" };

		StringBuilder selection = new StringBuilder();
		selection.append("serial = ?");

		String selectionArgs[] = { "1" };

		return DBService.getSingleCoumn("Interview", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 Email 查詢 訪員id
	 * 
	 * @param Email
	 * @param context
	 * @return
	 */
	public static String getInterviewGuid(String Email, Context context) {
		String columns[] = { "Guid" };

		StringBuilder selection = new StringBuilder();
		selection.append("Email = ?");

		String selectionArgs[] = { Email };

		return DBService.getSingleCoumn("Interview", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 查詢訪員Email
	 * 
	 * @param context
	 * @return
	 */
	public static String getEmail(Context context) {
		String columns[] = { "Email" };

		StringBuilder selection = new StringBuilder();
		selection.append("serial = ?");

		String selectionArgs[] = { "1" };

		return DBService.getSingleCoumn("Interview", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 Email 查詢 訪員密碼
	 * 
	 * @param Email
	 * @param context
	 * @return
	 */
	public static String getPassword(String Email, Context context) {
		String columns[] = { "Password" };

		StringBuilder selection = new StringBuilder();
		selection.append("Email = ?");

		String selectionArgs[] = { Email };

		return DBService.getSingleCoumn("Interview", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 查詢所有樣本資料
	 * 
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryAll(Context context) {
		return DBService.getListCoumn("Interview", null, null, null, null,
				null, null, context);
	}
}
