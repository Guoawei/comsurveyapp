package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.dto.SinglePsaaDto;

import android.content.ContentValues;
import android.content.Context;

public class SinglePsaaService {

	/**
	 * 新增單題規則資料
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdInsert(SinglePsaaDto dto, Context context) {
		ContentValues Data = new ContentValues();
		Data.put("SurveyGuid", dto.getSurveyGuid());
		Data.put("SubjectIndex", dto.getSubjectIndex());
		Data.put("answer", dto.getAnswer());
		Data.put("jumpto", dto.getJumpto());
		Data.put("isselect", dto.getIsselect());

		DBService.insertTableData("SinglePsaa", Data, context);
	}

	/**
	 * 依據 問題編號、題號 查詢 所以單題跳題資料
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryAllSinglePsaa(
			String SurveyGuid, String SubjectIndex, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		return DBService.getListCoumn("SinglePsaa", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 問卷編號、題號、答案 查詢 單題跳題題號
	 * 給單一跳題，且每個選項有獨立的跳題邏輯用的
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static String getSubjectIndexByAnswer(String SurveyGuid,
			String SubjectIndex, String Answer, Context context) {
		String columns[] = { "jumpto" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ? and ");
		selection.append("answer = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex, Answer };

		return DBService.getSingleCoumn("SinglePsaa", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}
	
	/**
	 * 依據 問卷編號、題號 查詢 單題跳題題號
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static String getSubjectIndex(String SurveyGuid,
			String SubjectIndex, Context context) {
		String columns[] = { "jumpto" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		return DBService.getSingleCoumn("SinglePsaa", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問卷編號、題號 查詢 選擇否
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static String getIsSelect(String SurveyGuid, String SubjectIndex,
			Context context) {
		String columns[] = { "isselect" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };
		return DBService.getSingleCoumn("SinglePsaa", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問題編號、題號 刪除 單選跳題
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 */
	public static void deleteSinglePsaa(String SurveyGuid, String SubjectIndex,
			Context context) {

		StringBuilder whereClause = new StringBuilder();
		whereClause.append("SurveyGuid = ? and ");
		whereClause.append("SubjectIndex = ?");

		String whereArgs[] = { SurveyGuid, SubjectIndex };

		DBService.deleteTableData("SinglePsaa", whereClause.toString(),
				whereArgs, context);
	}

	/**
	 * 查詢 所有單跳的答案
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static HashMap<String, String> querySinglePsaaAnswer(
			String SurveyGuid, String SubjectIndex, Context context) {
		String columns[] = { "answer" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ?  and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		return DBService.getHashMapCoumn("SinglePsaa", columns,
				selection.toString(), selectionArgs, null, null, null, context);
	}
}
