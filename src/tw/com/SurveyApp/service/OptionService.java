package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.dto.OptionDto;

import android.content.ContentValues;
import android.content.Context;

public class OptionService {

	/**
	 * 新增 選項
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdInsert(OptionDto dto, Context context) {
		ContentValues Data = new ContentValues();
		Data.put("SurveyGuid", dto.getSurveyGuid());
		Data.put("SubjectIndex", dto.getSubjectIndex());
		Data.put("optionIndex", dto.getOptionIndex());
		Data.put("optionText", dto.getOptionText());
		Data.put("optionValue", dto.getOptionValue());
		Data.put("optionIsShowTextView", dto.getOptionIsShowTextView());
		Data.put("optionTextViewPosition", dto.getOptionTextViewPosition());
		Data.put("optionPickerValueArray", dto.getOptionPickerValueArray());
		Data.put("optionDyPickerSubject", dto.getOptionDyPickerSubject());
		Data.put("optionDyPickerMin", dto.getOptionDyPickerMin());
		Data.put("optionDyPickerMax", dto.getOptionDyPickerMax());
		Data.put("optionDyPickerTick", dto.getOptionDyPickerTick());

		DBService.insertTableData("Option", Data, context);
	}

	/**
	 * 依據 問卷編號、題號 查詢 選項
	 * 
	 * @param SurveyGuid
	 *            問卷id
	 * @param SubjectIndex
	 *            題號
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryOption(
			String SurveyGuid, String SubjectIndex, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		return DBService.getListCoumn("Option", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 問卷編號、題號 亂數取得 選項
	 * 
	 * @param SurveyGuid
	 *            問卷id
	 * @param SubjectIndex
	 *            題號
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryRANDOMOption(
			String SurveyGuid, String SubjectIndex, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		String orderBy = "RANDOM()";

		return DBService.getListCoumn("Option", null, selection.toString(),
				selectionArgs, null, null, orderBy, context);
	}

	/**
	 * 依據 問卷編號、題號 刪除 選項
	 * 
	 * @param SurveyGuid
	 *            問卷id
	 * @param SubjectIndex
	 *            題號
	 * @param context
	 */
	public static void deleteOption(String SurveyGuid, String SubjectIndex,
			Context context) {

		StringBuilder whereClause = new StringBuilder();
		whereClause.append("SurveyGuid = ? and ");
		whereClause.append("SubjectIndex = ?");

		String whereArgs[] = { SurveyGuid, SubjectIndex };

		DBService.deleteTableData("Option", whereClause.toString(), whereArgs,
				context);
	}

	/**
	 * 依據 問卷id、題號、選項值 查詢 選項編號
	 * 
	 * @param SurveyGuid
	 *            問卷id
	 * @param SubjectIndex
	 *            題號
	 * @param optionValue
	 *            選項值
	 * @param context
	 * @return
	 */
	public String queryOptionIndex(String SurveyGuid, String SubjectIndex,
			String optionValue, Context context) {

		String columns[] = { "optionIndex" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ? and ");
		selection.append("optionValue = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex, optionValue };

		return DBService.getSingleCoumn("Option", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}
}
