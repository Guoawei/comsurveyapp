package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.dto.GroupPsaaDto;

import android.content.ContentValues;
import android.content.Context;

public class GroupPsaaService {

	/**
	 * 新增群組跳題資料
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdInsert(GroupPsaaDto dto, Context context) {
		ContentValues Data = new ContentValues();
		Data.put("SurveyGuid", dto.getSurveyGuid());
		Data.put("SubjectIndex", dto.getSubjectIndex());
		Data.put("subjectList", dto.getSubjectList());
		Data.put("answerList", dto.getAnswerList());
		Data.put("jumpto", dto.getJumpto());

		DBService.insertTableData("GroupPsaa", Data, context);
	}

	/**
	 * 依據 問題編號、題號 查詢 多題跳題題號
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static String getSubjectIndex(String SurveyGuid,
			String SubjectIndex, Context context) {

		String columns[] = { "jumpto" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		return DBService.getSingleCoumn("GroupPsaa", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問題編號、題號 查詢 所有多選跳題資料
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryAllGroupPsaa(
			String SurveyGuid, String SubjectIndex, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		return DBService.getListCoumn("GroupPsaa", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 問卷編號、題號 刪除 群組跳題
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 */
	public static void deleteGroupPsaa(String SurveyGuid, String SubjectIndex,
			Context context) {

		StringBuilder whereClause = new StringBuilder();
		whereClause.append("SurveyGuid = ? and ");
		whereClause.append("SubjectIndex = ?");

		String whereArgs[] = { SurveyGuid, SubjectIndex };

		DBService.deleteTableData("GroupPsaa", whereClause.toString(),
				whereArgs, context);
	}
}
