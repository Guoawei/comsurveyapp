package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.dto.DynamicOptionByAnswerDto;

import android.content.ContentValues;
import android.content.Context;

public class DynamicOptionByAnswerService {

	/**
	 * 新增動態選項資料
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdInsert(DynamicOptionByAnswerDto dto, Context context) {
		ContentValues Data = new ContentValues();
		Data.put("SurveyGuid", dto.getSurveyGuid());
		Data.put("DyaGuid", dto.getDyaGuid());
		Data.put("SubjectNumber", dto.getSubjectNumber());
		Data.put("TargetAnswer", dto.getTargetAnswer());
		Data.put("IsShow", dto.getIsShow());
		Data.put("TargetOptionIndex", dto.getTargetOptionIndex());
		Data.put("IsOr", dto.getIsOr());

		DBService.insertTableData("DynamicOptionByAnswer", Data, context);
	}

	/**
	 * 依據 動態選項編號 查詢 資料筆數
	 * 
	 * @param dyoptionguid
	 * @param context
	 * @return
	 */
	public static int getLength(String DyaGuid, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("DyaGuid = ?");

		String selectionArgs[] = { DyaGuid };

		return DBService.getLength("DynamicOptionByAnswer", null,
				selection.toString(), selectionArgs, null, null, null, context);
	}
	
	/**
	 * 依據 動態選項編號 查詢 And資料筆數
	 * 
	 * @param dyoptionguid
	 * @param context
	 * @return
	 */
	public static int getAndLogicLength(String DyaGuid, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("DyaGuid = ? and ");
		selection.append("IsOr = '0'");

		String selectionArgs[] = { DyaGuid };

		return DBService.getLength("DynamicOptionByAnswer", null,
				selection.toString(), selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 動態選項編號 刪除 動態選項
	 * 
	 * @param dyoptionguid
	 * @param context
	 */
	public static void deleteDynamicOption(String DyaGuid, Context context) {

		StringBuilder whereClause = new StringBuilder();
		whereClause.append("DyaGuid = ?");

		String whereArgs[] = { DyaGuid };

		DBService.deleteTableData("DynamicOptionByAnswer",
				whereClause.toString(), whereArgs, context);
	}

	/**
	 * 依據 動態選項編號、問卷編號 查詢 動態選項
	 * 
	 * @param SurveyGuid
	 * @param DyaGuid
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryAllDyamicOption(
			String SurveyGuid, String DyaGuid, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("DyaGuid = ?");

		String selectionArgs[] = { SurveyGuid, DyaGuid };

		return DBService.getListCoumn("DynamicOptionByAnswer", null,
				selection.toString(), selectionArgs, null, null, null, context);
	}
}
