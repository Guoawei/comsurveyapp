package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.dto.QuestionnaireDto;

import android.content.ContentValues;
import android.content.Context;

public class QuestionnaireService {

	/**
	 * 更新問卷資料
	 * 
	 * @param dto
	 * @param SurveyGuid
	 * @param context
	 */
	public static void cmdUpdata(QuestionnaireDto dto, String SurveyGuid,
			Context context) {
		ContentValues Data = new ContentValues();

		if (dto.getTitle() != null && !dto.getTitle().trim().equals("")) {
			Data.put("Title", dto.getTitle());
		}
		if (dto.getGreetingText() != null
				&& !dto.getGreetingText().trim().equals("")) {
			Data.put("GreetingText", dto.getGreetingText());
		}
		if (dto.getThankText() != null && !dto.getThankText().trim().equals("")) {
			Data.put("ThankText", dto.getThankText());
		}
		if (dto.getTotalRows() != null && !dto.getTotalRows().trim().equals("")) {
			Data.put("TotalRows", dto.getTotalRows());
		}
		if (dto.getVer() != null && !dto.getVer().trim().equals("")) {
			Data.put("Ver", dto.getVer());
		}
		if (dto.getCDateTime() != null && !dto.getCDateTime().trim().equals("")) {
			Data.put("CDateTime", dto.getCDateTime());
		}
		if (dto.getUDateTime() != null && !dto.getUDateTime().trim().equals("")) {
			Data.put("UDateTime", dto.getUDateTime());
		}
		if (dto.getCompletedRow() != null
				&& !dto.getCompletedRow().trim().equals("")) {
			Data.put("CompletedRow", dto.getCompletedRow());
		}
		if (dto.getIntervieweeRows() != null
				&& !dto.getIntervieweeRows().trim().equals("")) {
			Data.put("IntervieweeRows", dto.getIntervieweeRows());
		}

		StringBuilder whereClause = new StringBuilder();
		whereClause.append(" SurveyGuid = ?");

		String whereArgs[] = { SurveyGuid };

		DBService.updataTableData("Questionnaire", Data,
				whereClause.toString(), whereArgs, context);
	}

	/**
	 * 新增問卷資料
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdInsert(QuestionnaireDto dto, Context context) {
		ContentValues Data = new ContentValues();
		Data.put("Title", dto.getTitle());
		Data.put("GreetingText", dto.getGreetingText());
		Data.put("ThankText", dto.getThankText());
		Data.put("TotalRows", dto.getTotalRows());
		Data.put("Ver", dto.getVer());
		Data.put("CDateTime", dto.getCDateTime());
		Data.put("UDateTime", dto.getUDateTime());
		Data.put("SurveyGuid", dto.getSurveyGuid());
		Data.put("CompletedRow", dto.getCompletedRow());
		Data.put("IntervieweeRows", dto.getIntervieweeRows());

		DBService.insertTableData("Questionnaire", Data, context);
	}

	/**
	 * 依據 問卷編號 查詢 資料筆數
	 * 
	 * @param SurveyGuid
	 * @param context
	 * @return
	 */
	public static int getLength(String SurveyGuid, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ?");

		String selectionArgs[] = { SurveyGuid };

		return DBService.getLength("Questionnaire", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 問卷id 查詢 結束問候語
	 * 
	 * @param SurveyGuid
	 * @param context
	 * @return
	 */
	public static String getThankText(String SurveyGuid, Context context) {
		String columns[] = { "ThankText" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ?");

		String selectionArgs[] = { SurveyGuid };

		return DBService.getSingleCoumn("Questionnaire", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問卷id 查詢 問候語
	 * 
	 * @param SurveyGuid
	 * @param context
	 * @return
	 */
	public static String getGreetingText(String SurveyGuid, Context context) {
		String columns[] = { "GreetingText" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ?");

		String selectionArgs[] = { SurveyGuid };

		return DBService.getSingleCoumn("Questionnaire", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問卷id 查詢 問卷名稱
	 * 
	 * @param SurveyGuid
	 * @param context
	 * @return
	 */
	public static String getTitle(String SurveyGuid, Context context) {

		String columns[] = { "Title" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ?");

		String selectionArgs[] = { SurveyGuid };

		return DBService.getSingleCoumn("Questionnaire", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問卷id 查詢 已完成數量
	 * 
	 * @param SurveyGuid
	 * @param context
	 * @return
	 */
	public static String getCompletedRow(String SurveyGuid, Context context) {
		String columns[] = { "CompletedRow" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ?");

		String selectionArgs[] = { SurveyGuid };

		return DBService.getSingleCoumn("Questionnaire", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問卷id 查詢 總題數
	 * 
	 * @param SurveyGuid
	 * @param context
	 * @return
	 */
	public static String getTotalRows(String SurveyGuid, Context context) {
		String columns[] = { "TotalRows" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ?");

		String selectionArgs[] = { SurveyGuid };

		return DBService.getSingleCoumn("Questionnaire", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問卷編號 查詢 所需欄位
	 * 
	 * @param getCoumnname
	 * @param SurveyGuid
	 * @param context
	 * @return
	 */
	public static String getQuestionnaireData(String getCoumnname,
			String SurveyGuid, Context context) {

		String columns[] = { getCoumnname };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ?");

		String selectionArgs[] = { SurveyGuid };

		return DBService.getSingleCoumn("Questionnaire", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 查詢所有問卷列表
	 * 
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryQuestionnaireAll(
			Context context) {
		return DBService.getListCoumn("Questionnaire", null, null, null, null,
				null, null, context);
	}
}
