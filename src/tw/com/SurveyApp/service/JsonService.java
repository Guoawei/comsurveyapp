package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tw.com.SurveyApp.data.JsonConnection;
import tw.com.SurveyApp.dto.DynamicOptionByAnswerDto;
import tw.com.SurveyApp.dto.DynamicOptionDto;
import tw.com.SurveyApp.dto.GroupOptionDto;
import tw.com.SurveyApp.dto.GroupPsaaDto;
import tw.com.SurveyApp.dto.InterviewDto;
import tw.com.SurveyApp.dto.OptionDto;
import tw.com.SurveyApp.dto.QuestionnaireDto;
import tw.com.SurveyApp.dto.SampleDto;
import tw.com.SurveyApp.dto.SinglePsaaDto;
import tw.com.SurveyApp.dto.SubjectDto;
import tw.com.SurveyApp.dto.TipDto;
import tw.com.SurveyApp.dto.UpdateAppDto;
import android.content.Context;
import android.widget.Toast;

public class JsonService {

	/**
	 * 是否正確讀取
	 * 
	 * @param strResult
	 * @return
	 */
	public String readTrue(String strResult) {
		try {
			JSONObject header = new JSONObject(strResult)
					.getJSONObject("Header");
			String status = header.getString("Status");

			return status;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "1";
	}

	/**
	 * 取得問卷列表資料
	 * 
	 * @param strResult
	 * @param context
	 */
	public String[] readQuestionnaireList(String strResult, Context context) {
		try {
			JSONObject header = new JSONObject(strResult)
					.getJSONObject("Header");
			String status = header.getString("Status");

			if (status.equals("1")) {
				return null;
			}

			JSONObject body = new JSONObject(strResult).getJSONObject("Body");
			JSONObject getSurveyList = body.getJSONObject("getSurveyList");
			JSONArray MetaData = getSurveyList.getJSONArray("MetaData");

			String ID[] = new String[MetaData.length()];

			for (int i = 0; i < MetaData.length(); i++) {
				ID[i] = ((JSONObject) MetaData.get(i)).getString("SurveyGuid");

				String SurveyGuid = ((JSONObject) MetaData.get(i))
						.getString("SurveyGuid");

				int length = QuestionnaireService.getLength(ID[i], context);

				QuestionnaireDto questionnaireDto = new QuestionnaireDto();
				questionnaireDto.setTitle(((JSONObject) MetaData.get(i))
						.getString("Title"));
				questionnaireDto.setGreetingText(((JSONObject) MetaData.get(i))
						.getString("GreetingText"));
				questionnaireDto.setThankText(((JSONObject) MetaData.get(i))
						.getString("ThankText"));
				questionnaireDto.setTotalRows(((JSONObject) MetaData.get(i))
						.getString("TotalRows"));
				questionnaireDto.setVer(((JSONObject) MetaData.get(i))
						.getString("Ver"));
				questionnaireDto.setCDateTime(((JSONObject) MetaData.get(i))
						.getString("CDateTime"));
				questionnaireDto.setUDateTime(((JSONObject) MetaData.get(i))
						.getString("UDateTime"));

				if (length == 0) {
					questionnaireDto.setSurveyGuid(SurveyGuid);
					questionnaireDto.setCompletedRow("0");
					questionnaireDto.setIntervieweeRows("0");

					QuestionnaireService.cmdInsert(questionnaireDto, context);
				} else {
					String TotalRows = QuestionnaireService.getTotalRows(ID[i],
							context);

					String CompletedRow = QuestionnaireService.getCompletedRow(
							ID[i], context);

					questionnaireDto.setCompletedRow(CompletedRow);
					questionnaireDto.setTotalRows(TotalRows);

					QuestionnaireService.cmdUpdata(questionnaireDto,
							SurveyGuid, context);
				}
			}

			return ID;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * 取得更新App相關資料
	 * 
	 * @param strResult
	 * @param context
	 * @return
	 */
	public UpdateAppDto getUpdateApp(String strResult, Context context) {
		try {

			JSONObject header = new JSONObject(strResult)
					.getJSONObject("Header");
			String status = header.getString("Status");
			String doc = header.getString("Doc");

			if (status.equals("1")) {
				Toast.makeText(context, doc, Toast.LENGTH_LONG).show();
				return null;
			}

			JSONObject body = new JSONObject(strResult).getJSONObject("Body");
			JSONObject updateApp = body.getJSONObject("updateApp");

			UpdateAppDto dto = new UpdateAppDto();
			dto.setVersion(updateApp.getString("version"));
			dto.setUrl(updateApp.getString("url"));

			return dto;
		} catch (JSONException e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 取得訪問者資料
	 * 
	 * @param strResult
	 * @param pwd
	 * @param context
	 * @return
	 */
	public String readInterViewe(String strResult, String pwd, Context context) {
		try {

			JSONObject header = new JSONObject(strResult)
					.getJSONObject("Header");
			String status = header.getString("Status");
			String doc = header.getString("Doc");

			if (status.equals("1")) {
				Toast.makeText(context, doc, Toast.LENGTH_LONG).show();
				return "";
			}

			JSONObject body = new JSONObject(strResult).getJSONObject("Body");
			JSONObject login = body.getJSONObject("login");

			InterviewDto interviewDto = new InterviewDto();
			interviewDto.setGuid(login.getString("Guid"));
			interviewDto.setName(login.getString("Name"));
			interviewDto.setEmail(login.getString("Email"));
			interviewDto.setPassword(pwd);

			InterviewService.cmdInsert(interviewDto, context);

			return login.getString("Guid");
		} catch (JSONException e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 讀取動態選單
	 * 
	 * @param strResult
	 * @param dyoptionguid
	 * @param context
	 */
	private void readDynamicOption(String strResult, String SurveyGuid,
			String dyoptionguid, Context context) {
		try {
			JSONObject header = new JSONObject(strResult)
					.getJSONObject("Header");
			String status = header.getString("Status");

			if (status.equals("1")) {
				return;
			}

			int length = DynamicOptionService.getLength(dyoptionguid, context);

			if (length != 0) {
				DynamicOptionService.deleteDynamicOption(dyoptionguid, context);
			}

			JSONObject body = new JSONObject(strResult).getJSONObject("Body");
			JSONObject getDynamicOption = body
					.getJSONObject("getDynamicOption");

			JSONArray subject_accordingValue = getDynamicOption
					.getJSONArray("subject_accordingValue");

			for (int i = 0; i < subject_accordingValue.length(); i++) {
				DynamicOptionDto dynamicoptionDto = new DynamicOptionDto();
				dynamicoptionDto.setSurveyGuid(SurveyGuid);
				dynamicoptionDto.setDyoptionguid(dyoptionguid);
				dynamicoptionDto.setSubject_accordingKey(getDynamicOption
						.getString("subject_accordingKey"));
				dynamicoptionDto.setSubject_subjectIndex(getDynamicOption
						.getString("subject_subjectIndex"));
				dynamicoptionDto.setAccordingValue(subject_accordingValue
						.getJSONObject(i).getString("accordingValue"));
				dynamicoptionDto.setValue(subject_accordingValue.getJSONObject(
						i).getString("value"));
				dynamicoptionDto.setSubject_index(subject_accordingValue
						.getJSONObject(i).getString("index"));
				dynamicoptionDto.setText(subject_accordingValue
						.getJSONObject(i).getString("text"));

				DynamicOptionService.cmdInsert(dynamicoptionDto, context);
			}
		} catch (JSONException e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * 讀取前置動態選單
	 * 
	 * @param strResult
	 * @param SurveyGuid
	 * @param DyaGuid
	 * @param context
	 */
	private void readDynamicOptionByAnswer(String strResult, String SurveyGuid,
			String DyaGuid, Context context) {
		try {
			JSONObject header = new JSONObject(strResult)
					.getJSONObject("Header");
			String status = header.getString("Status");

			if (status.equals("1")) {
				return;
			}

			int length = DynamicOptionByAnswerService.getLength(DyaGuid,
					context);

			if (length != 0) {
				DynamicOptionByAnswerService.deleteDynamicOption(DyaGuid,
						context);
			}

			JSONObject body = new JSONObject(strResult).getJSONObject("Body");

			JSONArray getDynamicOptionByAnswer = body.getJSONArray("getDynamicOptionByAnswer");
			JSONObject andLogicArray = getDynamicOptionByAnswer.getJSONObject(0).getJSONObject("AndLogic");
			JSONObject orLogicArray = getDynamicOptionByAnswer.getJSONObject(0).getJSONObject("OrLogic");
			JSONObject[] logicArray = {andLogicArray,orLogicArray};
			
			for (int i = 0; i < getDynamicOptionByAnswer.length(); i++) {
				StringBuilder OptionIndex = new StringBuilder();
				JSONArray targetOptionIndex = getDynamicOptionByAnswer.getJSONObject(i).getJSONArray("targetOptionIndex");
				for (int j = 0; j < targetOptionIndex.length(); j++) {
					OptionIndex.append((String) targetOptionIndex.getString(i)).append(",");
				}

				for (int k = 0; k < logicArray.length; k++){
					JSONArray subjectNumber = logicArray[k].getJSONArray("subjectNumber");
					JSONArray targetAnswer = logicArray[k].getJSONArray("targetAnswer");

					if (subjectNumber.length() == targetAnswer.length()) {
						for (int z = 0; z < subjectNumber.length(); z++) {

							DynamicOptionByAnswerDto dynamicoptionbyanswerDto = new DynamicOptionByAnswerDto();
							dynamicoptionbyanswerDto.setSurveyGuid(SurveyGuid);
							dynamicoptionbyanswerDto.setDyaGuid(DyaGuid);
							dynamicoptionbyanswerDto.setSubjectNumber((String) subjectNumber.getString(z));

							dynamicoptionbyanswerDto.setTargetAnswer((String) targetAnswer.getString(z));
							dynamicoptionbyanswerDto.setIsShow((String) getDynamicOptionByAnswer.getJSONObject(i).getString("isShow"));
							dynamicoptionbyanswerDto.setTargetOptionIndex(OptionIndex.toString());
							if(k==1){
								dynamicoptionbyanswerDto.setIsOr("1");
							}else {
								dynamicoptionbyanswerDto.setIsOr("0");
							}

							DynamicOptionByAnswerService.cmdInsert(dynamicoptionbyanswerDto, context);
						}
					}
				}
				
			}

		} catch (JSONException e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * 取得選項題目資料
	 * 
	 * @param strResult
	 * @param SurveyGuid
	 * @param GoGuid
	 * @param context
	 */
	private void readSubjectOption(String strResult, String SurveyGuid,
			String SubjectIndex, Context context) {
		try {
			JSONObject header = new JSONObject(strResult)
					.getJSONObject("Header");
			String status = header.getString("Status");

			if (status.equals("1")) {
				return;
			}

			JSONObject body = new JSONObject(strResult).getJSONObject("Body");

			JSONArray getOption = body.getJSONArray("getOption");

			OptionService.deleteOption(SurveyGuid, SubjectIndex, context);

			for (int i = 0; i < getOption.length(); i++) {

				OptionDto optionDto = new OptionDto();
				optionDto.setSurveyGuid(SurveyGuid);
				optionDto.setSubjectIndex(SubjectIndex);
				optionDto.setOptionIndex(getOption.getJSONObject(i).getString(
						"optionIndex"));
				optionDto.setOptionText(getOption.getJSONObject(i).getString(
						"optionText"));
				optionDto.setOptionValue(getOption.getJSONObject(i).getString(
						"optionValue"));
				optionDto.setOptionIsShowTextView(getOption.getJSONObject(i)
						.getString("optionIsShowTextView"));
				optionDto.setOptionTextViewPosition(getOption.getJSONObject(i)
						.getString("optionTextViewPosition"));

				if (!getOption.getJSONObject(i)
						.isNull("optionPickerValueArray")) {

					JSONArray optionPickerValueArray = getOption.getJSONObject(
							i).getJSONArray("optionPickerValueArray");

					StringBuilder pickerValue = new StringBuilder();

					for (int j = 0; j < optionPickerValueArray.length(); j++) {

						pickerValue.append(
								optionPickerValueArray.get(j).toString())
								.append(",");
					}

					optionDto.setOptionPickerValueArray(pickerValue.toString());
				} else {
					optionDto.setOptionPickerValueArray("");
				}

				if (!getOption.getJSONObject(i).isNull("optionDyPickerSubject")) {
					optionDto.setOptionDyPickerSubject(getOption.getJSONObject(
							i).getString("optionDyPickerSubject"));
				} else {
					optionDto.setOptionDyPickerSubject("");
				}

				if (!getOption.getJSONObject(i).isNull("optionDyPickerMin")) {
					optionDto.setOptionDyPickerMin(getOption.getJSONObject(i)
							.getString("optionDyPickerMin"));
				} else {
					optionDto.setOptionDyPickerMin("");
				}

				if (!getOption.getJSONObject(i).isNull("optionDyPickerMax")) {
					optionDto.setOptionDyPickerMax(getOption.getJSONObject(i)
							.getString("optionDyPickerMax"));
				} else {
					optionDto.setOptionDyPickerMax("");
				}

				if (!getOption.getJSONObject(i).isNull("optionDyPickerTick")) {
					optionDto.setOptionDyPickerTick(getOption.getJSONObject(i)
							.getString("optionDyPickerTick"));
				} else {
					optionDto.setOptionDyPickerTick("");
				}

				OptionService.cmdInsert(optionDto, context);
			}
		} catch (JSONException e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * 取得群組選項內容
	 * 
	 * @param strResult
	 * @param SurveyGuid
	 * @param GoGuid
	 * @param context
	 */
	private void readGroupOption(String strResult, String SurveyGuid,
			String GoGuid, Context context) {
		try {
			JSONObject header = new JSONObject(strResult)
					.getJSONObject("Header");
			String status = header.getString("Status");

			if (status.equals("1")) {
				return;
			}

			int length = GroupOptionService.getLength(GoGuid, context);

			if (length != 0) {
				GroupOptionService.deleteGroupOption(GoGuid, context);
			}

			JSONObject body = new JSONObject(strResult).getJSONObject("Body");

			JSONArray getGroupOption = body.getJSONArray("getGroupOption");

			for (int i = 0; i < getGroupOption.length(); i++) {
				GroupOptionDto groupoptionDto = new GroupOptionDto();
				groupoptionDto.setSurveyGuid(SurveyGuid);
				groupoptionDto.setGoGuid(GoGuid);
				groupoptionDto.setGroupId((String) getGroupOption
						.getJSONObject(i).getString("id"));
				groupoptionDto.setGroupType((String) getGroupOption
						.getJSONObject(i).getString("type"));
				groupoptionDto.setGroupIndex((String) getGroupOption
						.getJSONObject(i).getString("index"));
				groupoptionDto.setGroupText((String) getGroupOption
						.getJSONObject(i).getString("text"));
				groupoptionDto.setGroupValue((String) getGroupOption
						.getJSONObject(i).getString("value"));
				groupoptionDto.setIsShowTextView((String) getGroupOption
						.getJSONObject(i).getString("isShowTextView"));
				groupoptionDto.setTextViewPosition((String) getGroupOption
						.getJSONObject(i).getString("textViewPosition"));

				GroupOptionService.cmdInsert(groupoptionDto, context);
			}

		} catch (JSONException e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * 取得題目和選項資料
	 * 
	 * @param strResult
	 * @param QuestionnairesSerial
	 * @param context
	 */
	public void readQuestionnaire(String strResult, String SurveyGuid,
			Context context) {
		try {
			JSONObject header = new JSONObject(strResult)
					.getJSONObject("Header");
			String status = header.getString("Status");

			if (status.equals("1")) {
				return;
			}

			TipService.deleteTip(SurveyGuid, context);

			JSONObject body = new JSONObject(strResult).getJSONObject("Body");
			JSONObject getQuestion = body.getJSONObject("getQuestion");
			JSONArray subject = getQuestion.getJSONArray("Subject");

			for (int i = 0; i < subject.length(); i++) {
				SubjectDto subjectdto = new SubjectDto();

				subjectdto.setSurveyGuid(SurveyGuid);

				String SubjectIndex = subject.getJSONObject(i).getString(
						"subject_index");
				subjectdto.setSubjectIndex(SubjectIndex);
				subjectdto.setSubjectType(subject.getJSONObject(i).getString(
						"subject_type"));
				subjectdto.setSubjectTitle(subject.getJSONObject(i).getString(
						"subject_title"));
				subjectdto.setMultiSelectedLimit(subject.getJSONObject(i)
						.getString("subject_multiSelectedLimit"));
				subjectdto.setDynamicOption(subject.getJSONObject(i).getString(
						"subject_dynamicOption"));
				subjectdto.setDynamicOptionAnswer(subject.getJSONObject(i)
						.getString("subject_dynamicOptionAnswer"));
				subjectdto.setGroupOption(subject.getJSONObject(i).getString(
						"subject_groupOption"));
				subjectdto.setSubjectOpiton(subject.getJSONObject(i).getString(
						"subject_option"));
				subjectdto.setSubjectIsRandom(subject.getJSONObject(i)
						.getString("subject_isRandom"));
				subjectdto.setOptionOutputForm(subject.getJSONObject(i)
						.getString("subject_optionOutputForm"));

				SubjectDto subjectDto = new SubjectDto();
				subjectDto.setSubjectType(subject.getJSONObject(i).getString(
						"subject_type"));
				subjectDto.setSubjectTitle(subject.getJSONObject(i).getString(
						"subject_title"));
				subjectDto.setMultiSelectedLimit(subject.getJSONObject(i)
						.getString("subject_multiSelectedLimit"));
				subjectDto.setDynamicOption(subject.getJSONObject(i).getString(
						"subject_dynamicOption"));
				subjectDto.setDynamicOptionAnswer(subject.getJSONObject(i)
						.getString("subject_dynamicOptionAnswer"));
				subjectDto.setGroupOption(subject.getJSONObject(i).getString(
						"subject_groupOption"));
				subjectDto.setSubjectOpiton(subject.getJSONObject(i).getString(
						"subject_option"));
				subjectDto.setSubjectIsRandom(subject.getJSONObject(i)
						.getString("subject_isRandom"));
				subjectDto.setOptionOutputForm(subject.getJSONObject(i)
						.getString("subject_optionOutputForm"));

				int length = SubjectService.getLength(SurveyGuid, SubjectIndex,
						context);

				if (length == 0) {
					subjectDto.setSurveyGuid(SurveyGuid);
					subjectDto.setSubjectIndex(SubjectIndex);

					SubjectService.cmdInsert(subjectDto, context);
				} else {
					SubjectService.cmdUpdata(subjectDto, SurveyGuid,
							SubjectIndex, context);
				}

				if (!subject.getJSONObject(i).isNull("subject_dynamicOption")) {
					StringBuilder url = new StringBuilder();
					url.append(JsonConnection.connection)
							.append("getDynamicOption?")
							.append("dyoptionguid=")
							.append(subjectdto.getDynamicOption());
					JsonConnection jc = new JsonConnection();

					String result = jc.connServerForResult(url.toString());

					String doc = readTrue(result);

					if (doc.equals("0")) {
						readDynamicOption(result, SurveyGuid,
								subjectdto.getDynamicOption(), context);
					}
				}

				if (!subject.getJSONObject(i).isNull(
						"subject_dynamicOptionAnswer")) {
					StringBuilder url = new StringBuilder();
					url.append(JsonConnection.connection)
							.append("getDynamicOptionByAnswer?")
							.append("dyaguid=")
							.append(subjectdto.getDynamicOptionAnswer());

					JsonConnection jc = new JsonConnection();

					String result = jc.connServerForResult(url.toString());

					String doc = readTrue(result);

					if (doc.equals("0")) {
						readDynamicOptionByAnswer(result, SurveyGuid,
								subjectdto.getDynamicOptionAnswer(), context);
					}
				}

				if (!subject.getJSONObject(i).isNull("subject_groupOption")) {
					StringBuilder url = new StringBuilder();
					url.append(JsonConnection.connection)
							.append("getGroupOption?")
							.append("groupoptionguid=")
							.append(subjectdto.getGroupOption());

					JsonConnection jc = new JsonConnection();

					String result = jc.connServerForResult(url.toString());

					String doc = readTrue(result);

					if (doc.equals("0")) {
						readGroupOption(result, SurveyGuid,
								subjectdto.getGroupOption(), context);
					}
				}

				if (!subject.getJSONObject(i).isNull("subject_option")) {

					if (!subjectdto.getSubjectOpiton().trim().equals("")) {
						StringBuilder url = new StringBuilder();
						url.append(JsonConnection.connection)
								.append("getOption?").append("optionguid=")
								.append(subjectdto.getSubjectOpiton());

						JsonConnection jc = new JsonConnection();

						String result = jc.connServerForResult(url.toString());

						String doc = readTrue(result);

						if (doc.equals("0")) {
							readSubjectOption(result, SurveyGuid, SubjectIndex,
									context);
						}
					}
				}

				if (!subject.getJSONObject(i).isNull("subject_singlePass")) {

					SinglePsaaService.deleteSinglePsaa(SurveyGuid,
							SubjectIndex, context);

					JSONObject subject_singlepass = subject.getJSONObject(i)
							.getJSONObject("subject_singlePass");

					if (subject_singlepass.length() != 0) {

						JSONArray answer = subject_singlepass
								.getJSONArray("answer");
						
						JSONArray jumpToArray = subject_singlepass.getJSONArray("jumpTo");
						ArrayList<String> jumpTo = new ArrayList<String>();
						for (int j = 0; j < jumpToArray.length(); j++){
							jumpTo.add(jumpToArray.getString(j)); 
						}
						System.out.println("@@@@@@"+jumpToArray.getString(0));
						for (int j = 0; j < answer.length(); j++) {
							SinglePsaaDto singlepsaaDto = new SinglePsaaDto();
							singlepsaaDto.setSurveyGuid(SurveyGuid);
							singlepsaaDto.setSubjectIndex(SubjectIndex);
							singlepsaaDto.setAnswer(answer.getString(j));
//							singlepsaaDto.setJumpto(subject_singlepass
//									.getString("jumpTo"));
							singlepsaaDto.setJumpto(jumpTo.get(j));
							singlepsaaDto.setIsselect(subject_singlepass
									.getString("select"));

							SinglePsaaService.cmdInsert(singlepsaaDto, context);
						}
					}
				}

				if (!subject.getJSONObject(i).isNull("subject_groupPass")) {

					JSONObject subject_grouppass = subject.getJSONObject(i)
							.getJSONObject("subject_groupPass");

					if (subject_grouppass.length() != 0) {
						JSONArray subject_grouppass_subject = subject_grouppass
								.getJSONArray("subject");
						String subjectList[] = new String[subject_grouppass_subject
								.length()];
						for (int j = 0; j < subject_grouppass_subject.length(); j++) {
							subjectList[j] = subject_grouppass_subject
									.getString(j);
						}

						JSONArray subject_grouppass_answer = subject_grouppass
								.getJSONArray("answer");
						String answerList[] = new String[subject_grouppass_answer
								.length()];
						for (int j = 0; j < subject_grouppass_answer.length(); j++) {
							answerList[j] = subject_grouppass_answer
									.getString(j);
						}

						GroupPsaaService.deleteGroupPsaa(SurveyGuid,
								SubjectIndex, context);

						for (int j = 0; j < subjectList.length; j++) {
							GroupPsaaDto grouppsaaDto = new GroupPsaaDto();
							grouppsaaDto.setSurveyGuid(SurveyGuid);
							grouppsaaDto.setSubjectIndex(SubjectIndex);
							grouppsaaDto.setSubjectList(subjectList[j]);
							grouppsaaDto.setAnswerList(answerList[j]);
							grouppsaaDto.setJumpto(subject_grouppass
									.getString("jumpTo"));

							GroupPsaaService.cmdInsert(grouppsaaDto, context);
						}
					}
				}

//				if (!subject.getJSONObject(i).isNull("subject_tips")) {
//					JSONArray subject_tip = subject.getJSONObject(i)
//							.getJSONArray("subject_tips");
//
//					if (subject_tip.length() != 0) {
//
//						TipService.deleteTip(SurveyGuid, SubjectIndex, context);
//
//						for (int j = 0; j < subject_tip.length(); j++) {
//							String img = subject_tip.getJSONObject(j)
//									.getString("tips_image");
//
//							TipDto tipDto = new TipDto();
//							tipDto.setSurveyGuid(SurveyGuid);
//							tipDto.setSubjectIndex(SubjectIndex);
//							tipDto.setTip_index(subject_tip.getJSONObject(j)
//									.getString("tips_index"));
//							tipDto.setTip_name("");
//							tipDto.setTip_image(subject_tip.getJSONObject(j)
//									.getString("tips_image"));
//							tipDto.setTip_text(subject_tip.getJSONObject(j)
//									.getString("tips_text"));
//
//							TipService.cmdInsert(tipDto, context);
//
//							if (!img.equals("null")) {
//								StringBuilder url = new StringBuilder();
//								url.append(JsonConnection.connection)
//										.append("getTipsImage?").append("img=")
//										.append(img);
//
//								JsonConnection jc = new JsonConnection();
//
//								jc.connServerForImage(url.toString(), img);
//							}
//						}
//					}
//				}
			}
		} catch (JSONException e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * 取得受訪者資料
	 * 
	 * @param strResult
	 * @param context
	 */
	public void readSample(String strResult, Context context) {
		try {

			JSONObject header = new JSONObject(strResult)
					.getJSONObject("Header");

			String status = header.getString("Status");
			String doc = header.getString("Doc");

			if (status.equals("1")) {
				Toast.makeText(context, doc, Toast.LENGTH_LONG).show();
				return;
			}

			JSONObject body = new JSONObject(strResult).getJSONObject("Body");
			JSONObject getSample = body.getJSONObject("getSample");

			// 依據 問卷編號 更新 問卷筆數
			QuestionnaireDto questionnaireDto = new QuestionnaireDto();
			questionnaireDto.setIntervieweeRows(getSample
					.getString("SampleRows"));

			QuestionnaireService.cmdUpdata(questionnaireDto,
					getSample.getString("SurveyGuid"), context);

			JSONArray Sample = getSample.getJSONArray("Sample");

			ArrayList<HashMap<String, String>> SampleList = new ArrayList<HashMap<String, String>>();
			SampleList = SampleService.querySampleAll(
					getSample.getString("SurveyGuid"), context);

			new SampleService().cmdDelete(getSample.getString("SurveyGuid"),
					context);

			for (int i = 0; i < Sample.length(); i++) {

				String SampleGuid = ((JSONObject) Sample.get(i)).getString(
						"SampleGuid").toString();

				SampleDto sampleDto = new SampleDto();
				sampleDto.setSurveyGuid(getSample.getString("SurveyGuid"));
				sampleDto.setSampleGuid(SampleGuid);
				sampleDto.setName(((JSONObject) Sample.get(i))
						.getString("Name").toString());
				sampleDto.setAge(((JSONObject) Sample.get(i)).getString(
						"Birthday").toString());
				sampleDto.setZipCode(((JSONObject) Sample.get(i)).getString(
						"ZipCode").toString());
				sampleDto.setAddress(((JSONObject) Sample.get(i)).getString(
						"Address").toString());
				sampleDto.setPhone(((JSONObject) Sample.get(i)).getString(
						"Phone").toString());
				sampleDto.setMainPriority(((JSONObject) Sample.get(i)).getString(
						"MainPriority").toString());
				sampleDto.setSubPriority(((JSONObject) Sample.get(i)).getString(
						"SubPriority").toString());

				sampleDto.setIsupdate("0");
				sampleDto.setAnswerDate("");

				for (int x = 0; x < SampleList.size(); x++) {
					if (SampleList.get(x).get("SampleGuid").equals(SampleGuid)) {
						sampleDto
								.setIsupdate(SampleList.get(x).get("isupdate"));
						sampleDto.setAnswerDate(SampleList.get(x).get(
								"AnswerDate"));
						break;
					}
				}

				SampleService.cmdInsert(sampleDto, context);
			}

		} catch (JSONException e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}
	/*
	 * 
	 * public void readSample(String strResult, Context context) { try {
	 * 
	 * JSONObject header = new JSONObject(strResult) .getJSONObject("Header");
	 * 
	 * String status = header.getString("Status"); String doc =
	 * header.getString("Doc");
	 * 
	 * if (status.equals("1")) { Toast.makeText(context, doc, 1).show(); return;
	 * }
	 * 
	 * JSONObject body = new JSONObject(strResult).getJSONObject("Body");
	 * JSONObject getSample = body.getJSONObject("getSample");
	 * 
	 * // 依據 問卷編號 更新 問卷筆數 ContentValues QuestionnaireData = new ContentValues();
	 * QuestionnaireData.put("IntervieweeRows",
	 * getSample.getString("SampleRows"));
	 * 
	 * StringBuilder QuestionnaireDataWhereClause = new StringBuilder();
	 * QuestionnaireDataWhereClause.append(" SurveyGuid = '")
	 * .append(getSample.getString("SurveyGuid")).append("'");
	 * 
	 * DBService.updataTableData("Questionnaire", QuestionnaireData,
	 * QuestionnaireDataWhereClause.toString(), context);
	 * 
	 * JSONArray Sample = getSample.getJSONArray("Sample");
	 * 
	 * new SampleService().cmdDelete(getSample.getString("SurveyGuid"),
	 * context);
	 * 
	 * for (int i = 0; i < Sample.length(); i++) { String SurveyGuid =
	 * getSample.getString("SurveyGuid"); String SampleGuid = ((JSONObject)
	 * Sample.get(i)).getString( "SampleGuid").toString();
	 * 
	 * SampleDto sampleDto = new SampleDto(); sampleDto.setName(((JSONObject)
	 * Sample.get(i)) .getString("Name").toString());
	 * sampleDto.setAge(((JSONObject) Sample.get(i)).getString(
	 * "Birthday").toString()); sampleDto.setZipCode(((JSONObject)
	 * Sample.get(i)).getString( "ZipCode").toString());
	 * sampleDto.setAddress(((JSONObject) Sample.get(i)).getString(
	 * "Address").toString()); sampleDto.setPhone(((JSONObject)
	 * Sample.get(i)).getString( "Phone").toString());
	 * sampleDto.setIsupdate("0"); sampleDto.setAnswerDate("");
	 * 
	 * sampleDto.setSampleGuid(SampleGuid); sampleDto.setSurveyGuid(SurveyGuid);
	 * 
	 * SampleService.cmdInsert(sampleDto, context); }
	 * 
	 * } catch (JSONException e) { System.out.println(e); e.printStackTrace(); }
	 * }
	 */
}
