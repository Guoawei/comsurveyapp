package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.dto.AnswerDto;

import android.content.ContentValues;
import android.content.Context;

public class AnswerService {

	/**
	 * 新增答案資料
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdInsert(AnswerDto dto, Context context) {
		ContentValues Data = new ContentValues();
		Data.put("SurveyGuid", dto.getSurveyGuid());
		Data.put("InterviewerID", dto.getInterviewerID());
		Data.put("SampleID", dto.getSampleID());
		Data.put("Previous", dto.getPrevious());
		Data.put("SubjectIndex", dto.getSubjectIndex());
		Data.put("Answer", dto.getAnswer());
		
		DBService.insertTableData("Answer", Data, context);
	}

	/**
	 * 依據 問卷編號、題號、受訪者編號 查詢 答案內容
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param SampleID
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryAnswer(
			String SurveyGuid, String SubjectIndex, String SampleID,
			Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ? and ");
		selection.append("SampleID = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex, SampleID };

		return DBService.getListCoumn("Answer", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 問卷編號、受訪者編號 查詢 答案
	 * 
	 * @param SurveyGuid
	 * @param SampleID
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryAnswer(
			String SurveyGuid, String SampleID, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SampleID = ?");

		String selectionArgs[] = { SurveyGuid, SampleID };

		return DBService.getListCoumn("Answer", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 問卷編號、題號、受訪者編號 刪除 答案內容
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param SampleID
	 * @param context
	 */
	public static void deleteAnswer(String SurveyGuid, String SubjectIndex,
			String SampleID, Context context) {

		StringBuilder whereClause = new StringBuilder();
		whereClause.append("SurveyGuid = ? and ");
		whereClause.append("SubjectIndex = ? and ");
		whereClause.append("SampleID = ?");

		String whereArgs[] = { SurveyGuid, SubjectIndex, SampleID };

		DBService.deleteTableData("Answer", whereClause.toString(), whereArgs,
				context);
	}

	/**
	 * 依據 題號 查詢 上一題題號
	 * 
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static String getPreviousString(String Previous, Context context) {
		String columns[] = { "Previous" };

		StringBuilder selection = new StringBuilder();
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { Previous };

		return DBService.getSingleCoumn("Answer", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 題號、受訪者編號 查詢 上一題題號
	 * 
	 * @param SubjectIndex
	 * @param SampleID
	 * @param context
	 * @return
	 */
	public static String getPrevious(String SubjectIndex, String SampleID,
			Context context) {

		String columns[] = { "Previous" };

		StringBuilder selection = new StringBuilder();
		selection.append("SubjectIndex = ? and ");
		selection.append("SampleID = ?");

		String selectionArgs[] = { SubjectIndex, SampleID };

		return DBService.getSingleCoumn("Answer", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 上一題題號、受訪者編號 查詢題號
	 * 
	 * @param SubjectIndex
	 * @param SampleID
	 * @param context
	 * @return
	 */
	public static String getSubjectIndex(String Previous, String SampleID,
			Context context) {

		String columns[] = { "Previous" };

		StringBuilder selection = new StringBuilder();
		selection.append("SubjectIndex = ? and ");
		selection.append("SampleID = ?");

		String selectionArgs[] = { Previous, SampleID };

		return DBService.getSingleCoumn("Answer", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問題編號、題號、受訪者編號 查詢 答案
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param SampleID
	 * @param context
	 * @return
	 */
	public static String getAnswer(String SurveyGuid, String SubjectIndex,
			String SampleID, Context context) {

		String columns[] = { "Answer" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ? and ");
		selection.append("SampleID = ?");
		String selectionArgs[] = { SurveyGuid, SubjectIndex, SampleID };
		return DBService.getSingleCoumn("Answer", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}
	
	/**
	 * 依據 問題編號、題號、受訪者編號 查詢 答案 回傳答案陣列
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param SampleID
	 * @param context
	 * @return array
	 */
	public static HashMap<String, String> getAnswerArray(String SurveyGuid, String SubjectIndex,
			String SampleID, Context context) {

		String columns[] = { "Answer" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ? and ");
		selection.append("SampleID = ?");
		String selectionArgs[] = { SurveyGuid, SubjectIndex, SampleID };
		return DBService.getHashMapCoumn("Answer", columns,
				selection.toString(), selectionArgs, null, null, null,
				context);
	}

	/**
	 * 依據 受訪者編號 查詢 答案筆數
	 * 
	 * @param SampleID
	 * @param context
	 * @return
	 */
	public static int getLength(String SampleID, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SampleID = ?");

		String selectionArgs[] = { SampleID };

		String groupBy = "SubjectIndex";

		return DBService.getLength("Answer", null, selection.toString(),
				selectionArgs, groupBy, null, null, context);
	}

	/**
	 * 取得最後一個作答的題號
	 * 
	 * @param SampleID
	 * @param context
	 * @return
	 */
	public static String getLastSubjectIndex(String SampleID, Context context) {
		String columns[] = { "SubjectIndex" };

		StringBuilder selection = new StringBuilder();
		selection.append("SampleID = ? and ");
		selection.append(" (SubjectIndex != null or ");
		selection.append(" SubjectIndex != '' )");

		String selectionArgs[] = { SampleID };

		String orderBy = "serial desc";

		String limit = "1";

		return DBService.getSingleCoumn("Answer", columns,
				selection.toString(), selectionArgs, null, null, orderBy,
				limit, context);
	}
}
