package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.dto.DynamicOptionDto;

import android.content.ContentValues;
import android.content.Context;

public class DynamicOptionService {

	/**
	 * 新增動態選項資料
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdInsert(DynamicOptionDto dto, Context context) {
		ContentValues Data = new ContentValues();
		Data.put("SurveyGuid", dto.getSurveyGuid());
		Data.put("dyoptionguid", dto.getDyoptionguid());
		Data.put("subject_accordingKey", dto.getSubject_accordingKey());
		Data.put("subject_subjectIndex", dto.getSubject_subjectIndex());
		Data.put("accordingValue", dto.getAccordingValue());
		Data.put("value", dto.getValue());
		Data.put("subject_index", dto.getSubject_index());
		Data.put("text", dto.getText());

		DBService.insertTableData("DynamicOption", Data, context);
	}

	/**
	 * 依據 動態選項編號 查詢 所需欄位資料
	 * 
	 * @param getCoumnname
	 * @param Dyoptionguid
	 * @param context
	 * @return
	 */
	public static String getDynamicOption(String getCoumnname,
			String Dyoptionguid, Context context) {

		String columns[] = { getCoumnname };

		StringBuilder selection = new StringBuilder();
		selection.append("dyoptionguid = ?");

		String selectionArgs[] = { Dyoptionguid };
		return DBService.getSingleCoumn("DynamicOption", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 動態選項編號 查詢 動態選項
	 * 
	 * @param Dyoptionguid
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryAllDynamicOption(
			String Dyoptionguid, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("dyoptionguid = ?");

		String selectionArgs[] = { Dyoptionguid };

		return DBService.getListCoumn("DynamicOption", null,
				selection.toString(), selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 問卷編號、動態選項編號、動態選項對應值 查詢 動態選項
	 * 
	 * @param SurveyGuid
	 * @param Dyoptionguid
	 * @param ZipCode
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryAllDynamicOption(
			String Dyoptionguid, String ZipCode, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("dyoptionguid = ? and ");
		selection.append("accordingValue = ?");

		String selectionArgs[] = { Dyoptionguid, ZipCode };

		return DBService.getListCoumn("DynamicOption", null,
				selection.toString(), selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 動態選項編號 查詢 資料筆數
	 * 
	 * @param dyoptionguid
	 * @param context
	 * @return
	 */
	public static int getLength(String dyoptionguid, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("dyoptionguid = ?");

		String selectionArgs[] = { dyoptionguid };

		return DBService.getLength("DynamicOption", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 動態選項編號 刪除 動態選項
	 * 
	 * @param dyoptionguid
	 * @param context
	 */
	public static void deleteDynamicOption(String dyoptionguid, Context context) {

		StringBuilder whereClause = new StringBuilder();
		whereClause.append("dyoptionguid = ?");

		String whereArgs[] = { dyoptionguid };

		DBService.deleteTableData("DynamicOption", whereClause.toString(),
				whereArgs, context);
	}
}
