package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.dto.LogDto;
import android.content.ContentValues;
import android.content.Context;

public class LogService {

	/**
	 * 依據 問卷編號、受訪者編號 查詢 系統記錄
	 * 
	 * @param SurveyGuid
	 * @param SampleID
	 * @param context
	 * @return
	 */
	public ArrayList<HashMap<String, String>> queryLogdata(String SurveyGuid,
			String SampleGuid, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SampleGuid = ?");

		String selectionArgs[] = { SurveyGuid, SampleGuid };

		return DBService.getListCoumn("SampleLog", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 問卷編號、受訪者編號 查詢 系統記錄
	 * 
	 * @param SurveyGuid
	 * @param SampleID
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryLog(
			String SurveyGuid, String SampleGuid, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SampleGuid = ?");

		String selectionArgs[] = { SurveyGuid, SampleGuid };

		return DBService.getListCoumn("SampleLog", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}
	
	/**
	 * 依據 問卷編號、受訪者編號 查詢 最新的次數編號
	 * Add by Awei 2013/05/23 V1.1.18
	 * @param SurveyGuid
	 * @param SampleID
	 * @param context
	 * @return
	 */
	public static String queryLatestSIndex(
			String SurveyGuid, String SampleGuid, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SampleGuid = ?");
		selection.append("");

		String selectionArgs[] = { SurveyGuid, SampleGuid };
		String columnsArgs[] = { "Sindex" };
		
		String orderBy = "Sindex DESC";
		String limit = "1";
		
		return DBService.getSingleCoumn("SampleLog", columnsArgs, selection.toString(),
				selectionArgs, null, null, orderBy, limit, context);
		
	}

	/**
	 * 更新樣本作答記錄資料
	 * 
	 * @param dto
	 * @param serial
	 * @param context
	 */
	public static void cmdUpdata(LogDto dto, String serial, Context context) {
		ContentValues Data = new ContentValues();
		if (dto.getEnd() != null && !dto.getEnd().trim().equals("")) {
			Data.put("End", dto.getEnd());
		}

		StringBuilder whereClause = new StringBuilder();
		whereClause.append(" serial = ?");

		String whereArgs[] = { serial };

		DBService.updataTableData("SampleLog", Data, whereClause.toString(),
				whereArgs, context);
	}

	/**
	 * 新增樣本作答記錄資料
	 * 
	 * @param dto
	 * @param context
	 */
	public static String cmdInsert(LogDto dto, Context context) {

		ContentValues Data = new ContentValues();
		Data.put("SampleGuid", dto.getSampleGuid());
		Data.put("SurveyGuid", dto.getSurveyGuid());
		Data.put("Sindex", dto.getSindex());
		Data.put("Start", dto.getStart());
		Data.put("End", dto.getEnd());

		DBService.insertTableData("SampleLog", Data, context);

		String columns[] = { "serial" };

		StringBuilder selection = new StringBuilder();
		selection.append("SampleGuid = ? and ");
		selection.append("SurveyGuid = ? and ");
		selection.append("Sindex = ? and ");
		selection.append("Start = ? and ");
		selection.append("End = ?");

		String selectionArgs[] = { dto.getSampleGuid(), dto.getSurveyGuid(),
				dto.getSindex(), dto.getStart(), dto.getEnd() };
		return DBService.getSingleCoumn("SampleLog", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}
}
