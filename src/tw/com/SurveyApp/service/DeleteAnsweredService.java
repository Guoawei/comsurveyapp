package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.view.StartView;

import android.content.Context;

public class DeleteAnsweredService {

	public static void deleteAnswerAnsweredForSingleAndGroupPass(String surveyGuid, String sampleID, String nowSubjectSerial,
			String passSubjectSerial, Context context) {
		ArrayList<HashMap<String, String>> deleteSerialArray = _getDeleteSerialArray(surveyGuid, nowSubjectSerial, passSubjectSerial,
				context);
		
		for(int i=0 ; i<deleteSerialArray.size();i++){
			String deleteSubjectIndex = deleteSerialArray.get(i).get("SubjectIndex");
			System.out.println(i+" deleteSubjectIndex:"+deleteSubjectIndex);
			
			AnswerService.deleteAnswer(surveyGuid, deleteSerialArray.get(i).get("SubjectIndex"), sampleID, context);
			
			System.out.println("刪除"+deleteSubjectIndex+"的答案"+" : "+"Serial="+deleteSerialArray.get(i).get("serial"));
		}
		
	}
	
	private static ArrayList<HashMap<String, String>> _getDeleteSerialArray(String surveyGuid, String nowSubjectSerial,
			String passSubjectSerial, Context context) {
		
		return SubjectService.listDeleteAnswerSerial(surveyGuid, 
				nowSubjectSerial, passSubjectSerial, context);
	}

	public static void deleteAnswerAnsweredForDynamicOptionAnswer(String SurveyGuid, String SampleID, String SubjectIndex,
			Context context) {
		AnswerService.deleteAnswer(SurveyGuid, SubjectIndex, SampleID, context);
		
		System.out.println("刪除"+SubjectIndex+"的答案");
	}
}
