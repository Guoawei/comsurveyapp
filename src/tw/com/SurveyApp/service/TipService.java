package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.dto.TipDto;

import android.content.ContentValues;
import android.content.Context;

public class TipService {

	/**
	 * 新增提示卡資料
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdInsert(TipDto dto, Context context) {
		ContentValues Data = new ContentValues();
		Data.put("SurveyGuid", dto.getSurveyGuid());
		Data.put("SubjectIndex", dto.getSubjectIndex());
		Data.put("tip_index", dto.getTip_index());
		Data.put("tip_name", dto.getTip_name());
		Data.put("tip_image", dto.getTip_image());
		Data.put("tip_text", dto.getTip_text());

		DBService.insertTableData("Tip", Data, context);
	}

	/**
	 * 依據 問卷編號、題號 查詢 提示卡內容
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryAllTip(
			String SurveyGuid, String SubjectIndex, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		return DBService.getListCoumn("Tip", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 問卷編號 刪除 提示卡資料
	 * 
	 * @param SurveyGuid
	 * @param context
	 */
	public static void deleteTip(String SurveyGuid, Context context) {

		StringBuilder whereClause = new StringBuilder();
		whereClause.append("SurveyGuid = ?");

		String whereArgs[] = { SurveyGuid };

		DBService.deleteTableData("Tip", whereClause.toString(), whereArgs,
				context);
	}

	/**
	 * 依據 問題編號、題號 刪除 提示卡資料
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 */
	public static void deleteTip(String SurveyGuid, String SubjectIndex,
			Context context) {

		StringBuilder whereClause = new StringBuilder();
		whereClause.append("SurveyGuid = ? and ");
		whereClause.append("SubjectIndex = ?");

		String whereArgs[] = { SurveyGuid, SubjectIndex };

		DBService.deleteTableData("Tip", whereClause.toString(), whereArgs,
				context);
	}
}
