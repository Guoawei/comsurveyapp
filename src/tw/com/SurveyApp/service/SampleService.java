package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.dto.SampleDto;

import android.content.ContentValues;
import android.content.Context;

public class SampleService {

	/**
	 * 刪除樣本資料
	 * 
	 * @param SurveyGuid
	 * @param context
	 */
	public void cmdDelete(String SurveyGuid, Context context) {
		StringBuilder whereClause = new StringBuilder();
		whereClause.append("SurveyGuid = ?");

		String whereArgs[] = { SurveyGuid };

		DBService.deleteTableData("Sample", whereClause.toString(), whereArgs,
				context);
	}

	/**
	 * 資改樣本資料
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdUpdata(SampleDto dto, String SampleGuid,
			String SurveyGuid, Context context) {

		ContentValues Data = new ContentValues();
		if (dto.getName() != null && !dto.getName().trim().equals("")) {
			Data.put("Name", dto.getName());
		}
		if (dto.getAge() != null && !dto.getAge().trim().equals("")) {
			Data.put("Age", dto.getAge());
		}
		if (dto.getZipCode() != null && !dto.getZipCode().trim().equals("")) {
			Data.put("ZipCode", dto.getZipCode());
		}
		if (dto.getAddress() != null && !dto.getAddress().trim().equals("")) {
			Data.put("Address", dto.getAddress());
		}
		if (dto.getPhone() != null && !dto.getPhone().trim().equals("")) {
			Data.put("Phone", dto.getPhone());
		}
		if (dto.getIsupdate() != null && !dto.getIsupdate().trim().equals("")) {
			Data.put("isupdate", dto.getIsupdate());
		}
		if (dto.getAnswerDate() != null
				&& !dto.getAnswerDate().trim().equals("")) {
			Data.put("AnswerDate", dto.getAnswerDate());
		}

		StringBuilder whereClause = new StringBuilder();
		whereClause.append(" SampleGuid = ? and ");
		whereClause.append(" SurveyGuid = ?");

		String whereArgs[] = { SampleGuid, SurveyGuid };

		DBService.updataTableData("Sample", Data, whereClause.toString(),
				whereArgs, context);
	}

	/**
	 * 新增樣本資料
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdInsert(SampleDto dto, Context context) {

		ContentValues Data = new ContentValues();
		Data.put("Name", dto.getName());
		Data.put("Age", dto.getAge());
		Data.put("ZipCode", dto.getZipCode());
		Data.put("Address", dto.getAddress());
		Data.put("Phone", dto.getPhone());
		Data.put("isupdate", dto.getIsupdate());
		Data.put("AnswerDate", dto.getAnswerDate());
		Data.put("SampleGuid", dto.getSampleGuid());
		Data.put("SurveyGuid", dto.getSurveyGuid());
		Data.put("MainPriority", dto.getMainPriority());
		Data.put("SubPriority", dto.getSubPriority());

		DBService.insertTableData("Sample", Data, context);
	}

	/**
	 * 依據 問卷編號、是否上傳 查詢 樣本
	 * 
	 * @param SurveyGuid
	 * @param isupdate
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> querySample(
			String SurveyGuid, Context context) {

		String columns[] = { "SampleGuid" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ?");

		String selectionArgs[] = { SurveyGuid };
		
		String orderBy = "MainPriority, SubPriority ASC";
		
		return DBService.getListCoumn("Sample", columns, selection.toString(),
				selectionArgs, null, null, orderBy, context);
	}

	/**
	 * 依據 問卷id、樣本id 查詢 是否上鎖
	 * 
	 * @param SurveyGuid
	 * @param SampleGuid
	 * @param context
	 * @return
	 */
	public static boolean getIsUpdata(String SurveyGuid, String SampleGuid,
			Context context) {
		String columns[] = { "isupdate" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SampleGuid = ?");

		String selectionArgs[] = { SurveyGuid, SampleGuid };

		String isupdate = DBService.getSingleCoumn("Sample", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);

		if (isupdate.equals("0")) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 取得樣本人數
	 * 
	 * @param SurveyGuid
	 * @param context
	 * @return
	 */
	public static int getSampleLength(String SurveyGuid, Context context) {
		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ?");

		String selectionArgs[] = { SurveyGuid };

		return DBService.getLength("Sample", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 取得完成作答人數
	 * 
	 * @param context
	 * @return
	 */
	public static int getLength(String SurveyGuid, Context context) {
		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("AnswerDate != ?");

		String selectionArgs[] = { SurveyGuid, "" };

		return DBService.getLength("Sample", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 樣本編號 查詢 樣本地區編號
	 * 
	 * @param SampleID
	 * @param context
	 * @return
	 */
	public static String getZipCode(String SampleGuid, Context context) {
		String columns[] = { "ZipCode" };

		StringBuilder selection = new StringBuilder();
		selection.append("SampleGuid = ?");

		String selectionArgs[] = { SampleGuid };

		return DBService.getSingleCoumn("Sample", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 取得 樣本名稱
	 * 
	 * @param SampleGuid
	 * @param context
	 * @return
	 */
	public String getName(String SampleGuid, Context context) {
		String columns[] = { "Name" };

		StringBuilder selection = new StringBuilder();
		selection.append("SampleGuid = ?");

		String selectionArgs[] = { SampleGuid };

		return DBService.getSingleCoumn("Sample", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問廠編號 查詢 所有樣本
	 * 
	 * @param SurveyGuid
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> querySampleAll(
			String SurveyGuid, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ?");

		String selectionArgs[] = { SurveyGuid };

		return DBService.getListCoumn("Sample", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}
}
