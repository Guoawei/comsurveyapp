package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.dto.StatusDataDto;
import tw.com.SurveyApp.dto.StatusDto;
import android.content.ContentValues;
import android.content.Context;

public class StatusService {
	
	public String getStatus() {
		StringBuilder StatusAll = new StringBuilder();
		StatusAll.append("1,繼續訪問,0:");
		StatusAll.append("2,需再訪問,0:");
		StatusAll.append("3,不住原址,0:");
		StatusAll.append("4,地址無法訪問或無此人,0:");
		StatusAll.append("5,受訪者拒訪或無法接受訪問,0:");
		StatusAll.append("6,其他,0:");

		StatusAll.append("21,因故中止訪問（已經訪問一部分了，可以改天再去訪問）,2:");
		StatusAll.append("22,暫時不便接受訪問（還沒開始訪問，可以改天再去訪問）,2:");
		StatusAll.append("23,外出，訪問期間會回來,2:");
		StatusAll.append("24,語言不通（請聯絡輔導員確認是否應使用此代碼）,2:");
		StatusAll.append("25,無人在家,2:");
		StatusAll.append("26,受訪者以外的人代為拒絕（請設法找到受訪者本人）,2:");
		StatusAll.append("27,管理員阻止（請出示所有公文或留本中心聯絡電話）,2:");

		StatusAll.append("31,「需再訪」：受訪者不住在戶籍地，有問到新電話及新地址(如因工作、求學、房屋出租等原因),3:");
		StatusAll.append("32,「需再訪」：受訪者不住在戶籍地，只問到新電話或新地址(如因工作、求學、房屋出租等原因),3:");
		StatusAll.append("33,「不需再訪」：受訪者不住在戶籍地，沒有問到新電話及新地址(如因工作、求學、房屋出租等原因),3:");

		StatusAll.append("41,軍事單位、醫院、療養院、學校、職訓中心、監獄等機構,4:");
		StatusAll.append("42,空屋（如因房子改建/出售/出租等，無人居住，經村里長、警察等證實）,4:");
		StatusAll.append("43,查無此地址（經村里長、警察等證實）,4:");
		StatusAll.append("44,因環境惡劣，無法抵達訪區(如山崩、地震或其他天災等),4:");
		StatusAll.append("45,該地址查無此人,4:");
		StatusAll.append("46,無法進入家戶/建物,4:");

		StatusAll.append("51,受訪者拒絕接受訪問,5:");
		StatusAll.append("52,受訪者中途拒訪,5:");
		StatusAll.append("53,受訪者以外的人中途拒訪,5:");
		StatusAll.append("54,受訪者因生理/心理問題，無法接受訪問(如生重病、重聽、精神疾病),5:");
		StatusAll.append("55,外出(如旅遊、遊學、出差等)，訪問期間不會回來,5:");
		StatusAll.append("56,外出不知去向、失蹤,5:");
		StatusAll.append("57,死亡,5:");
		StatusAll.append("58,服刑,5:");
		StatusAll.append("59,受訪者戶籍遷出(因結婚、移民等),5:");
		StatusAll.append("60,服兵役,5:");

		StatusAll.append("96,非合格受訪者（年齡不符）,6:");
		StatusAll.append("97,配額已滿,6:");
		StatusAll.append("98,,6:");

		return StatusAll.toString();
	}

	/**
	 * 依據 階層 查詢所有 狀態
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public ArrayList<HashMap<String, String>> queryStatus(String Step,
			Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("Step = ?");

		String selectionArgs[] = { Step };

		return DBService.getListCoumn("StatusLog", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 查詢樣本狀態記錄筆數
	 * 
	 * @param SampleID
	 * @param context
	 * @return
	 */
	public int getLength(Context context) {
		return DBService.getLength("StatusLog", null, null, null, null, null,
				null, context);
	}

	/**
	 * 新增樣本狀態記錄資料
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdInsert(StatusDto dto, Context context) {

		ContentValues Data = new ContentValues();

		Data.put("StatusNumber", dto.getStatusNumber());
		Data.put("Step", dto.getStep());
		Data.put("Name", dto.getName());

		DBService.insertTableData("StatusLog", Data, context);
	}

	/**
	 * 新增狀態記錄資料
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdInsertData(StatusDataDto dto, Context context) {

		ContentValues Data = new ContentValues();

		Data.put("SampleGuid", dto.getSampleGuid());
		Data.put("SurveyGuid", dto.getSurveyGuid());
		Data.put("SituationCode", dto.getSituationCode());
		Data.put("Comment", dto.getComment());
		Data.put("CDateTime", dto.getCDateTime());

		DBService.insertTableData("StatusData", Data, context);
	}
	
	/**
	 * 刪除樣本狀態記錄資料
	 * 
	 * @param context
	 * 
	 */
	public static void cmdDelete(Context context) {

		DBService.deleteTableData("StatusLog",null,null,context);
	}

	/**
	 * 依據 問卷編號、受訪者編號 查詢 系統記錄
	 * 
	 * @param SurveyGuid
	 * @param SampleID
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryStatus(
			String SurveyGuid, String SampleGuid, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SampleGuid = ?");

		String selectionArgs[] = { SurveyGuid, SampleGuid };

		return DBService.getListCoumn("StatusData", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}
}
