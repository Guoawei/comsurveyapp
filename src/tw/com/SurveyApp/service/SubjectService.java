package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.dto.SubjectDto;

import android.content.ContentValues;
import android.content.Context;

public class SubjectService {

	/**
	 * 修改題目資料
	 * 
	 * @param dto
	 * @param where
	 * @param context
	 */
	public static void cmdUpdata(SubjectDto dto, String SurveyGuid,
			String SubjectIndex, Context context) {

		ContentValues Data = new ContentValues();
		if (dto.getSubjectType() != null
				&& !dto.getSubjectType().trim().equals("")) {
			Data.put("SubjectType", dto.getSubjectType());
		}
		if (dto.getSubjectTitle() != null
				&& !dto.getSubjectTitle().trim().equals("")) {
			Data.put("SubjectTitle", dto.getSubjectTitle());
		}
		if (dto.getMultiSelectedLimit() != null
				&& !dto.getMultiSelectedLimit().trim().equals("")) {
			Data.put("MultiSelectedLimit", dto.getMultiSelectedLimit());
		}
		if (dto.getDynamicOption() != null
				&& !dto.getDynamicOption().trim().equals("")) {
			Data.put("DynamicOption", dto.getDynamicOption());
		}
		if (dto.getDynamicOptionAnswer() != null
				&& !dto.getDynamicOptionAnswer().trim().equals("")) {
			Data.put("DynamicOptionAnswer", dto.getDynamicOptionAnswer());
		}
		if (dto.getGroupOption() != null
				&& !dto.getGroupOption().trim().equals("")) {
			Data.put("GroupOption", dto.getGroupOption());
		}
		if (dto.getSubjectOpiton() != null
				&& !dto.getSubjectOpiton().trim().equals("")) {
			Data.put("SubjectOpiton", dto.getSubjectOpiton());
		}
		if (dto.getSubjectIsRandom() != null
				&& !dto.getSubjectIsRandom().trim().equals("")) {
			Data.put("SubjectIsRandom", dto.getSubjectIsRandom());
		}
		if (dto.getOptionOutputForm() != null
				&& !dto.getOptionOutputForm().trim().equals("")) {
			Data.put("OptionOutputForm", dto.getOptionOutputForm());
		}

		StringBuilder whereClause = new StringBuilder();
		whereClause.append("SurveyGuid = ? and ");
		whereClause.append("SubjectIndex = ?");

		String whereArgs[] = { SurveyGuid, SubjectIndex };

		DBService.updataTableData("Subject", Data, whereClause.toString(),
				whereArgs, context);
	}

	/**
	 * 新增題目資料
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdInsert(SubjectDto dto, Context context) {
		ContentValues Data = new ContentValues();
		Data.put("SubjectType", dto.getSubjectType());
		Data.put("SubjectTitle", dto.getSubjectTitle());
		Data.put("MultiSelectedLimit", dto.getMultiSelectedLimit());
		Data.put("DynamicOption", dto.getDynamicOption());
		Data.put("DynamicOptionAnswer", dto.getDynamicOptionAnswer());
		Data.put("GroupOption", dto.getGroupOption());
		Data.put("SubjectOpiton", dto.getSubjectOpiton());
		Data.put("SubjectIsRandom", dto.getSubjectIsRandom());
		Data.put("SurveyGuid", dto.getSurveyGuid());
		Data.put("SubjectIndex", dto.getSubjectIndex());
		Data.put("OptionOutputForm", dto.getOptionOutputForm());

		DBService.insertTableData("Subject", Data, context);
	}

	/**
	 * 依據 問題編號、題號 查詢 問卷代碼
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static String getSerial(String SurveyGuid, String SubjectIndex,
			Context context) {

		String columns[] = { "serial" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		return DBService.getSingleCoumn("Subject", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問題編號、題號 查詢 亂數選項
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static String getSubjectIsRandom(String SurveyGuid,
			String SubjectIndex, Context context) {

		String columns[] = { "SubjectIsRandom" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		return DBService.getSingleCoumn("Subject", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問題編號、題號 查詢 多選限制
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static String getMultiSelectedLimit(String SurveyGuid,
			String SubjectIndex, Context context) {

		String columns[] = { "MultiSelectedLimit" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		return DBService.getSingleCoumn("Subject", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問卷編號、題號 查詢 群組題目編號
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static String getGroupOption(String SurveyGuid, String SubjectIndex,
			Context context) {

		String columns[] = { "GroupOption" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		return DBService.getSingleCoumn("Subject", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問卷編號、題號 查詢 選項顯示方式
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static String getOptionOutputForm(String SurveyGuid,
			String SubjectIndex, Context context) {

		String columns[] = { "OptionOutputForm" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		return DBService.getSingleCoumn("Subject", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問卷編號、題號 查詢 動態選項
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static String getDynamicOption(String SurveyGuid,
			String SubjectIndex, Context context) {

		String columns[] = { "DynamicOption" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		return DBService.getSingleCoumn("Subject", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問卷編號、問卷代碼 查詢 題號
	 * 
	 * @param SurveyGuid
	 * @param Serial
	 * @param context
	 * @return
	 */
	public static String getSubjectIndex(String SurveyGuid, String Serial,
			Context context) {

		String columns[] = { "SubjectIndex" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("serial = ?");

		String selectionArgs[] = { SurveyGuid, Serial };

		return DBService.getSingleCoumn("Subject", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}

	/**
	 * 依據 問卷編號 查詢 所有的題目
	 * 
	 * @param SurveyGuid
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryAllSubject(
			String SurveyGuid, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ?");

		String selectionArgs[] = { SurveyGuid };
		
//		String orderBySubjectIndex = "serial ASC"; //Add by Awei 2013/04/13

		return DBService.getListCoumn("Subject", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 問卷編號、題號 查詢 題目
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryAllSubject(
			String SurveyGuid, String SubjectIndex, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		return DBService.getListCoumn("Subject", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 問卷編號 查詢 資料筆數
	 * 
	 * @param SurveyGuid
	 * @param context
	 * @return
	 */
	public static int getLength(String SurveyGuid, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ?");

		String selectionArgs[] = { SurveyGuid };

		return DBService.getLength("Subject", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 問卷編號、題號 查詢 資料筆數
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static int getLength(String SurveyGuid, String SubjectIndex,
			Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("SubjectIndex = ?");

		String selectionArgs[] = { SurveyGuid, SubjectIndex };

		return DBService.getLength("Subject", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}
	
	/**
	 * 依據 當題序號、跳題的序號
	 * 
	 * @param SurveyGuid
	 * @param SubjectIndex
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> listDeleteAnswerSerial(String SurveyGuid, String nowSubjectSerial, String passSubjectSerial,
			Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("serial > ? and ");
		selection.append("serial < ?");

		String selectionArgs[] = { SurveyGuid, nowSubjectSerial, passSubjectSerial };

		return DBService.getListCoumn("Subject", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}
}
