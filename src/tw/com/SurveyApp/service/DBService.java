package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.data.DBConnection;
import tw.com.SurveyApp.dto.AnswerDto;
import tw.com.SurveyApp.dto.DynamicOptionByAnswerDto;
import tw.com.SurveyApp.dto.DynamicOptionDto;
import tw.com.SurveyApp.dto.GroupOptionDto;
import tw.com.SurveyApp.dto.GroupPsaaDto;
import tw.com.SurveyApp.dto.InterviewDto;
import tw.com.SurveyApp.dto.LogDto;
import tw.com.SurveyApp.dto.OptionDto;
import tw.com.SurveyApp.dto.QuestionnaireDto;
import tw.com.SurveyApp.dto.SampleDto;
import tw.com.SurveyApp.dto.SinglePsaaDto;
import tw.com.SurveyApp.dto.StatusDataDto;
import tw.com.SurveyApp.dto.StatusDto;
import tw.com.SurveyApp.dto.SubjectDto;
import tw.com.SurveyApp.dto.TipDto;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class DBService {
	private static SQLiteDatabase db = null;
	private static DBConnection conn = null;

	/**
	 * 取得資料長度
	 * 
	 * @param Tablename
	 * @param Coumnname
	 * @param values
	 * @param _db
	 * @return
	 */
	public static int getLength(String Tablename, String[] columns,
			String selection, String[] selectionArgs, String groupBy,
			String having, String orderBy, Context context) {
		conn = new DBConnection(context);
		db = conn.getReadableDatabase();

		Cursor cursor = db.query(Tablename, columns, selection, selectionArgs,
				groupBy, having, orderBy);

		int Count = cursor.getCount();

		conn.close();
		db.close();

		return Count;
	}

	/**
	 * 新增資料
	 * 
	 * @param TableName
	 * @param Data
	 * @param context
	 */
	public static void insertTableData(String TableName, ContentValues Data,
			Context context) {

		conn = new DBConnection(context);
		db = conn.getWritableDatabase();

		db.insert(TableName, null, Data);
		conn.close();
		db.close();
	}

	/**
	 * 修改資料
	 * 
	 * @param TableName
	 * @param Data
	 * @param whereClause
	 * @param whereArgs
	 * @param context
	 */
	public static void updataTableData(String TableName, ContentValues Data,
			String whereClause, String whereArgs[], Context context) {
		conn = new DBConnection(context);
		db = conn.getWritableDatabase();

		db.update(TableName, Data, whereClause, whereArgs);
		conn.close();
		db.close();
	}

	/**
	 * 刪除資料
	 * 
	 * @param TableName
	 * @param whereClause
	 * @param whereArgs
	 * @param context
	 */
	public static void deleteTableData(String TableName, String whereClause,
			String whereArgs[], Context context) {
		conn = new DBConnection(context);
		db = conn.getWritableDatabase();

		db.delete(TableName, whereClause, whereArgs);
		conn.close();
		db.close();
	}

	/**
	 * 取得 表格中 各個欄位名稱
	 * 
	 * @param Tablename
	 * @return
	 */
	private static String[] getCoumnname(String Tablename) {
		String Coumnname[] = null;
		if (Tablename.equals("Questionnaire")) {
			Coumnname = new QuestionnaireDto().getColumnname();
		} else if (Tablename.equals("Sample")) {
			Coumnname = new SampleDto().getColumnname();
		} else if (Tablename.equals("Option")) {
			Coumnname = new OptionDto().getColumnname();
		} else if (Tablename.equals("Subject")) {
			Coumnname = new SubjectDto().getColumnname();
		} else if (Tablename.equals("SampleColumn")) {
			Coumnname = new SampleDto().getSampleColumnname();
		} else if (Tablename.equals("Interview")) {
			Coumnname = new InterviewDto().getColumnname();
		} else if (Tablename.equals("Answer")) {
			Coumnname = new AnswerDto().getColumnname();
		} else if (Tablename.equals("GroupPsaa")) {
			Coumnname = new GroupPsaaDto().getColumnname();
		} else if (Tablename.equals("Tip")) {
			Coumnname = new TipDto().getColumnname();
		} else if (Tablename.equals("SinglePsaa")) {
			Coumnname = new SinglePsaaDto().getColumnname();
		} else if (Tablename.equals("DynamicOption")) {
			Coumnname = new DynamicOptionDto().getColumnname();
		} else if (Tablename.equals("DynamicOptionByAnswer")) {
			Coumnname = new DynamicOptionByAnswerDto().getColumnname();
		} else if (Tablename.equals("GroupOption")) {
			Coumnname = new GroupOptionDto().getColumnname();
		} else if (Tablename.equals("StatusLog")) {
			Coumnname = new StatusDto().getColumnname();
		} else if (Tablename.equals("StatusData")) {
			Coumnname = new StatusDataDto().getColumnname();
		} else if (Tablename.equals("SampleLog")) {
			Coumnname = new LogDto().getColumnname();
		}
		return Coumnname;
	}

	/**
	 * 查詢 List 結果
	 * 
	 * @param Tablename
	 * @param columns
	 * @param selection
	 * @param selectionArgs
	 * @param groupBy
	 * @param having
	 * @param orderBy
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> getListCoumn(
			String Tablename, String columns[], String selection,
			String selectionArgs[], String groupBy, String having,
			String orderBy, Context context) {
		conn = new DBConnection(context);
		db = conn.getReadableDatabase();

		Cursor cursor = db.query(Tablename, columns, selection, selectionArgs,
				groupBy, having, orderBy);

		ArrayList<HashMap<String, String>> allData = new ArrayList<HashMap<String, String>>();

		String Coumnnames[] = getCoumnname(Tablename);
		while (cursor.moveToNext()) {
			if (columns == null) {

				HashMap<String, String> single = new HashMap<String, String>();

				single.put("serial", cursor.getInt(0) + "");
				int i = 1;
				for (String ss : Coumnnames) {
					single.put(ss, cursor.getString(i++));
				}
				allData.add(single);
			} else {
				HashMap<String, String> single = new HashMap<String, String>();

				int i = 0;
				for (String ss : columns) {
					single.put(ss, cursor.getString(i++));
				}
				allData.add(single);
			}
		}

		conn.close();
		db.close();

		return allData;
	}

	/**
	 * 查詢 單一欄位
	 * 
	 * @param Tablename
	 * @param columns
	 * @param selection
	 * @param selectionArgs
	 * @param groupBy
	 * @param having
	 * @param orderBy
	 * @param limit
	 * @param context
	 * @return
	 */
	public static String getSingleCoumn(String Tablename, String columns[],
			String selection, String selectionArgs[], String groupBy,
			String having, String orderBy, String limit, Context context) {
		conn = new DBConnection(context);
		db = conn.getReadableDatabase();

		Cursor cursor = db.query(Tablename, columns, selection, selectionArgs,
				groupBy, having, orderBy, limit);

		String Coumn = "";

		if (cursor.moveToFirst()) {

			if (columns[0].equals("serial")) {
				Coumn = String.valueOf(cursor.getInt(0));
			} else {
				Coumn = cursor.getString(0);
			}
		}

		conn.close();
		db.close();

		return Coumn;
	}

	/**
	 * 查詢 HashMap 結果
	 * 
	 * @param Tablename
	 * @param columns
	 * @param selection
	 * @param selectionArgs
	 * @param groupBy
	 * @param having
	 * @param orderBy
	 * @param context
	 * @return
	 */
	public static HashMap<String, String> getHashMapCoumn(String Tablename,
			String columns[], String selection, String selectionArgs[],
			String groupBy, String having, String orderBy, Context context) {
		conn = new DBConnection(context);
		db = conn.getReadableDatabase();

		try {
			Cursor cursor = db.query(Tablename, columns, selection,
					selectionArgs, groupBy, having, orderBy);

			HashMap<String, String> single = new HashMap<String, String>();
			int i = 0;
			while (cursor.moveToNext()) {
				single.put(i + "", cursor.getString(0));
				i++;
			}
			cursor.close();
			return single;
		} finally {
			conn.close();
			db.close();
		}
	}

	/**
	 * 查詢樣本資料
	 * 
	 * @param SurveyGuid
	 * @param _db
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> querySample(
			String SurveyGuid, Context context) {
		conn = new DBConnection(context);
		db = conn.getReadableDatabase();

		ArrayList<HashMap<String, String>> allData = new ArrayList<HashMap<String, String>>();

		String Coumnnames[] = getCoumnname("SampleColumn");

		StringBuilder sql = new StringBuilder();
		sql.append("select * from  Questionnaire q ");
		sql.append(" left join Sample s on q.SurveyGuid = s.SurveyGuid ");
		sql.append(" where s.SurveyGuid = '%s' ");
		/*
		 *by Awei 2013/04/17 新增了名字的排序 
		 */
		sql.append(" order by s.MainPriority, s.SubPriority asc ");
		String sqlstr = String.format(sql.toString(), SurveyGuid);

		try {
			Cursor cursor = db.rawQuery(sqlstr, null);
			while (cursor.moveToNext()) {
				HashMap<String, String> single = new HashMap<String, String>();
				int i = 0;

				for (String ss : Coumnnames) {
					single.put(ss, cursor.getString(i++));
				}
				allData.add(single);
			}
		} catch (SQLiteException ex) {
			System.out.println(ex);
			ex.printStackTrace();
			return null;
		} finally {
			conn.close();
			db.close();
		}

		return allData;
	}

}
