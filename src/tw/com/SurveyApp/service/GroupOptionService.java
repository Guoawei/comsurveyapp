package tw.com.SurveyApp.service;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.dto.GroupOptionDto;

import android.content.ContentValues;
import android.content.Context;

public class GroupOptionService {

	/**
	 * 新增群組題目資料
	 * 
	 * @param dto
	 * @param context
	 */
	public static void cmdInsert(GroupOptionDto dto, Context context) {
		ContentValues Data = new ContentValues();
		Data.put("SurveyGuid", dto.getSurveyGuid());
		Data.put("GoGuid", dto.getGoGuid());
		Data.put("GroupId", dto.getGroupId());
		Data.put("GroupType", dto.getGroupType());
		Data.put("GroupIndex", dto.getGroupIndex());
		Data.put("GroupText", dto.getGroupText());
		Data.put("GroupValue", dto.getGroupValue());
		Data.put("isShowTextView", dto.getIsShowTextView());
		Data.put("textViewPosition", dto.getTextViewPosition());

		DBService.insertTableData("GroupOption", Data, context);
	}

	/**
	 * 依據 群組題目id 查詢 資料筆數
	 * 
	 * @param GoGuid
	 *            群組題目id
	 * @param context
	 * @return
	 */
	public static int getLength(String GoGuid, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("GoGuid = ?");

		String selectionArgs[] = { GoGuid };

		return DBService.getLength("GroupOption", null, selection.toString(),
				selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 群組題目id 刪除 群組題目
	 * 
	 * @param GoGuid
	 *            群組題目id
	 * @param context
	 */
	public static void deleteGroupOption(String GoGuid, Context context) {

		StringBuilder whereClause = new StringBuilder();
		whereClause.append("GoGuid = ?");

		String whereArgs[] = { GoGuid };

		DBService.deleteTableData("GroupOption", whereClause.toString(),
				whereArgs, context);
	}

	/**
	 * 依據 問卷id、群組題目id、群組答案值 查詢 群組題目分類
	 * 
	 * @param SurveyGuid
	 *            問卷id
	 * @param GoGuid
	 *            群組題目id
	 * @param GroupValue
	 *            群組答案值
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryTitle(
			String SurveyGuid, String GoGuid, String GroupValue, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("GoGuid = ? and ");
		selection.append("GroupValue = ? and ");
		selection.append("GroupType = ?");

		String selectionArgs[] = { SurveyGuid, GoGuid, GroupValue, "0" };

		return DBService.getListCoumn("GroupOption", null,
				selection.toString(), selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 問卷id、群組題目id、分類號 查詢 群組題目選單內容
	 * 
	 * @param SurveyGuid
	 *            問卷id
	 * @param GoGuid
	 *            群組題目id
	 * @param GroupType
	 *            分類號
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryOption(
			String SurveyGuid, String GoGuid, String GroupType, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("GoGuid = ? and ");
		selection.append("GroupType = ?");

		String selectionArgs[] = { SurveyGuid, GoGuid, GroupType };

		return DBService.getListCoumn("GroupOption", null,
				selection.toString(), selectionArgs, null, null, null, context);
	}

	/**
	 * 依據 問卷id、群組題目id、分類號 查詢 群組題目
	 * 
	 * @param SurveyGuid
	 *            問卷id
	 * @param GoGuid
	 *            群組題目id
	 * @param Type
	 *            分類號
	 * @param context
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> queryAllGroupOption(
			String SurveyGuid, String GoGuid, String[] Type, Context context) {

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("GoGuid = ? and ");
		selection.append("GroupType in (");
		for (String ss : Type) {
			selection.append(ss + ", ");
		}
		if (selection.toString().endsWith(", ")) {
			selection.delete(selection.length() - 2, selection.length());
		}
		selection.append(")");

		String selectionArgs[] = { SurveyGuid, GoGuid };

		return DBService.getListCoumn("GroupOption", null,
				selection.toString(), selectionArgs, null, null, "serial",
				context);
	}

	/**
	 * 依據 問卷id、群組選項id、群組答案值 查詢 分類順序
	 * 
	 * @param SurveyGuid
	 *            問卷id
	 * @param GoGuid
	 *            群組題目id
	 * @param GroupId
	 *            群組答案id
	 * @param context
	 * @return
	 */
	public String queryGroupIndex(String SurveyGuid, String GoGuid,
			String GroupId, Context context) {

		String columns[] = { "GroupIndex" };

		StringBuilder selection = new StringBuilder();
		selection.append("SurveyGuid = ? and ");
		selection.append("GoGuid = ? and ");
		selection.append("GroupId = ?");

		String selectionArgs[] = { SurveyGuid, GoGuid, GroupId };

		return DBService.getSingleCoumn("GroupOption", columns,
				selection.toString(), selectionArgs, null, null, null, null,
				context);
	}
}
