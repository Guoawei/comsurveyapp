package tw.com.SurveyApp.dto;

public class DynamicOptionByAnswerDto {
	private String serial;// 編號
	private String SurveyGuid;// 問卷編號
	private String DyaGuid;// 動態選項編號
	private String SubjectNumber;// 題號
	private String TargetAnswer;// 答案
	private String IsShow;// 顯示否
	private String TargetOptionIndex;// 目標選項
	private String IsOr;//是否為或的條件

	/**
	 * 取得各欄位名稱
	 * 
	 * @return
	 */
	public String[] getColumnname() {
		String Columnname[] = { "SurveyGuid", "DyaGuid", "SubjectNumber",
				"TargetAnswer", "IsShow", "TargetOptionIndex", "IsOr" };
		return Columnname;
	}

	/**
	 * 取得代碼
	 * 
	 * @return
	 */
	public String getSerial() {
		return serial;
	}

	/**
	 * 設定代碼
	 * 
	 * @param serial
	 */
	public void setSerial(String serial) {
		this.serial = serial;
	}

	/**
	 * 取得問卷編號
	 * 
	 * @return
	 */
	public String getSurveyGuid() {
		return SurveyGuid;
	}

	/**
	 * 設定問卷編號
	 * 
	 * @param surveyGuid
	 */
	public void setSurveyGuid(String surveyGuid) {
		SurveyGuid = surveyGuid;
	}

	/**
	 * 取得動態選項編號
	 * 
	 * @return
	 */
	public String getDyaGuid() {
		return DyaGuid;
	}

	/**
	 * 設定動態選項編號
	 * 
	 * @param dyaGuid
	 */
	public void setDyaGuid(String dyaGuid) {
		DyaGuid = dyaGuid;
	}

	/**
	 * 取得題號
	 * 
	 * @return
	 */
	public String getSubjectNumber() {
		return SubjectNumber;
	}

	/**
	 * 設定題號
	 * 
	 * @param subjectNumber
	 */
	public void setSubjectNumber(String subjectNumber) {
		SubjectNumber = subjectNumber;
	}

	/**
	 * 取得答案
	 * 
	 * @return
	 */
	public String getTargetAnswer() {
		return TargetAnswer;
	}

	/**
	 * 設定答案
	 * 
	 * @param targetAnswer
	 */
	public void setTargetAnswer(String targetAnswer) {
		TargetAnswer = targetAnswer;
	}

	/**
	 * 取的顯示否
	 * 
	 * @return
	 */
	public String getIsShow() {
		return IsShow;
	}

	/**
	 * 設定顯示否
	 * 
	 * @param isShow
	 */
	public void setIsShow(String isShow) {
		IsShow = isShow;
	}

	/**
	 * 取得目標選項
	 * 
	 * @return
	 */
	public String getTargetOptionIndex() {
		return TargetOptionIndex;
	}

	/**
	 * 設定目標選項
	 * 
	 * @param targetOptionIndex
	 */
	public void setTargetOptionIndex(String targetOptionIndex) {
		TargetOptionIndex = targetOptionIndex;
	}

	/**
	 * 取得是否為或的條件
	 * 
	 * @return
	 */
	public String getIsOr() {
		return IsOr;
	}

	/**
	 * 設定是否為或的條件
	 * 
	 * @param isOr
	 */
	public void setIsOr(String isOr) {
		IsOr = isOr;
	}
}
