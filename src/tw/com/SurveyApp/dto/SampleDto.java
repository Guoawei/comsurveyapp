package tw.com.SurveyApp.dto;

public class SampleDto {

	private int serial;
	private String SampleGuid;// 受訪者ID
	private String SurveyGuid;// 問卷ID
	private String Name;// 受訪者名稱
	private String Age;// 受訪者年齡
	private String ZipCode;// 地區編碼
	private String Address;// 受訪者地址
	private String Phone;// 受訪者電話
	private String isupdate;// 上傳否
	private String AnswerDate;// 上傳日期
	private String MainPriority;
	private String SubPriority;

	public SampleDto() {
	}

	/**
	 * 取得各欄位名稱
	 * 
	 * @return
	 */
	public String[] getColumnname() {
		String Columname[] = { "SampleGuid", "SurveyGuid", "name", "age",
				"ZipCode", "address", "phone", "isupdate", "AnswerDate","MainPriority","SubPriority" };
		return Columname;
	}

	public String[] getSampleColumnname() {
		String Coumnname[] = { "serial", "SurveyGuid", "Title", "GreetingText",
				"ThankText", "TotalRows", "Ver", "CDateTime", "UDateTime",
				"CompletedRow", "IntervieweeRows", "serial", "SampleGuid",
				"SurveyGuid", "name", "age", "ZipCode", "address", "phone",
				"isupdate", "AnswerDate", "MainPriority","SubPriority" };
		return Coumnname;
	}

	/**
	 * 取得受訪者ID
	 * 
	 * @return
	 */
	public String getSampleGuid() {
		return SampleGuid;
	}

	/**
	 * 設定受訪者ID
	 * 
	 * @param iD
	 */
	public void setSampleGuid(String sampleGuid) {
		SampleGuid = sampleGuid;
	}

	/**
	 * 取得編號
	 * 
	 * @return
	 */
	public int getSerial() {
		return serial;
	}

	/**
	 * 設定編號
	 * 
	 * @param serial
	 */
	public void setSerial(int serial) {
		this.serial = serial;
	}

	/**
	 * 取得問卷代碼
	 * 
	 * @return
	 */
	public String getSurveyGuid() {
		return SurveyGuid;
	}

	/**
	 * 設定問卷代碼
	 * 
	 * @param questionarireID
	 */
	public void setSurveyGuid(String surveyGuid) {
		SurveyGuid = surveyGuid;
	}

	/**
	 * 取得受訪者名稱
	 * 
	 * @return
	 */
	public String getName() {
		return Name;
	}

	/**
	 * 設定受訪者名稱
	 * 
	 * @param name
	 */
	public void setName(String Name) {
		this.Name = Name;
	}

	/**
	 * 取得受訪者年齡
	 * 
	 * @return
	 */
	public String getAge() {
		return Age;
	}

	/**
	 * 設定受訪者年齡
	 * 
	 * @param age
	 */
	public void setAge(String Age) {
		this.Age = Age;
	}

	/**
	 * 取得受訪者地址
	 * 
	 * @return
	 */
	public String getAddress() {
		return Address;
	}

	/**
	 * 設定受訪者地址
	 * 
	 * @param address
	 */
	public void setAddress(String Address) {
		this.Address = Address;
	}

	/**
	 * 取得受訪者電話
	 * 
	 * @return
	 */
	public String getPhone() {
		return Phone;
	}

	/**
	 * 設定受訪者電話
	 * 
	 * @param phone
	 */
	public void setPhone(String Phone) {
		this.Phone = Phone;
	}

	/**
	 * 取得上傳否
	 * 
	 * @return
	 */
	public String getIsupdate() {
		return isupdate;
	}

	/**
	 * 設定上傳否
	 * 
	 * @param update
	 */
	public void setIsupdate(String isupdate) {
		this.isupdate = isupdate;
	}

	/**
	 * 取得上傳日期
	 * 
	 * @return
	 */
	public String getAnswerDate() {
		return AnswerDate;
	}

	/**
	 * 設定上傳日期
	 * 
	 * @param answerDate
	 */
	public void setAnswerDate(String answerDate) {
		AnswerDate = answerDate;
	}

	/**
	 * 取得地區編碼
	 * 
	 * @return
	 */
	public String getZipCode() {
		return ZipCode;
	}

	/**
	 * 設定地區編碼
	 * 
	 * @param zipCode
	 */
	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}

	public String getMainPriority() {
		return MainPriority;
	}

	public void setMainPriority(String mainPriority) {
		MainPriority = mainPriority;
	}
	
	public String getSubPriority() {
		return SubPriority;
	}

	public void setSubPriority(String subPriority) {
		SubPriority = subPriority;
	}

}
