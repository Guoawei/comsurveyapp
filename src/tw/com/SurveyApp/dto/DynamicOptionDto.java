package tw.com.SurveyApp.dto;

public class DynamicOptionDto {
	private String serial;// 編號
	private String SurveyGuid;// 問卷編號
	private String dyoptionguid;// 動態選項編號
	private String subject_accordingKey;// 選項對應鍵
	private String subject_subjectIndex;// 圖項題號
	private String accordingValue;// 選項對應值
	private String value;// 選項值
	private String subject_index;// 選項索引
	private String text;// 選項內容

	/**
	 * 取得各欄位名稱
	 * 
	 * @return
	 */
	public String[] getColumnname() {
		String Columnname[] = { "SurveyGuid", "dyoptionguid",
				"subject_accordingKey", "subject_subjectIndex",
				"accordingValue", "value", "subject_index", "text" };
		return Columnname;
	}

	/**
	 * 取得編號
	 * 
	 * @return
	 */
	public String getSerial() {
		return serial;
	}

	/**
	 * 設定編號
	 * 
	 * @param serial
	 */
	public void setSerial(String serial) {
		this.serial = serial;
	}

	/**
	 * 取得問卷編號
	 * 
	 * @return
	 */
	public String getSurveyGuid() {
		return SurveyGuid;
	}

	/**
	 * 設定問卷編號
	 * 
	 * @param surveyGuid
	 */
	public void setSurveyGuid(String surveyGuid) {
		SurveyGuid = surveyGuid;
	}

	/**
	 * 取得動態選項編號
	 * 
	 * @return
	 */
	public String getDyoptionguid() {
		return dyoptionguid;
	}

	/**
	 * 設定動態選項編號
	 * 
	 * @param dyoptionguid
	 */
	public void setDyoptionguid(String dyoptionguid) {
		this.dyoptionguid = dyoptionguid;
	}

	/**
	 * 取得選項對應鍵
	 * 
	 * @return
	 */
	public String getSubject_accordingKey() {
		return subject_accordingKey;
	}

	/**
	 * 設定選項對應鍵
	 * 
	 * @param subject_accordingKey
	 */
	public void setSubject_accordingKey(String subject_accordingKey) {
		this.subject_accordingKey = subject_accordingKey;
	}

	/**
	 * 取得選項值
	 * 
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * 設定選項值
	 * 
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * 取得選項索引
	 * 
	 * @return
	 */
	public String getSubject_index() {
		return subject_index;
	}

	/**
	 * 設定選項索引
	 * 
	 * @param index
	 */
	public void setSubject_index(String subject_index) {
		this.subject_index = subject_index;
	}

	/**
	 * 取得選項內容
	 * 
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * 設定選項內容
	 * 
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * 取得選項對應值
	 * 
	 * @return
	 */
	public String getAccordingValue() {
		return accordingValue;
	}

	/**
	 * 設定選項對應值
	 * 
	 * @param accordingValue
	 */
	public void setAccordingValue(String accordingValue) {
		this.accordingValue = accordingValue;
	}

	/**
	 * 取的選項題號
	 * 
	 * @return
	 */
	public String getSubject_subjectIndex() {
		return subject_subjectIndex;
	}

	/**
	 * 設定選項題號
	 * 
	 * @param subject_subjectIndex
	 */
	public void setSubject_subjectIndex(String subject_subjectIndex) {
		this.subject_subjectIndex = subject_subjectIndex;
	}

}
