package tw.com.SurveyApp.dto;

public class SubjectDto {
	private int serial;// 編號
	private String SurveyGuid;// 問卷編號
	private String SubjectIndex;// 題號
	private String SubjectType;// 題型
	private String SubjectTitle;// 題目標題
	private String MultiSelectedLimit;// 複選題選擇限制
	private String DynamicOption;// 動態選項
	private String DynamicOptionAnswer;// 動態答案選項
	private String GroupOption;// 群組選項
	private String SubjectOpiton;// 選項內容
	private String SubjectIsRandom;// 是否隨機亂數
	private String OptionOutputForm;// 顯示方式

	public SubjectDto() {
	}

	public String[] getColumnname() {
		String Coumnname[] = { "SurveyGuid", "SubjectIndex", "SubjectType",
				"SubjectTitle", "MultiSelectedLimit", "DynamicOption",
				"DynamicOptionAnswer", "GroupOption", "SubjectOpiton",
				"SubjectIsRandom", "OptionOutputForm" };
		return Coumnname;
	}

	/**
	 * 取得編號
	 * 
	 * @return
	 */
	public int getSerial() {
		return serial;
	}

	/**
	 * 設定編號
	 * 
	 * @param serial
	 */
	public void setSerial(int serial) {
		this.serial = serial;
	}

	/**
	 * 取得問卷編號
	 * 
	 * @return
	 */
	public String getSurveyGuid() {
		return SurveyGuid;
	}

	/**
	 * 設定問卷編號
	 * 
	 * @param surveyGuid
	 */
	public void setSurveyGuid(String surveyGuid) {
		SurveyGuid = surveyGuid;
	}

	/**
	 * 取得題號
	 * 
	 * @return
	 */
	public String getSubjectIndex() {
		return SubjectIndex;
	}

	/**
	 * 設定題號
	 * 
	 * @param subjectIndex
	 */
	public void setSubjectIndex(String subjectIndex) {
		SubjectIndex = subjectIndex;
	}

	/**
	 * 取得題型
	 * 
	 * @return
	 */
	public String getSubjectType() {
		return SubjectType;
	}

	/**
	 * 設定題型
	 * 
	 * @param subjectType
	 */
	public void setSubjectType(String subjectType) {
		SubjectType = subjectType;
	}

	/**
	 * 取得題目標題
	 * 
	 * @return
	 */
	public String getSubjectTitle() {
		return SubjectTitle;
	}

	/**
	 * 設定題目標題
	 * 
	 * @param subjectTitle
	 */
	public void setSubjectTitle(String subjectTitle) {
		SubjectTitle = subjectTitle;
	}

	/**
	 * 取得複選題選擇限制
	 * 
	 * @return
	 */
	public String getMultiSelectedLimit() {
		return MultiSelectedLimit;
	}

	/**
	 * 設定複選題選擇限制
	 * 
	 * @param multiSelectedLimit
	 */
	public void setMultiSelectedLimit(String multiSelectedLimit) {
		MultiSelectedLimit = multiSelectedLimit;
	}

	/**
	 * 取得動態選項
	 * 
	 * @return
	 */
	public String getDynamicOption() {
		return DynamicOption;
	}

	/**
	 * 設定動態選項
	 * 
	 * @param dynamicOption
	 */
	public void setDynamicOption(String dynamicOption) {
		DynamicOption = dynamicOption;
	}

	/**
	 * 取得動態答案選項
	 * 
	 * @return
	 */
	public String getDynamicOptionAnswer() {
		return DynamicOptionAnswer;
	}

	/**
	 * 設定動態答案選項
	 * 
	 * @param dynamicOptionAnswer
	 */
	public void setDynamicOptionAnswer(String dynamicOptionAnswer) {
		DynamicOptionAnswer = dynamicOptionAnswer;
	}

	/**
	 * 取得群組選項
	 * 
	 * @return
	 */
	public String getGroupOption() {
		return GroupOption;
	}

	/**
	 * 設定群組選項
	 * 
	 * @param groupOption
	 */
	public void setGroupOption(String groupOption) {
		GroupOption = groupOption;
	}

	/**
	 * 取得選項內容
	 * 
	 * @return
	 */
	public String getSubjectOpiton() {
		return SubjectOpiton;
	}

	/**
	 * 設定選項內容
	 * 
	 * @param subjectOpiton
	 */
	public void setSubjectOpiton(String subjectOpiton) {
		SubjectOpiton = subjectOpiton;
	}

	/**
	 * 取得是否隨機亂數
	 * 
	 * @return
	 */
	public String getSubjectIsRandom() {
		return SubjectIsRandom;
	}

	/**
	 * 設定是否隨機亂數
	 * 
	 * @param subjectIsRandom
	 */
	public void setSubjectIsRandom(String subjectIsRandom) {
		SubjectIsRandom = subjectIsRandom;
	}

	/**
	 * 取得顯示方式
	 * 
	 * @return
	 */
	public String getOptionOutputForm() {
		return OptionOutputForm;
	}

	/**
	 * 設定顯示方式
	 * 
	 * @param optionOutputForm
	 */
	public void setOptionOutputForm(String optionOutputForm) {
		OptionOutputForm = optionOutputForm;
	}

}
