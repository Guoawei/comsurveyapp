package tw.com.SurveyApp.dto;

public class UpdataLogDto {
	private String SurveyGuid;
	private String SampleGuid;

	/**
	 * 取得問卷id
	 * 
	 * @return
	 */
	public String getSurveyGuid() {
		return SurveyGuid;
	}

	/**
	 * 設定問卷id
	 * 
	 * @param surveyGuid
	 */
	public void setSurveyGuid(String surveyGuid) {
		SurveyGuid = surveyGuid;
	}

	/**
	 * 取得樣本id
	 * 
	 * @return
	 */
	public String getSampleGuid() {
		return SampleGuid;
	}

	/**
	 * 設定樣本id
	 * 
	 * @param sampleGuid
	 */
	public void setSampleGuid(String sampleGuid) {
		SampleGuid = sampleGuid;
	}

}
