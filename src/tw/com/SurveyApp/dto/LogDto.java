package tw.com.SurveyApp.dto;

public class LogDto {

	private String sampleGuid;
	private String surveyGuid;
	private String Sindex;
	private String start;
	private String end;
	
	/**
	 * 取得各欄位名稱
	 * 
	 * @return
	 */
	public String[] getColumnname() {
		String Columnname[] = { "SampleGuid", "SurveyGuid", "Sindex",
				"Start", "End"};
		return Columnname;
	}

	/**
	 * 取得次數
	 * 
	 * @return
	 */
	public String getSindex() {
		return Sindex;
	}

	/**
	 * 設定次數
	 * 
	 * @param sindex
	 */
	public void setSindex(String sindex) {
		Sindex = sindex;
	}

	/**
	 * 取得樣本id
	 * 
	 * @return
	 */
	public String getSampleGuid() {
		return sampleGuid;
	}

	/**
	 * 設定樣本id
	 * 
	 * @param sampleGuid
	 */
	public void setSampleGuid(String sampleGuid) {
		this.sampleGuid = sampleGuid;
	}

	/**
	 * 取得問卷id
	 * 
	 * @return
	 */
	public String getSurveyGuid() {
		return surveyGuid;
	}

	/**
	 * 設定樣本id
	 * 
	 * @param surveyGuid
	 */
	public void setSurveyGuid(String surveyGuid) {
		this.surveyGuid = surveyGuid;
	}

	/**
	 * 取得開始時間
	 * 
	 * @return
	 */
	public String getStart() {
		return start;
	}

	/**
	 * 設定開始時間
	 * 
	 * @param start
	 */
	public void setStart(String start) {
		this.start = start;
	}

	/**
	 * 取得結束時間
	 * 
	 * @return
	 */
	public String getEnd() {
		return end;
	}

	/**
	 * 設定結束時間
	 * 
	 * @param end
	 */
	public void setEnd(String end) {
		this.end = end;
	}

}
