package tw.com.SurveyApp.dto;

public class StatusDto {

	private String StatusNumber;
	private String Step;
	private String Name;

	/**
	 * 取得各欄位名稱
	 * 
	 * @return
	 */
	public String[] getColumnname() {
		String Columnname[] = { "Step", "StatusNumber",  "Name" };
		return Columnname;
	}

	/**
	 * 取得階段
	 * 
	 * @return
	 */
	public String getStep() {
		return Step;
	}

	/**
	 * 設定階段
	 * 
	 * @param step
	 */
	public void setStep(String step) {
		Step = step;
	}

	/**
	 * 取得狀態
	 * 
	 * @return
	 */
	public String getStatusNumber() {
		return StatusNumber;
	}

	/**
	 * 設定狀態
	 * 
	 * @param status
	 */
	public void setStatusNumber(String statusNumber) {
		StatusNumber = statusNumber;
	}

	/**
	 * 取得狀態內容
	 * 
	 * @return
	 */
	public String getName() {
		return Name;
	}

	/**
	 * 設定狀態內容
	 * 
	 * @param name
	 */
	public void setName(String name) {
		Name = name;
	}

}
