package tw.com.SurveyApp.dto;

public class AnswerDto {
	private int serial;
	private String SurveyGuid;// 問卷ID
	private String InterviewerID;// 訪員ID
	private String SampleID;// 樣本ID
	private String Previous;// 上一題題號
	private String SubjectIndex;// 題號
	private String Answer;// 答案

	/**
	 * 取得各欄位名稱
	 * 
	 * @return
	 */
	public String[] getColumnname() {
		String Columnname[] = { "SurveyGuid", "InterviewerID", "SampleID",
				"Previous", "SubjectIndex", "Answer" };
		return Columnname;
	}

	/**
	 * 取得代碼
	 * 
	 * @return
	 */
	public int getSerial() {
		return serial;
	}

	/**
	 * 設定代碼
	 * 
	 * @param serial
	 */
	public void setSerial(int serial) {
		this.serial = serial;
	}

	/**
	 * 取得問卷ID
	 * 
	 * @return
	 */
	public String getSurveyGuid() {
		return SurveyGuid;
	}

	/**
	 * 設定問卷ID
	 * 
	 * @param SurveyGuid
	 */
	public void setSurveyGuid(String surveyGuid) {
		SurveyGuid = surveyGuid;
	}

	/**
	 * 取得訪員ID
	 * 
	 * @return
	 */
	public String getInterviewerID() {
		return InterviewerID;
	}

	/**
	 * 設定訪員ID
	 * 
	 * @param interviewerID
	 */
	public void setInterviewerID(String interviewerID) {
		InterviewerID = interviewerID;
	}

	/**
	 * 取得樣本ID
	 * 
	 * @return
	 */
	public String getSampleID() {
		return SampleID;
	}

	/**
	 * 設定樣本ID
	 * 
	 * @param sampleID
	 */
	public void setSampleID(String sampleID) {
		SampleID = sampleID;
	}

	/**
	 * 取得上一題題號
	 * 
	 * @return
	 */
	public String getPrevious() {
		return Previous;
	}

	/**
	 * 設定上一題題號
	 * 
	 * @param previous
	 */
	public void setPrevious(String previous) {
		Previous = previous;
	}

	/**
	 * 取得題號
	 * 
	 * @return
	 */
	public String getSubjectIndex() {
		return SubjectIndex;
	}

	/**
	 * 設定題號
	 * 
	 * @param subjectIndex
	 */
	public void setSubjectIndex(String subjectIndex) {
		SubjectIndex = subjectIndex;
	}

	/**
	 * 取得答案
	 * 
	 * @return
	 */
	public String getAnswer() {
		return Answer;
	}

	/**
	 * 設定答案
	 * 
	 * @param answer
	 */
	public void setAnswer(String answer) {
		Answer = answer;
	}

}
