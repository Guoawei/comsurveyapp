package tw.com.SurveyApp.dto;

public class StatusDataDto {

	private String SampleGuid;
	private String SurveyGuid;
	private String SituationCode;
	private String Comment;
	private String CDateTime;

	/**
	 * 取得各欄位名稱
	 * 
	 * @return
	 */
	public String[] getColumnname() {
		String Columnname[] = { "SampleGuid", "SurveyGuid", "SituationCode",
				"Comment", "CDateTime" };
		return Columnname;
	}

	/**
	 * 取得說明文字
	 * 
	 * @return
	 */
	public String getComment() {
		return Comment;
	}

	/**
	 * 設定說明文字
	 * 
	 * @param comment
	 */
	public void setComment(String comment) {
		Comment = comment;
	}

	/**
	 * 取得樣本id
	 * 
	 * @return
	 */
	public String getSampleGuid() {
		return SampleGuid;
	}

	/**
	 * 設定樣本id
	 * 
	 * @param sampleGuid
	 */
	public void setSampleGuid(String sampleGuid) {
		SampleGuid = sampleGuid;
	}

	/**
	 * 取得問卷id
	 * 
	 * @return
	 */
	public String getSurveyGuid() {
		return SurveyGuid;
	}

	/**
	 * 設定問卷id
	 * 
	 * @param surveyGuid
	 */
	public void setSurveyGuid(String surveyGuid) {
		SurveyGuid = surveyGuid;
	}

	/**
	 * 取得狀態編號
	 * 
	 * @return
	 */
	public String getSituationCode() {
		return SituationCode;
	}

	/**
	 * 設定狀態編號
	 * 
	 * @param statusNumber
	 */
	public void setSituationCode(String situationCode) {
		SituationCode = situationCode;
	}

	/**
	 * 取得開始時間
	 * 
	 * @return
	 */
	public String getCDateTime() {
		return CDateTime;
	}

	/**
	 * 設定開始時間
	 * 
	 * @param cDateTime
	 */
	public void setCDateTime(String cDateTime) {
		CDateTime = cDateTime;
	}

}
