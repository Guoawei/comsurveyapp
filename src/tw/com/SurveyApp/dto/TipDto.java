package tw.com.SurveyApp.dto;

public class TipDto {
	private int serial;
	private String SurveyGuid;// 問卷ID
	private String SubjectIndex;// 題號
	private String tip_index;// 提示卡代碼
	private String tip_name;// 提示卡名稱
	private String tip_image;// 提示卡圖片
	private String tip_text;// 提示卡文字

	/**
	 * 取得各欄位名稱
	 * 
	 * @return
	 */
	public String[] getColumnname() {
		String Columnname[] = { "SurveyGuid", "SubjectIndex", "tip_index",
				"tip_name", "tip_image", "tip_text" };
		return Columnname;
	}

	/**
	 * 取得代碼
	 * 
	 * @return
	 */
	public int getSerial() {
		return serial;
	}

	/**
	 * 設定代碼
	 * 
	 * @param serial
	 */
	public void setSerial(int serial) {
		this.serial = serial;
	}

	/**
	 * 取得問卷ID
	 * 
	 * @return
	 */
	public String getSurveyGuid() {
		return SurveyGuid;
	}

	/**
	 * 設定問卷ID
	 * 
	 * @param SurveyGuid
	 */
	public void setSurveyGuid(String surveyGuid) {
		SurveyGuid = surveyGuid;
	}

	/**
	 * 取得題號
	 * 
	 * @return
	 */
	public String getSubjectIndex() {
		return SubjectIndex;
	}

	/**
	 * 設定題號
	 * 
	 * @param subjectIndex
	 */
	public void setSubjectIndex(String subjectIndex) {
		SubjectIndex = subjectIndex;
	}

	/**
	 * 取得提示卡代碼
	 * 
	 * @return
	 */
	public String getTip_index() {
		return tip_index;
	}

	/**
	 * 設定提示卡代碼
	 * 
	 * @param tip_index
	 */
	public void setTip_index(String tip_index) {
		this.tip_index = tip_index;
	}

	/**
	 * 取得提示卡名稱
	 * 
	 * @return
	 */
	public String getTip_name() {
		return tip_name;
	}

	/**
	 * 設定提示卡名稱
	 * 
	 * @param tip_name
	 */
	public void setTip_name(String tip_name) {
		this.tip_name = tip_name;
	}

	/**
	 * 取得提示卡圖片
	 * 
	 * @return
	 */
	public String getTip_image() {
		return tip_image;
	}

	/**
	 * 設定提示卡圖片
	 * 
	 * @param tip_image
	 */
	public void setTip_image(String tip_image) {
		this.tip_image = tip_image;
	}

	/**
	 * 取得提示卡文字
	 * 
	 * @return
	 */
	public String getTip_text() {
		return tip_text;
	}

	/**
	 * 設定提示卡文字Ｄ
	 * 
	 * @param tip_text
	 */
	public void setTip_text(String tip_text) {
		this.tip_text = tip_text;
	}

}
