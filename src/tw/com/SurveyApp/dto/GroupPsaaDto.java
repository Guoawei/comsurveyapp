package tw.com.SurveyApp.dto;

public class GroupPsaaDto {
	private int serial;// 單題跳題代碼
	private String SurveyGuid;// 問卷編號
	private String SubjectIndex;// 題號
	private String subjectList;// 相關題號
	private String answerList;// 選擇答案
	private String jumpto;// 跳題題號

	/**
	 * 取得各欄位名稱
	 * 
	 * @return
	 */
	public String[] getColumnname() {
		String Columnname[] = { "SurveyGuid", "SubjectIndex", "subjectList",
				"answerList", "jumpto" };
		return Columnname;
	}

	/**
	 * 取得代碼
	 * 
	 * @return
	 */
	public int getSerial() {
		return serial;
	}

	/**
	 * 設定代碼
	 * 
	 * @param serial
	 */
	public void setSerial(int serial) {
		this.serial = serial;
	}

	/**
	 * 取得問卷編號
	 * 
	 * @return
	 */
	public String getSurveyGuid() {
		return SurveyGuid;
	}

	/**
	 * 設定問卷編號
	 * 
	 * @param SurveyGuid
	 */
	public void setSurveyGuid(String surveyGuid) {
		SurveyGuid = surveyGuid;
	}

	/**
	 * 取得題號
	 * 
	 * @return
	 */
	public String getSubjectIndex() {
		return SubjectIndex;
	}

	/**
	 * 設定題號
	 * 
	 * @param subject
	 */
	public void setSubjectIndex(String SubjectIndex) {
		this.SubjectIndex = SubjectIndex;
	}

	/**
	 * 取得相關題號
	 * 
	 * @return
	 */
	public String getSubjectList() {
		return subjectList;
	}

	/**
	 * 設定相關題號
	 * 
	 * @param subjectList
	 */
	public void setSubjectList(String subjectList) {
		this.subjectList = subjectList;
	}

	/**
	 * 取得答案
	 * 
	 * @return
	 */
	public String getAnswerList() {
		return answerList;
	}

	/**
	 * 設定答案
	 * 
	 * @param answer
	 */
	public void setAnswerList(String answerList) {
		this.answerList = answerList;
	}

	/**
	 * 取得跳題題號
	 * 
	 * @return
	 */
	public String getJumpto() {
		return jumpto;
	}

	/**
	 * 設定跳題題號
	 * 
	 * @param jumpto
	 */
	public void setJumpto(String jumpto) {
		this.jumpto = jumpto;
	}

}
