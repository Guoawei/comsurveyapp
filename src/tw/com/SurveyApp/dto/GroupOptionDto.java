package tw.com.SurveyApp.dto;

public class GroupOptionDto {

	private String serial;
	private String SurveyGuid;// 問卷編號
	private String GoGuid;// 群組題目編號
	private String GroupId;// 分類id
	private String GroupType;// 分類號
	private String GroupIndex;// 分類順序
	private String GroupText;// 分類文字
	private String GroupValue;// 分類值
	private String isShowTextView;// 是否顯示文字
	private String textViewPosition;// 文字在前在後

	/**
	 * 取得各欄位名稱
	 * 
	 * @return
	 */
	public String[] getColumnname() {
		String Columnname[] = { "SurveyGuid", "GoGuid", "GroupId", "GroupType",
				"GroupIndex", "GroupText", "GroupValue", "isShowTextView",
				"textViewPosition" };
		return Columnname;
	}

	/**
	 * 取得是否顯示文字
	 * 
	 * @return
	 */
	public String getIsShowTextView() {
		return isShowTextView;
	}

	/**
	 * 設定是否顯示文字
	 * 
	 * @param isShowTextView
	 */
	public void setIsShowTextView(String isShowTextView) {
		this.isShowTextView = isShowTextView;
	}

	/**
	 * 取得文字在前在後
	 * 
	 * @return
	 */
	public String getTextViewPosition() {
		return textViewPosition;
	}

	/**
	 * 設定文字在前在後
	 * 
	 * @param textViewPosition
	 */
	public void setTextViewPosition(String textViewPosition) {
		this.textViewPosition = textViewPosition;
	}

	/**
	 * 取得代碼
	 * 
	 * @return
	 */
	public String getSerial() {
		return serial;
	}

	/**
	 * 設定代碼
	 * 
	 * @param serial
	 */
	public void setSerial(String serial) {
		this.serial = serial;
	}

	/**
	 * 取得問卷編號
	 * 
	 * @return
	 */
	public String getSurveyGuid() {
		return SurveyGuid;
	}

	/**
	 * 設定問卷編號
	 * 
	 * @param surveyGuid
	 */
	public void setSurveyGuid(String surveyGuid) {
		SurveyGuid = surveyGuid;
	}

	/**
	 * 取得群組題號編號
	 * 
	 * @return
	 */
	public String getGoGuid() {
		return GoGuid;
	}

	/**
	 * 設定群組題號編號
	 * 
	 * @param goGuid
	 */
	public void setGoGuid(String goGuid) {
		GoGuid = goGuid;
	}

	/**
	 * 取得分類號
	 * 
	 * @return
	 */
	public String getGroupType() {
		return GroupType;
	}

	/**
	 * 設定分類號
	 * 
	 * @param groupType
	 */
	public void setGroupType(String groupType) {
		GroupType = groupType;
	}

	/**
	 * 取得分類順序
	 * 
	 * @return
	 */
	public String getGroupIndex() {
		return GroupIndex;
	}

	/**
	 * 設定分類順序
	 * 
	 * @param groupIndex
	 */
	public void setGroupIndex(String groupIndex) {
		GroupIndex = groupIndex;
	}

	/**
	 * 取得分類文字
	 * 
	 * @return
	 */
	public String getGroupText() {
		return GroupText;
	}

	/**
	 * 設定分類文字
	 * 
	 * @param groupText
	 */
	public void setGroupText(String groupText) {
		GroupText = groupText;
	}

	/**
	 * 取得分類值
	 * 
	 * @return
	 */
	public String getGroupValue() {
		return GroupValue;
	}

	/**
	 * 設定分類值
	 * 
	 * @param groupValue
	 */
	public void setGroupValue(String groupValue) {
		GroupValue = groupValue;
	}

	/**
	 * 取得分類Id
	 * 
	 * @return
	 */
	public String getGroupId() {
		return GroupId;
	}

	/**
	 * 設定分類Id
	 * 
	 * @param groupId
	 */
	public void setGroupId(String groupId) {
		GroupId = groupId;
	}

}
