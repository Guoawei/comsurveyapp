package tw.com.SurveyApp.dto;

public class UpdateAppDto {
	private String version;
	private String url;

	/**
	 * 取得版本
	 * 
	 * @return
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * 設定版本
	 * 
	 * @param version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * 取得App位置
	 * 
	 * @return
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 設定App位置
	 * 
	 * @param url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

}
