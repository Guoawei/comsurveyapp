package tw.com.SurveyApp.dto;

public class OptionDto {
	private int serial;
	private String SurveyGuid;// 問號編號
	private String SubjectIndex;// 題目編號
	private String optionIndex;// 選項順序索引
	private String optionText;// 選項內容
	private String optionValue;// 選項值
	private String optionIsShowTextView;// 是否顯示文字輸入框
	private String optionTextViewPosition;// 文字輸入框位置
	private String optionPickerValueArray;// 給P題型用的UI選項內容
	private String optionDyPickerSubject;// 動態p題型所根據的答案來源
	private String optionDyPickerMin;// 動態p題型選項最小值
	private String optionDyPickerMax;// 動態p題型選項最大值
	private String optionDyPickerTick;// 動態p題型選項變動量

	public OptionDto() {
	}

	/**
	 * 取得各欄位名稱
	 * 
	 * @return
	 */
	public String[] getColumnname() {
		String Columnname[] = { "SurveyGuid", "SubjectIndex", "optionIndex",
				"optionText", "optionValue", "optionIsShowTextView",
				"optionTextViewPosition", "optionPickerValueArray",
				"optionDyPickerSubject", "optionDyPickerMin",
				"optionDyPickerMax", "optionDyPickerTick" };
		return Columnname;
	}

	/**
	 * 取得動態p題型所根據的答案來源
	 * 
	 * @return
	 */
	public String getOptionDyPickerSubject() {
		return optionDyPickerSubject;
	}

	/**
	 * 設定動態p題型所根據的答案來源
	 * 
	 * @param optionDyPickerSubject
	 */
	public void setOptionDyPickerSubject(String optionDyPickerSubject) {
		this.optionDyPickerSubject = optionDyPickerSubject;
	}

	/**
	 * 取得動態p題型選項最小值
	 * 
	 * @return
	 */
	public String getOptionDyPickerMin() {
		return optionDyPickerMin;
	}

	/**
	 * 設定動態p題型選項最小值
	 * 
	 * @param optionDyPickerMin
	 */
	public void setOptionDyPickerMin(String optionDyPickerMin) {
		this.optionDyPickerMin = optionDyPickerMin;
	}

	/**
	 * 取得動態p題型選項最大值
	 * 
	 * @return
	 */
	public String getOptionDyPickerMax() {
		return optionDyPickerMax;
	}

	/**
	 * 設定動態p題型選項最大值
	 * 
	 * @param optionDyPickerMax
	 */
	public void setOptionDyPickerMax(String optionDyPickerMax) {
		this.optionDyPickerMax = optionDyPickerMax;
	}

	/**
	 * 取得動態p題型選項變動量
	 * 
	 * @return
	 */
	public String getOptionDyPickerTick() {
		return optionDyPickerTick;
	}

	/**
	 * 設定動態p題型選項變動量
	 * 
	 * @param optionDyPickerTick
	 */
	public void setOptionDyPickerTick(String optionDyPickerTick) {
		this.optionDyPickerTick = optionDyPickerTick;
	}

	/**
	 * 取得選項順序索引
	 * 
	 * @return
	 */
	public String getOptionIndex() {
		return optionIndex;
	}

	/**
	 * 設定選項順序索引
	 * 
	 * @param optionIndex
	 */
	public void setOptionIndex(String optionIndex) {
		this.optionIndex = optionIndex;
	}

	/**
	 * 取得問卷編號
	 * 
	 * @return
	 */
	public String getSurveyGuid() {
		return SurveyGuid;
	}

	/**
	 * 設定問卷編號
	 * 
	 * @param SurveyGuid
	 */
	public void setSurveyGuid(String surveyGuid) {
		SurveyGuid = surveyGuid;
	}

	/**
	 * 取得編號
	 * 
	 * @return
	 */
	public int getSerial() {
		return serial;
	}

	/**
	 * 設定編號
	 * 
	 * @param serial
	 */
	public void setSerial(int serial) {
		this.serial = serial;
	}

	/**
	 * 取得題目編號
	 * 
	 * @return
	 */
	public String getSubjectIndex() {
		return SubjectIndex;
	}

	/**
	 * 設定題目編號
	 * 
	 * @param subjectSerial
	 */
	public void setSubjectIndex(String subjectIndex) {
		SubjectIndex = subjectIndex;
	}

	/**
	 * 取得選項內容
	 * 
	 * @return
	 */
	public String getOptionText() {
		return optionText;
	}

	/**
	 * 設定選項內容
	 * 
	 * @param optionText
	 */
	public void setOptionText(String optionText) {
		this.optionText = optionText;
	}

	/**
	 * 設定選項值
	 * 
	 * @param optionValue
	 */
	public String getOptionValue() {
		return optionValue;
	}

	/**
	 * 設定選項值
	 * 
	 * @param optionValue
	 */
	public void setOptionValue(String optionValue) {
		this.optionValue = optionValue;
	}

	/**
	 * 取得是否顯示文字輸入框
	 * 
	 * @return
	 */
	public String getOptionIsShowTextView() {
		return optionIsShowTextView;
	}

	/**
	 * 設定是否顯示文字輸入框
	 * 
	 * @param optionIsShowTextView
	 */
	public void setOptionIsShowTextView(String optionIsShowTextView) {
		this.optionIsShowTextView = optionIsShowTextView;
	}

	/**
	 * 取得文字輸入框位置
	 * 
	 * @return
	 */
	public String getOptionTextViewPosition() {
		return optionTextViewPosition;
	}

	/**
	 * 設定文字輸入框位置
	 * 
	 * @param optionTextViewPosition
	 */
	public void setOptionTextViewPosition(String optionTextViewPosition) {
		this.optionTextViewPosition = optionTextViewPosition;
	}

	/**
	 * 取得給P題型用的UI選項內容
	 * 
	 * @return
	 */
	public String getOptionPickerValueArray() {
		return optionPickerValueArray;
	}

	/**
	 * 設定給P題型用的UI選項內容
	 * 
	 * @param optionPickerValueArray
	 */
	public void setOptionPickerValueArray(String optionPickerValueArray) {
		this.optionPickerValueArray = optionPickerValueArray;
	}
}
