package tw.com.SurveyApp.dto;

public class SinglePsaaDto {
	private int serial;// 單題跳題代碼
	private String SurveyGuid;// 問卷編號
	private String SubjectIndex;// 題號
	private String answer;// 選擇答案
	private String jumpto;// 跳題題號
	private String isselect;// 選擇否

	/**
	 * 取得各欄位名稱
	 * 
	 * @return
	 */
	public String[] getColumnname() {
		String Columnname[] = { "SurveyGuid", "SubjectIndex", "answer",
				"jumpto", "isselect" };
		return Columnname;
	}

	/**
	 * 取得代碼
	 * 
	 * @return
	 */
	public int getSerial() {
		return serial;
	}

	/**
	 * 設定代碼
	 * 
	 * @param serial
	 */
	public void setSerial(int serial) {
		this.serial = serial;
	}

	/**
	 * 取得問題編號
	 * 
	 * @return
	 */
	public String getSurveyGuid() {
		return SurveyGuid;
	}

	/**
	 * 設定問編號
	 * 
	 * @param SurveyGuid
	 */
	public void setSurveyGuid(String surveyGuid) {
		SurveyGuid = surveyGuid;
	}

	/**
	 * 取得題號
	 * 
	 * @return
	 */
	public String getSubjectIndex() {
		return SubjectIndex;
	}

	/**
	 * 設定題號
	 * 
	 * @param subject_index
	 */
	public void setSubjectIndex(String SubjectIndex) {
		this.SubjectIndex = SubjectIndex;
	}

	/**
	 * 取得選擇答案
	 * 
	 * @return
	 */
	public String getAnswer() {
		return answer;
	}

	/**
	 * 設定選擇答案
	 * 
	 * @param answer
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	/**
	 * 取得跳題題號
	 * 
	 * @return
	 */
	public String getJumpto() {
		return jumpto;
	}

	/**
	 * 設定跳題題號
	 * 
	 * @param jumpto
	 */
	public void setJumpto(String jumpto) {
		this.jumpto = jumpto;
	}

	/**
	 * 取得選擇否
	 * 
	 * @return
	 */
	public String getIsselect() {
		return isselect;
	}

	/**
	 * 設定選擇否
	 * 
	 * @param select
	 */
	public void setIsselect(String isselect) {
		this.isselect = isselect;
	}

}
