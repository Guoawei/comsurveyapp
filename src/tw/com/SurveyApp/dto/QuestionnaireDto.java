package tw.com.SurveyApp.dto;

public class QuestionnaireDto {

	private int serial;
	private String SurveyGuid;// 問卷編號
	private String Title;// 問卷標題
	private String GreetingText;// 問候語
	private String ThankText;// 結束問候語
	private String TotalRows;// 總題數
	private String Ver;// 版本
	private String CDateTime;// 建立時間
	private String UDateTime;// 更新時間
	private String CompletedRow;// 已完成數量
	private String IntervieweeRows;// 受訪者數量

	/**
	 * 取得各欄位名稱
	 * 
	 * @return
	 */
	public String[] getColumnname() {
		String Coumnname[] = { "SurveyGuid", "Title", "GreetingText",
				"ThankText", "TotalRows", "Ver", "CDateTime", "UDateTime",
				"CompletedRow", "IntervieweeRows" };
		return Coumnname;
	}

	/**
	 * 取得編號
	 * 
	 * @return
	 */
	public int getSerial() {
		return serial;
	}

	/**
	 * 設定編號
	 * 
	 * @param serial
	 */
	public void setSerial(int serial) {
		this.serial = serial;
	}

	/**
	 * 取得問卷ID
	 * 
	 * @return
	 */
	public String getSurveyGuid() {
		return SurveyGuid;
	}

	/**
	 * 設定問卷ID
	 * 
	 * @param iD
	 */
	public void setSurveyGuid(String surveyGuid) {
		SurveyGuid = surveyGuid;
	}

	/**
	 * 取得問卷名稱
	 * 
	 * @return
	 */
	public String getTitle() {
		return Title;
	}

	/**
	 * 設定問卷名稱
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		Title = title;
	}

	/**
	 * 取得歡迎詞
	 * 
	 * @return
	 */
	public String getGreetingText() {
		return GreetingText;
	}

	/**
	 * 設得歡迎詞
	 * 
	 * @param greetingText
	 */
	public void setGreetingText(String greetingText) {
		GreetingText = greetingText;
	}

	/**
	 * 取得結束詞
	 * 
	 * @return
	 */
	public String getThankText() {
		return ThankText;
	}

	/**
	 * 設定結束詞
	 * 
	 * @param thankText
	 */
	public void setThankText(String thankText) {
		ThankText = thankText;
	}

	/**
	 * 取得總共欄位
	 * 
	 * @return
	 */
	public String getTotalRows() {
		return TotalRows;
	}

	/**
	 * 設定總共欄位
	 * 
	 * @param totalRows
	 */
	public void setTotalRows(String totalRows) {
		TotalRows = totalRows;
	}

	/**
	 * 取得版本
	 * 
	 * @return
	 */
	public String getVer() {
		return Ver;
	}

	/**
	 * 設定版本
	 * 
	 * @param ver
	 */
	public void setVer(String ver) {
		Ver = ver;
	}

	/**
	 * 取得建立時間
	 * 
	 * @return
	 */
	public String getCDateTime() {
		return CDateTime;
	}

	/**
	 * 設定建立時間
	 * 
	 * @param cDateTime
	 */
	public void setCDateTime(String cDateTime) {
		CDateTime = cDateTime;
	}

	/**
	 * 取得更新時間
	 * 
	 * @return
	 */
	public String getUDateTime() {
		return UDateTime;
	}

	/**
	 * 設定更新時間
	 * 
	 * @param uDateTime
	 */
	public void setUDateTime(String uDateTime) {
		UDateTime = uDateTime;
	}

	/**
	 * 取得完成筆數
	 * 
	 * @return
	 */
	public String getCompletedRow() {
		return CompletedRow;
	}

	/**
	 * 設定完成筆數
	 * 
	 * @param completedRow
	 */
	public void setCompletedRow(String completedRow) {
		CompletedRow = completedRow;
	}

	/**
	 * 取得總訪問人數
	 * 
	 * @return
	 */
	public String getIntervieweeRows() {
		return IntervieweeRows;
	}

	/**
	 * 設定總訪問人數
	 * 
	 * @param intervieweeRows
	 */
	public void setIntervieweeRows(String intervieweeRows) {
		IntervieweeRows = intervieweeRows;
	}

}
