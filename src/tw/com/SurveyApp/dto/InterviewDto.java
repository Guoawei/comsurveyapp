package tw.com.SurveyApp.dto;

public class InterviewDto {
	private int serial;
	private String Guid;// 訪員編號
	private String Name;// 訪員姓名
	private String Email;// 登入mail

	private String Password;// 訪員密碼

	/**
	 * 取得各欄位名稱
	 * 
	 * @return
	 */
	public String[] getColumnname() {
		String Columnname[] = { "Guid", "Name", "Email", "Password" };
		return Columnname;
	}

	/**
	 * 取得代碼
	 * 
	 * @return
	 */
	public int getSerial() {
		return serial;
	}

	/**
	 * 設定代碼
	 * 
	 * @param serial
	 */
	public void setSerial(int serial) {
		this.serial = serial;
	}

	/**
	 * 取得訪員編號
	 * 
	 * @return
	 */
	public String getGuid() {
		return Guid;
	}

	/**
	 * 設定訪員編號
	 * 
	 * @param iD
	 */
	public void setGuid(String guid) {
		Guid = guid;
	}

	/**
	 * 取得訪員姓名
	 * 
	 * @return
	 */
	public String getName() {
		return Name;
	}

	/**
	 * 設定訪員姓名
	 * 
	 * @param interviewer
	 */
	public void setName(String name) {
		Name = name;
	}

	/**
	 * 取得訪員帳號
	 * 
	 * @return
	 */
	public String getEmail() {
		return Email;
	}

	/**
	 * 設定訪員帳號
	 * 
	 * @param account
	 */
	public void setEmail(String email) {
		Email = email;
	}

	/**
	 * 取得訪員帳號
	 * 
	 * @return
	 */
	public String getPassword() {
		return Password;
	}

	/**
	 * 設定訪員帳號
	 * 
	 * @param password
	 */
	public void setPassword(String password) {
		Password = password;
	}

}
