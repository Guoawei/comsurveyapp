package tw.com.SurveyApp.data;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

public class JsonConnection {

	private Bitmap bitmap;
	private String fileName;
	private final static String ALBUM_PATH = Environment
			.getExternalStorageDirectory() + "/download_test/";

//	 public final static String connection =
//	 "http://184.169.147.105/surveyapp/";//正式區
	public final static String connection = "http://capi.nctu.edu.tw/surveyapp/";
//	 public final static String connection = "http://192.168.0.105/surveyapp/";
//	public final static String connection = "http://10.0.1.4/surveyapp/";
	private final String appVersion = "http://dl.dropbox.com/u/72594791/updata.txt";
	private final String appUpdata = "http://dl.dropbox.com/u/72594791/SurveyApp.apk";

	/**
	 * 取得App版本
	 * 
	 * @return
	 */
	public String getAppVersion() {
		return appVersion;
	}

	/**
	 * 取得App更新軟體
	 * 
	 * @return
	 */
	public String getAppUpdata() {
		return appUpdata;
	}

	/**
	 * 連入伺服器取的圖片
	 * 
	 * @param filePath
	 * @param fileName
	 */
	public void connServerForImage(String filePath, String fileName) {
		byte[] data;
		try {
			data = getImage(filePath);
			if (data != null) {
				bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);// bitmap

				this.fileName = fileName;

				new Thread(saveFileRunnable).start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 取得圖片資料
	 * 
	 * @param inStream
	 * @return byte[]
	 * @throws Exception
	 */
	public byte[] readStream(InputStream inStream) throws Exception {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = inStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, len);
		}
		outStream.close();
		inStream.close();
		return outStream.toByteArray();
	}

	/**
	 * 從網路上下載圖片
	 * 
	 * @param path
	 *            The path of image
	 * @return byte[]
	 * @throws Exception
	 */
	public byte[] getImage(String path) throws Exception {
		URL url = new URL(path);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setConnectTimeout(5 * 1000);
		conn.setRequestMethod("GET");
		InputStream inStream = conn.getInputStream();
		if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
			return readStream(inStream);
		}
		return null;
	}

	/**
	 * 保存文件
	 * 
	 * @param bm
	 * @param fileName
	 * @throws IOException
	 */
	public void saveFile(Bitmap bm, String fileName) throws IOException {
		File dirFile = new File(ALBUM_PATH);
		if (!dirFile.exists()) {
			dirFile.mkdir();
		}

		File myCaptureFile = new File(ALBUM_PATH + fileName);

		if (myCaptureFile.exists()) {
			myCaptureFile.delete();
		}
		BufferedOutputStream bos = new BufferedOutputStream(
				new FileOutputStream(myCaptureFile));
		bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
		bos.flush();
		bos.close();
	}

	/**
	 * 儲存圖片的執行緒
	 */
	private Runnable saveFileRunnable = new Runnable() {
		public void run() {
			try {
				saveFile(bitmap, fileName);
			} catch (IOException e) {
				System.out.println(e);
			}
		}
	};

	/**
	 * 取得JSON資料
	 * 
	 * @param strUrl
	 * @return
	 */
	public String connServerForResult(String strUrl) {
		String strResult = "";
		try {
			// HttpClient對象
			HttpClient httpClient = new DefaultHttpClient();
			// Socket超時設置60s
			httpClient.getParams().setIntParameter(
					HttpConnectionParams.SO_TIMEOUT, 60000);
			// 連線超時60s
			httpClient.getParams().setIntParameter(
					HttpConnectionParams.CONNECTION_TIMEOUT, 60000);

			// HttpGet對象
			HttpGet httpRequest = new HttpGet(strUrl);
			// 獲得HttpResponse對象
			HttpResponse httpResponse = httpClient.execute(httpRequest);

			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				// 取得返回的數據
				strResult = EntityUtils.toString(httpResponse.getEntity());
			}
			httpRequest.abort();
		} catch (ClientProtocolException e) {
			System.out.println("ClientProtocolException = " + e);
			e.printStackTrace();
		} catch (ConnectTimeoutException  e){
			System.out.println("ConnectTimeoutException = " + e);
			e.printStackTrace();
		} catch (ParseException e) {
			System.out.println("ParseException =" + e);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IOException = " + e);
			e.printStackTrace();
		}
		return strResult;
	}

	public HttpResponse doPost(String url, String c)
			throws ClientProtocolException, IOException {
		HttpParams params = new BasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(params, "UTF-8");
		params.setBooleanParameter("http.protocol.expect-continue", false);

		HttpClient httpclient = new DefaultHttpClient(params);
		HttpPost request = new HttpPost(url);
		HttpEntity entity;

		StringEntity s = new StringEntity(c);
		s.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
				"application/json"));
		entity = s;
		request.setEntity(entity);
		HttpResponse response;
		response = httpclient.execute(request);
		return response;
	}
}
