package tw.com.SurveyApp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBConnection extends SQLiteOpenHelper {
	private final static String DBNAME = "mydb";
	private final static int VERSION = 6;

	public DBConnection(Context context) {
		super(context, DBNAME, null, VERSION);
	}

	@Override
	/**
	 * �إ߸�Ʈw
	 */
	public void onCreate(SQLiteDatabase db) {

		String sqlstr = "create table Questionnaire("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "SurveyGuid nvarchar(10)," 
				+ "Title nvarchar(50),"
				+ "GreetingText nvarchar(50)," 
				+ "ThankText nvarchar(50),"
				+ "TotalRows nvarchar(10)," 
				+ "Ver nvarchar(10),"
				+ "CDateTime nvarchar(10)," 
				+ "UDateTime nvarchar(10),"
				+ "CompletedRow nvarchar(10)," 
				+ "IntervieweeRows nvarchar(10)"
				+ ")";
		db.execSQL(sqlstr);

		sqlstr = "create table Sample("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "SampleGuid nvarchar(10)," 
				+ "SurveyGuid nvarchar(10),"
				+ "Name nvarchar(10)," 
				+ "Age nvarchar(3),"
				+ "ZipCode nvarchar(8)," 
				+ "Address nvarchar(50),"
				+ "Phone nvarchar(15)," 
				+ "isupdate nvarchar(2),"
				+ "AnswerDate nvarchar(10),"
				+ "MainPriority INTEGER(11),"
				+ "SubPriority INTEGER(11)"
				+ ")";
		db.execSQL(sqlstr);
		
		sqlstr = "create table SampleLog("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "SampleGuid nvarchar(10)," 
				+ "SurveyGuid nvarchar(10),"
				+ "Sindex nvarchar(2),"
				+ "Start nvarchar(20),"
				+ "End nvarchar(20)"
				+ ")";
		db.execSQL(sqlstr);
		
		sqlstr = "create table StatusLog("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "Step navrchar(2),"
				+ "StatusNumber nvarchar(4),"
				+ "Name nvarchar(50)"
				+ ")";
		db.execSQL(sqlstr);
		
		sqlstr = "create table StatusData("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "SampleGuid navrchar(10),"
				+ "SurveyGuid nvarchar(10),"
				+ "SituationCode nvarchar(5),"
				+ "Comment nvarchar(50),"
				+ "CDateTime nvarchar(20)"
				+ ")";
		db.execSQL(sqlstr);

		sqlstr = "create table Subject("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "SurveyGuid nvarchar(10)," 
				+ "SubjectIndex nvarchar(10),"
				+ "SubjectType nvarchar(5)," 
				+ "SubjectTitle nvarchar(50),"
				+ "MultiSelectedLimit nvarchar(10), "
				+ "DynamicOption nvarchar(10),"
				+ "DynamicOptionAnswer nvarchar(10),"
				+ "GroupOption nvarchar(10),"
				+ "SubjectOpiton nvarchar(10),"
				+ "SubjectIsRandom nvarchar(10),"
				+ "OptionOutputForm nvarchar(10)"
				+ ")";
		db.execSQL(sqlstr);

		sqlstr = "create table Option("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "SurveyGuid nvarchar(10)," 
				+ "SubjectIndex nvarchar(10),"
				+ "optionIndex nvarchar(10),"
				+ "optionText nvarchar(20),"
				+ "optionValue nvarchar(10),"
				+ "optionIsShowTextView nvarchar(10),"
				+ "optionTextViewPosition nvarchar(10),"
				+ "optionPickerValueArray nvarchar(10),"
				+ "optionDyPickerSubject nvarchar(10),"
				+ "optionDyPickerMin nvarchar(10),"
				+ "optionDyPickerMax nvarchar(10),"
				+ "optionDyPickerTick nvarchar(10)"
				+ ")";
		db.execSQL(sqlstr);

		sqlstr = "create table SinglePsaa("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "SurveyGuid nvarchar(10)," 
				+ "SubjectIndex nvarchar(10),"
				+ "answer nvarchar(10)," 
				+ "jumpto nvarchar(10),"
				+ "isselect nvarchar(2)" + ")";
		db.execSQL(sqlstr);

		sqlstr = "create table GroupPsaa("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "SurveyGuid nvarchar(10)," 
				+ "SubjectIndex nvarchar(10),"
				+ "subjectList nvarchar(10)," 
				+ "answerList nvarchar(10),"
				+ "jumpto nvarchar(10)" + ")";
		db.execSQL(sqlstr);

		sqlstr = "create table Interview("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "Guid nvarchar(50)," 
				+ "Name nvarchar(10),"
				+ "Email nvarchar(50)," 
				+ "Password nvarchar(10)" + ")";
		db.execSQL(sqlstr);

		sqlstr = "create table Answer("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "SurveyGuid nvarchar(10)," 
				+ "InterviewerID nvarchar(10),"
				+ "SampleID nvarchar(10)," 
				+ "Previous nvarchar(10),"
				+ "SubjectIndex nvarchar(10)," 
				+ "Answer nvarchar(10)" + ")";
		db.execSQL(sqlstr);

		sqlstr = "create table Tip("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "SurveyGuid nvarchar(10)," 
				+ "SubjectIndex nvarchar(10),"
				+ "tip_index nvarchar(10)," 
				+ "tip_name nvarchar(10),"
				+ "tip_image nvarchar(50)," 
				+ "tip_text nvarchar(255)" + ")";
		db.execSQL(sqlstr);

		sqlstr = "create table DynamicOption("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "SurveyGuid nvarchar(10)," 
				+ "dyoptionguid nvarchar(10),"
				+ "subject_accordingKey nvarchar(10),"
				+ "subject_subjectIndex nvarchar(10),"
				+ "accordingValue nvarchar(10),"
				+ "value nvarchar(10)," 
				+ "subject_index nvarchar(10),"
				+ "text nvarchar(10)" 
				+ ")";
		db.execSQL(sqlstr);
		
		sqlstr = "create table DynamicOptionByAnswer("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "SurveyGuid nvarchar(10)," 
				+ "DyaGuid nvarchar(10),"
				+ "SubjectNumber nvarchar(10),"
				+ "TargetAnswer nvarchar(10),"
				+ "IsShow nvarchar(10),"
				+ "TargetOptionIndex nvarchar(10),"
				+ "IsOr nvarchar(10)"
				+ ")";
		db.execSQL(sqlstr);
		
		sqlstr = "create table GroupOption("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "SurveyGuid nvarchar(10)," 
				+ "GoGuid nvarchar(10),"
				+ "GroupId nvarchar(10),"
				+ "GroupType nvarchar(10),"
				+ "GroupIndex nvarchar(10),"
				+ "GroupText nvarchar(10),"
				+ "GroupValue nvarchar(10),"
				+ "isShowTextView nvarchar(2),"
				+ "textViewPosition nvarchar(6)"
				+ ")";
		db.execSQL(sqlstr);
		
		sqlstr = "create table UpdataLog("
				+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "SurveyGuid nvarchar(10)," 
				+ "SampleGuid nvarchar(10)" 
				+ ")";
		db.execSQL(sqlstr);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String sqlstr = "";
		
		switch(oldVersion){
		case 2:
			sqlstr = "drop table Option";
			db.execSQL(sqlstr);
			
			sqlstr = "create table Option("
					+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ "SampleGuid nvarchar(10)," 
					+ "SurveyGuid nvarchar(10),"
					+ "optionIndex nvarchar(10),"
					+ "optionText nvarchar(20),"
					+ "optionValue nvarchar(10),"
					+ "optionIsShowTextView nvarchar(10),"
					+ "optionTextViewPosition nvarchar(10),"
					+ "optionPickerValueArray nvarchar(10),"
					+ "optionDyPickerSubject nvarchar(10),"
					+ "optionDyPickerMin nvarchar(10),"
					+ "optionDyPickerMax nvarchar(10),"
					+ "optionDyPickerTick nvarchar(10)"
					+ ")";
			db.execSQL(sqlstr);
		case 3:
			sqlstr = "create table UpdataLog("
					+ "serial INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ "SurveyGuid nvarchar(10)," 
					+ "SampleGuid nvarchar(10)" 
					+ ")";
			db.execSQL(sqlstr);
		case 4:
			sqlstr = "alter table Subject"
					+ " add column "
					+ " OptionOutputForm nvarchar(10)";
			db.execSQL(sqlstr);
		}
	}
}
