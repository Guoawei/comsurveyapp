package tw.com.SurveyApp.view;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.R;
import tw.com.SurveyApp.data.JsonConnection;
import tw.com.SurveyApp.service.JsonService;
import tw.com.SurveyApp.service.QuestionnaireService;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;

public class QuestionnaireView extends Activity implements OnTouchListener {
	private ListView listview = null;
	private ArrayList<HashMap<String, String>> listdata = null;
	private String Guid = "";
	private JsonConnection jc = new JsonConnection();
	private JsonService js = new JsonService();
	private ProgressDialog progressDialog = null;
	private ImageButton btnUpdateDowm, logoutbutton;
	private LinearLayout llbg;

	int myProgress = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.questionnairlist);

		// 處理ListView的資料
		listview = (ListView) findViewById(R.id.listViewQuestionnairlist);

		// 指定標題文字
		LayoutInflater flater = LayoutInflater.from(QuestionnaireView.this);
		listview.addHeaderView(flater.inflate(R.layout.questionnairlisttitle,
				null));

		btnUpdateDowm = (ImageButton) findViewById(R.id.btnUpdateDowm);
		logoutbutton = (ImageButton) findViewById(R.id.logoutbutton);
		btnUpdateDowm.setId(R.id.btnUpdateDowm);
		logoutbutton.setId(R.id.logoutbutton);

		if (!isWifi()) {
			btnUpdateDowm.setEnabled(false);
		}
		btnUpdateDowm.setEnabled(true);

		LoadData();

		Bundle extras = getIntent().getExtras();
		Guid = extras.getString("Guid");

		btnUpdateDowm.setOnTouchListener(this);
		logoutbutton.setOnTouchListener(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		ImageButton ib = (ImageButton) v;
		switch (ib.getId()) {
		case R.id.btnUpdateDowm:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setImageResource(R.drawable.updatebutton_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setImageResource(R.drawable.updatebutton);
			}
			break;
		case R.id.logoutbutton:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setImageResource(R.drawable.logoutbutton_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setImageResource(R.drawable.logoutbutton);
			}
			break;
		}
		return false;
	}

	/**
	 * 查詢是否有Wifi，如果有回傳true, 如果沒有回傳false
	 * 
	 * @return
	 */
	private boolean isWifi() {
		ConnectivityManager connectivityManager = (ConnectivityManager) QuestionnaireView.this
				.getSystemService(LoginView.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
		if (activeNetInfo != null
				&& activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {
			return true;
		}
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				|| keyCode == KeyEvent.KEYCODE_HOME) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * 載入資料
	 */
	private void LoadData() {
		progressDialog = ProgressDialog.show(QuestionnaireView.this, "載入中....",
				"資料處理中。。。", true);

		new Thread() {
			public void run() {
				progressDialog.dismiss();

				cwjHandler.post(mUpdateResults);
			}
		}.start();
	}

	/**
	 * 回首頁-按鈕
	 * 
	 * @param v
	 */
	public void end(View v) {
		System.exit(0);
	}

	/**
	 * 取得Json檔
	 */
	private void getReadJson() {
		StringBuilder url = new StringBuilder();
		url.append(JsonConnection.connection).append("getSurveyList?")
				.append("guid=").append(Guid);
		String strResult = jc.connServerForResult(url.toString());

		String doc = js.readTrue(strResult);

		if (!doc.equals("0")) {
			return;
		}

		String surveyguid[] = js.readQuestionnaireList(strResult,
				QuestionnaireView.this);

		for (String sg : surveyguid) {

			url = new StringBuilder();
			url.append(JsonConnection.connection).append("getQuestion?")
					.append("surveyguid=").append(sg);

			strResult = jc.connServerForResult(url.toString());

			doc = js.readTrue(strResult);

			if (!doc.equals("0")) {
				continue;
			}

			js.readQuestionnaire(strResult, sg, QuestionnaireView.this);
		}

		for (String sg : surveyguid) {
			url = new StringBuilder();
			url.append(JsonConnection.connection).append("getSample?")
					.append("surveyguid=").append(sg).append("&")
					.append("interviewerguid=").append(Guid);

			strResult = jc.connServerForResult(url.toString());
			doc = js.readTrue(strResult);

			if (!doc.equals("0")) {
				continue;
			}

			js.readSample(strResult, QuestionnaireView.this);
		}
	}

	final Handler cwjHandler = new Handler();

	final Runnable mUpdateResults = new Runnable() {
		public void run() {
			// 顯示ListView
			setQuestionnaireListView(QuestionnaireView.this, listview);
		}
	};

	/**
	 * 更新-按鈕
	 * 
	 * @param v
	 */
	public void update(View v) {

		btnUpdateDowm.setEnabled(false);

		progressDialog = ProgressDialog.show(QuestionnaireView.this, "下載中....",
				"資料處理中。。。", true);

		new Thread() {
			public void run() {
				getReadJson();

				progressDialog.dismiss();

				cwjHandler.post(mUpdateResults);
			}
		}.start();

		btnUpdateDowm.setEnabled(true);
	}

	public void setQuestionnaireListView(Context context, ListView listview) {
		// 指定ListView 的記錄內

		listdata = QuestionnaireService
				.queryQuestionnaireAll(QuestionnaireView.this);

		if (listdata != null) {
			SimpleAdapter adapter = new SimpleAdapter(context, listdata,
					R.layout.questionnairlistrow, new String[] { "Title",
							"CompletedRow", "IntervieweeRows", "Ver",
							"UDateTime" }, new int[] { R.id.textTitle,
							R.id.textCompletedRow, R.id.textIntervieweeRows,
							R.id.textVer, R.id.textUDateTime });
			listview.setAdapter(adapter);
		}

		// 匿名內部類別
		listview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				if (position > 0) {
					HashMap<String, String> single = listdata.get(position - 1);

					Intent intent = new Intent();
					intent.setClass(QuestionnaireView.this, CompletedView.class);
					intent.putExtra("SurveyGuid", single.get("SurveyGuid"));
					QuestionnaireView.this.startActivityForResult(intent, 0);
					//停止過場動畫
					overridePendingTransition(0, 0);
					
					llbg = (LinearLayout) arg1;
					llbg.setBackgroundResource(R.color.selected);
				}
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 0) {
			if (resultCode == 0) {
				setQuestionnaireListView(QuestionnaireView.this, listview);
			}
		}
	}

}
