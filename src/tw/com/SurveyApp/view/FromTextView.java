package tw.com.SurveyApp.view;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.R;
import tw.com.SurveyApp.dto.AnswerDto;
import tw.com.SurveyApp.service.AnswerService;
import tw.com.SurveyApp.service.GroupPsaaService;
import tw.com.SurveyApp.service.InterviewService;
import tw.com.SurveyApp.service.OptionService;
import tw.com.SurveyApp.service.SinglePsaaService;
import tw.com.SurveyApp.service.SubjectService;
import tw.com.SurveyApp.service.TipService;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 文字輸入 題型
 * 
 * @author sakata
 * 
 */
public class FromTextView extends Activity implements OnTouchListener {
	private ListView listview = null;
	private String SurveyGuid = "";
	private String SubjectIndex = "";
	private String SubjectTitle = "";
	private String Previous = "";
	private String serial = "";
	private String SampleID = "";
	private ArrayList<HashMap<String, String>> listdata = null;
	private ImageButton btnPrevious, btnNext;
	private ImageButton btnPreviousPage, btnNextPage;
	private ImageButton btnPicture;
	private TextView textTips, textSubjectIndexTitle;
	private HashMap<Integer, String> values = null;
	private ProgressDialog progressDialog = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.subject);

		/*
		 * by Awei 2013/04/16
		 * 把題號跟題目合併在一起
		 */
		textSubjectIndexTitle = (TextView) findViewById(R.id.textSubjectIndexTitle);		
		btnPicture = (ImageButton) findViewById(R.id.btnPicture);
		textTips = (TextView) findViewById(R.id.textTips);
		listview = (ListView) findViewById(R.id.listViewFormText);

		btnPreviousPage = (ImageButton) findViewById(R.id.btnPreviousPage);
		btnNextPage = (ImageButton) findViewById(R.id.btnNextPage);
		btnPrevious = (ImageButton) findViewById(R.id.btnPrevious);
		btnNext = (ImageButton) findViewById(R.id.btnNext);
		
		btnPrevious.setId(R.id.btnPrevious);
		btnNext.setId(R.id.btnNext);
		btnPreviousPage.setId(R.id.btnPreviousPage);
		btnNextPage.setId(R.id.btnNextPage);

		btnPreviousPage.setVisibility(View.GONE);
		btnNextPage.setVisibility(View.GONE);

		// 儲存傳入資料
		Bundle extras = getIntent().getExtras();
		SurveyGuid = extras.getString("SurveyGuid");
		SubjectIndex = extras.getString("SubjectIndex");
		SubjectTitle = extras.getString("SubjectTitle");
		Previous = extras.getString("Previous");
		SampleID = extras.getString("SampleID");

		// 設定題號和題目標題
		/*
		 * by Awei 2013/04/16
		 * 把題號跟題目合併在一起
		 */
		textSubjectIndexTitle.setText(SubjectIndex+" : "+SubjectTitle);

		// 如果是第一題的話隱藏「上一題」按鈕
		if (Previous.equals("null")) {
			btnPrevious.setEnabled(false);
//			btnPrevious.setVisibility(View.INVISIBLE);
		}

		LoadData();

		btnPrevious.setOnTouchListener(this);
		btnNext.setOnTouchListener(this);
		btnPreviousPage.setOnTouchListener(this);
		btnNextPage.setOnTouchListener(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		ImageButton ib = (ImageButton) v;
		switch (ib.getId()) {
		case R.id.btnPrevious:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.previousbtn_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.previousbtn);
			}
			break;
		case R.id.btnNext:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.nextbutton_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.nextbutton);
			}
			break;
		case R.id.btnPreviousPage:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.previousbtn_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.previousbtn);
			}
			break;
		case R.id.btnNextPage:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.nextbutton_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.nextbutton);
			}
			break;
		}
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				|| keyCode == KeyEvent.KEYCODE_HOME) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	final Handler cwjHandler = new Handler();

	final Runnable mUpdateResults = new Runnable() {
		public void run() {
			ArrayList<HashMap<String, String>> tipdata = TipService
					.queryAllTip(SurveyGuid, SubjectIndex, FromTextView.this);

			// 是否有提示卡，有的話顯示提示卡
			if (tipdata.size() == 0) {
				btnPicture.setVisibility(View.GONE);
				textTips.setVisibility(View.GONE);
			} else {
				if (tipdata.get(0).get("tip_image") != null
						&& !tipdata.get(0).get("tip_image").trim().equals("")) {
					textTips.setVisibility(View.GONE);
				} else {
					btnPicture.setVisibility(View.GONE);
					if (tipdata.get(0).get("tip_text") == null
							|| tipdata.get(0).get("tip_text").trim().equals("")) {
						textTips.setVisibility(View.GONE);
					} else {
						textTips.setText(tipdata.get(0).get("tip_text"));
					}
				}
			}

			// 顯示ListView
			values = setTextRow(FromTextView.this, listview, SubjectIndex);
		}
	};

	/**
	 * 載入資料
	 */
	private void LoadData() {
		progressDialog = ProgressDialog.show(FromTextView.this, "載入中....",
				"資料處理中。。。", true);

		new Thread() {
			public void run() {
				progressDialog.dismiss();

				cwjHandler.post(mUpdateResults);
			}
		}.start();
	}

	/**
	 * 上一題
	 * 
	 * @param v
	 */
	public void previous(View v) {
		Intent intent = new Intent();
		intent.putExtra("SurveyGuid", SurveyGuid);
		intent.putExtra("SubjectIndex", Previous);

		// 查詢「上一題」的「題號」
		SubjectIndex = AnswerService.getSubjectIndex(Previous, SampleID,
				FromTextView.this);

		intent.putExtra("Previous", SubjectIndex);

		this.setResult(RESULT_OK, intent);

		finish();
	}

	/**
	 * 提示卡圖片
	 * 
	 * @param v
	 */
	public void picture(View v) {
		Intent intent = new Intent();
		intent.setClass(FromTextView.this, TipImageView.class);
		FromTextView.this.startActivity(intent);
	}

	/**
	 * 取得代碼
	 */
	private void getserial() {
		// 查詢目前題目的代碼
		serial = SubjectService.getSerial(SurveyGuid, SubjectIndex,
				FromTextView.this);
	}

	/**
	 * 取得題號
	 */
	private void getSubjectIndex(String TableName) {
		// 取得下一題的題號
		if (TableName.equals("Subject")) {
			String Serial = Integer.parseInt(serial) + 1 + "";
			SubjectIndex = SubjectService.getSubjectIndex(SurveyGuid, Serial,
					FromTextView.this);
		} else if (TableName.equals("SinglePass")) {
			SubjectIndex = SinglePsaaService.getSubjectIndex(SurveyGuid,
					SubjectIndex, FromTextView.this);
		} else if (TableName.equals("GroupPsaa")) {
			SubjectIndex = GroupPsaaService.getSubjectIndex(SurveyGuid,
					SubjectIndex, FromTextView.this);
		}
	}

	/**
	 * 下一題
	 * 
	 * @param v
	 */
	public void next(View v) {
		btnNext.setEnabled(false);
		boolean nextSubject = true;
		// 檢查是否有正確作答
		if (values.size() != 0) {
			for (int i = 0; i < values.size(); i++) {
				if (values.get(i) == null || values.get(i).trim().equals("")
						|| values.get(i).trim().length() == 0) {
					Toast.makeText(FromTextView.this, "請輸入資料",
							Toast.LENGTH_LONG).show();
					nextSubject = false;
					btnNext.setEnabled(true);
					return;
				}
			}
		} else {
			Toast.makeText(FromTextView.this, "請輸入資料", Toast.LENGTH_LONG)
					.show();
			nextSubject = false;
			btnNext.setEnabled(true);
			return;
		}

		// 如果有正確作答的話
		if (nextSubject) {
			// 刪除上次作答的答案
			AnswerService.deleteAnswer(SurveyGuid, SubjectIndex, SampleID,
					FromTextView.this);

			for (int i = 0; i < values.size(); i++) {
				// 儲存本次作答的答案
				AnswerDto answerDto = new AnswerDto();
				answerDto.setSurveyGuid(SurveyGuid);
				answerDto.setInterviewerID(InterviewService
						.getInterviewGuid(FromTextView.this));
				answerDto.setSampleID(SampleID);
				answerDto.setPrevious(Previous);
				answerDto.setSubjectIndex(SubjectIndex);
				answerDto.setAnswer(values.get(i));

				AnswerService.cmdInsert(answerDto, FromTextView.this);
			}

			Previous = SubjectIndex;

			HashMap<String, String> SinglePsaa = SinglePsaaService
					.querySinglePsaaAnswer(SurveyGuid, SubjectIndex,
							FromTextView.this);

			ArrayList<HashMap<String, String>> GroupPsaa = GroupPsaaService
					.queryAllGroupPsaa(SurveyGuid, SubjectIndex,
							FromTextView.this);

			boolean isSP = false;
			boolean isGP = false;

			// 檢查是否有符合「群組跳題」的規則
			if (GroupPsaa.size() != 0) {
				int count = GroupPsaa.size();
				int sum = 0;

				for (int i = 0; i < GroupPsaa.size(); i++) {
					String subjectList = GroupPsaa.get(i).get("subjectList");
					String answerList = GroupPsaa.get(i).get("answerList");

					String answer = AnswerService.getAnswer(SurveyGuid,
							subjectList, SampleID, FromTextView.this);

					if (answer == null) {
						isGP = false;
						break;
					}

					if (answer.trim().equals(answerList.trim())) {
						sum++;
					}
				}

				if (count == sum) {
					getSubjectIndex("GroupPsaa");
					isGP = true;
				}
			}

			// 檢查是否有符合「單題跳題」的規則
			if (SinglePsaa.size() != 0 && isGP == false) {
				String Answer = SinglePsaa.get("0");

				for (int i = 0; i < values.size(); i++) {
					if (values.get(i).equals(Answer)) {
						getSubjectIndex("SinglePass");
						isSP = true;
						break;
					}
				}
			}

			// 不符合「單題跳題」和「群組跳題」
			if (isSP == false && isGP == false) {
				getserial();
				getSubjectIndex("Subject");
			}

			Intent intent = new Intent();
			intent.putExtra("SurveyGuid", SurveyGuid);
			intent.putExtra("SubjectIndex", SubjectIndex);
			intent.putExtra("Previous", Previous);

			this.setResult(RESULT_OK, intent);

			finish();
		}
	}

	public HashMap<Integer, String> setTextRow(Context context,
			ListView listview, String SubjectIndex) {

		// 取得所有題目選項標題
		listdata = OptionService.queryOption(SurveyGuid, SubjectIndex,
				FromTextView.this);

		// 取得前次作答所有答案內容
		ArrayList<HashMap<String, String>> answerdata = AnswerService
				.queryAnswer(SurveyGuid, SubjectIndex, SampleID,
						FromTextView.this);

		HashMap<Integer, String> values = null;

		if (listdata != null) {
			TextAdapter adapter = new TextAdapter(context, listdata, answerdata);
			listview.setAdapter(adapter);
			values = adapter.values;
			return values;
		}
		return values;
	}

	public class TextAdapter extends BaseAdapter {
		Context context;
		ArrayList<HashMap<String, String>> listData;
		ArrayList<HashMap<String, String>> answerdata;
		HashMap<Integer, String> values = new HashMap<Integer, String>();

		public TextAdapter(Context context,
				ArrayList<HashMap<String, String>> listData,
				ArrayList<HashMap<String, String>> answerdata) {
			this.context = context;
			this.listData = listData;
			this.answerdata = answerdata;
		}

		/**
		 * 取得資料筆數
		 */
		public int getCount() {
			return listData.size();
		}

		/**
		 * 取得資料列
		 */
		public Object getItem(int position) {
			return listData.get(position);
		}

		/**
		 * 取得資料ID
		 */
		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			LayoutInflater mInflater = LayoutInflater.from(context);
			if("back".equals(((String) listData.get(position).get("optionTextViewPosition")))){
				convertView = mInflater.inflate(R.layout.textrowback, null);
			}else {
				convertView = mInflater.inflate(R.layout.textrow, null);
			}
			

			EditText editOption = (EditText) convertView
					.findViewById(R.id.editOption);
			// 如果之前有作答過，顯示上次作答內容
			if (answerdata.size() != 0) {
				editOption.setText(answerdata.get(position).get("Answer"));
				values.put(position, answerdata.get(position).get("Answer"));
			}
			// 顯示「選項」內容
			TextView textOption = (TextView) convertView
					.findViewById(R.id.textOption);

			textOption.setText((String) listData.get(position)
					.get("optionText"));

			editOption.setOnFocusChangeListener(new OnFocusChangeListener() {

				public void onFocusChange(View v, boolean hasFocus) {
					EditText option = (EditText) v;
					values.put(position, option.getText().toString());
				}

			});
			return convertView;
		}
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			this.setResult(RESULT_CANCELED);
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, R.string.Endanswer);
		return super.onCreateOptionsMenu(menu);
	}
}
