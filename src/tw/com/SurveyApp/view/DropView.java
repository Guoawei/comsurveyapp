package tw.com.SurveyApp.view;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.R;
import tw.com.SurveyApp.UI.ArrayWheelAdapter;
import tw.com.SurveyApp.UI.OnWheelChangedListener;
import tw.com.SurveyApp.UI.WheelView;
import tw.com.SurveyApp.dto.AnswerDto;
import tw.com.SurveyApp.service.AnswerService;
import tw.com.SurveyApp.service.GroupPsaaService;
import tw.com.SurveyApp.service.InterviewService;
import tw.com.SurveyApp.service.OptionService;
import tw.com.SurveyApp.service.SinglePsaaService;
import tw.com.SurveyApp.service.SubjectService;
import tw.com.SurveyApp.service.TipService;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DropView extends Activity implements OnTouchListener {
	private ListView listview = null;
	private String SurveyGuid = "";
	private String SubjectIndex = "";
	private String SubjectTitle = "";
	private String Previous = "";
	private String serial = "";
	private String SampleID = "";
	private ArrayList<HashMap<String, String>> listdata = null;
	private ImageButton btnPrevious, btnNext;
	private ImageButton btnPreviousPage, btnNextPage;
	private ImageButton btnPicture;
	private TextView textTips, textSubjectIndexTitle;
	private HashMap<Integer, String> values = null;
	private HashMap<Integer, String> index = null;
	private HashMap<Integer, String> answers = null; //Add by Awei 2013/04/13
	private int[] subjectIndex;
//	private ProgressDialog progressDialog = null;
	private boolean isInitWheelViewFinish = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.subject);

		/*
		 * by Awei 2013/04/16
		 * 把題號跟題目合併在一起
		 */
		textSubjectIndexTitle = (TextView) findViewById(R.id.textSubjectIndexTitle);
		
		btnPicture = (ImageButton) findViewById(R.id.btnPicture);
		textTips = (TextView) findViewById(R.id.textTips);
		listview = (ListView) findViewById(R.id.listViewFormText);
		/*
		 * Add By Awei 
		 * 2013/05/26
		 * V 1.1.19
		 * 新增捲軸永久顯示
		 */
		listview.setScrollbarFadingEnabled(false);
		
		btnPrevious = (ImageButton) findViewById(R.id.btnPrevious);
		btnNext = (ImageButton) findViewById(R.id.btnNext);
		btnPreviousPage = (ImageButton) findViewById(R.id.btnPreviousPage);
		btnNextPage = (ImageButton) findViewById(R.id.btnNextPage);

		btnPrevious.setId(R.id.btnPrevious);
		btnNext.setId(R.id.btnNext);
		btnPreviousPage.setId(R.id.btnPreviousPage);
		btnNextPage.setId(R.id.btnNextPage);

		btnPreviousPage.setVisibility(View.GONE);
		btnNextPage.setVisibility(View.GONE);

		index = new HashMap<Integer, String>();

		// 儲存傳入資料
		Bundle extras = getIntent().getExtras();
		SurveyGuid = extras.getString("SurveyGuid");
		SubjectIndex = extras.getString("SubjectIndex");
		SubjectTitle = extras.getString("SubjectTitle");
		Previous = extras.getString("Previous");
		SampleID = extras.getString("SampleID");

		// 設定題號和題目標題
		/*
		 * by Awei 2013/04/16
		 * 把題號跟題目合併在一起
		 */
		textSubjectIndexTitle.setText(SubjectIndex+" : "+SubjectTitle);
		

		// 如果是第一題的話隱藏「上一題」按鈕
		if (Previous.equals("null")) {
			btnPrevious.setEnabled(false);
//			btnPrevious.setVisibility(View.INVISIBLE);
		}
		btnNext.setEnabled(false);
		LoadData();

		btnPrevious.setOnTouchListener(this);
		btnNext.setOnTouchListener(this);
		btnPreviousPage.setOnTouchListener(this);
		btnNextPage.setOnTouchListener(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		ImageButton ib = (ImageButton) v;
		switch (ib.getId()) {
		case R.id.btnPrevious:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.previousbtn_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.previousbtn);
			}
			break;
		case R.id.btnNext:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.nextbutton_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.nextbutton);
			}
			break;
		case R.id.btnPreviousPage:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.previousbtn_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.previousbtn);
			}
			break;
		case R.id.btnNextPage:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.nextbutton_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.nextbutton);
			}
			break;
		}
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				|| keyCode == KeyEvent.KEYCODE_HOME) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	final Handler cwjHandler = new Handler();

	final Runnable mUpdateResults = new Runnable() {
		public void run() {
			ArrayList<HashMap<String, String>> tipdata = TipService
					.queryAllTip(SurveyGuid, SubjectIndex, DropView.this);

			// 是否有提示卡，有的話顯示提示卡
			if (tipdata.size() == 0) {
				btnPicture.setVisibility(View.GONE);
				textTips.setVisibility(View.GONE);
			} else {
				if (tipdata.get(0).get("tip_image") != null
						&& !tipdata.get(0).get("tip_image").trim().equals("")) {
					textTips.setVisibility(View.GONE);
				} else {
					btnPicture.setVisibility(View.GONE);
					if (tipdata.get(0).get("tip_text") == null
							|| tipdata.get(0).get("tip_text").trim().equals("")) {
						textTips.setVisibility(View.GONE);
					} else {
						textTips.setText(tipdata.get(0).get("tip_text"));
					}
				}
			}

			// 顯示ListView
			values = setTextRow(DropView.this, listview, SubjectIndex);
			answers = values; //Add by Awei 2013/04/13 
		}
	};

	/**
	 * 載入資料
	 */
	private void LoadData() {
//		progressDialog = ProgressDialog.show(DropView.this, "載入中....",
//				"資料處理中。。。", true);
//
//		new Thread() {
//			public void run() {
//				progressDialog.dismiss();

				cwjHandler.post(mUpdateResults);
//			}
//		}.start();
	}

	/**
	 * 上一題
	 * 
	 * @param v
	 */
	public void previous(View v) {
		Intent intent = new Intent();
		intent.putExtra("SurveyGuid", SurveyGuid);
		intent.putExtra("SubjectIndex", Previous);

		// 查詢「上一題」的「題號」
		SubjectIndex = AnswerService.getSubjectIndex(Previous, SampleID,
				DropView.this);

		intent.putExtra("Previous", SubjectIndex);

		this.setResult(RESULT_OK, intent);

		finish();
		//停止過場動畫
		overridePendingTransition(0, 0);
	}

	/**
	 * 提示卡圖片
	 * 
	 * @param v
	 */
	public void picture(View v) {
		Intent intent = new Intent();
		intent.setClass(DropView.this, TipImageView.class);
		DropView.this.startActivity(intent);
	}

	/**
	 * 取得代碼
	 */
	private void getserial() {
		// 查詢目前題目的代碼
		serial = SubjectService.getSerial(SurveyGuid, SubjectIndex,
				DropView.this);
	}

	/**
	 * 取得題號
	 */
	private void getSubjectIndex(String TableName) {
		// 取得下一題的題號
		if (TableName.equals("Subject")) {
			String Serial = Integer.parseInt(serial) + 1 + "";
			SubjectIndex = SubjectService.getSubjectIndex(SurveyGuid, Serial,
					DropView.this); 
		} else if (TableName.equals("SinglePass")) {
			SubjectIndex = SinglePsaaService.getSubjectIndex(SurveyGuid,
					SubjectIndex, DropView.this);
		} else if (TableName.equals("GroupPsaa")) {
			SubjectIndex = GroupPsaaService.getSubjectIndex(SurveyGuid,
					SubjectIndex, DropView.this);
		}
	}

	/**
	 * 下一題
	 * 
	 * @param v
	 */
	public void next(View v) {

		if(checkAnswerHavePleaseSelectText(answers)){
			Toast.makeText(DropView.this, "請做答", Toast.LENGTH_LONG).show();
			return;
		}
		btnNext.setEnabled(false);
		// 刪除上次作答的答案
		AnswerService.deleteAnswer(SurveyGuid, SubjectIndex, SampleID,
				DropView.this);

		for (int i = 0; i < answers.size(); i++) { // by Awei 2013/04/13
			// 儲存本次作答的答案
			AnswerDto answerDto = new AnswerDto();
			answerDto.setSurveyGuid(SurveyGuid);
			answerDto.setInterviewerID(InterviewService
					.getInterviewGuid(DropView.this));
			answerDto.setSampleID(SampleID);
			answerDto.setPrevious(Previous);
			answerDto.setSubjectIndex(SubjectIndex);
			answerDto.setAnswer(answers.get(i));
//			System.out.println("answer:"+answers.get(i)); // by Awei 2013/04/13
			AnswerService.cmdInsert(answerDto, DropView.this);

			Previous = SubjectIndex;
		}

		HashMap<String, String> SinglePsaa = SinglePsaaService
				.querySinglePsaaAnswer(SurveyGuid, SubjectIndex, DropView.this);

		ArrayList<HashMap<String, String>> GroupPsaa = GroupPsaaService
				.queryAllGroupPsaa(SurveyGuid, SubjectIndex, DropView.this);

		boolean isSP = false;
		boolean isGP = false;

		// 檢查是否有符合「群組跳題」的規則
		if (GroupPsaa.size() != 0) {
			int count = GroupPsaa.size();
			int sum = 0;

			for (int j = 0; j < GroupPsaa.size(); j++) {
				String subjectList = GroupPsaa.get(j).get("subjectList");
				String answerList = GroupPsaa.get(j).get("answerList");

				String answer = AnswerService.getAnswer(SurveyGuid,
						subjectList, SampleID, DropView.this);

				if (answer == null) {
					isGP = false;
					break;
				}

				if (answer.trim().equals(answerList.trim())) {
					sum++;
				}
			}

			if (count == sum) {
				getSubjectIndex("GroupPsaa");
				isGP = true;
			}
		}

		// 檢查是否有符合「單題跳題」的規則
		if (SinglePsaa.size() != 0 && isGP == false) {
			String Answer = SinglePsaa.get("0");

			for (int j = 0; j < answers.size(); j++) { // by Awei 2013/04/13
				if (answers.get(j).equals(Answer)) {  // by Awei 2013/04/13
					getSubjectIndex("SinglePass");
					isSP = true;
					break;
				}
			}
		}

		// 不符合「單題跳題」和「群組跳題」
		if (isSP == false && isGP == false) {
			getserial();
			getSubjectIndex("Subject");
		}

		Intent intent = new Intent();
		intent.putExtra("SurveyGuid", SurveyGuid);
		intent.putExtra("SubjectIndex", SubjectIndex);
		intent.putExtra("Previous", Previous);

		this.setResult(RESULT_OK, intent);

		finish();
		//停止過場動畫
		overridePendingTransition(0, 0);
	}

	public HashMap<Integer, String> setTextRow(Context context,
			ListView listview, String SubjectIndex) {

		// 取得所有題目選項標題
		listdata = OptionService.queryOption(SurveyGuid, SubjectIndex,
				DropView.this);
				
		subjectIndex = new int[listdata.size()];
		for (int i = 0; i < subjectIndex.length; i++) {
			subjectIndex[i] = 0;
		}

		// 取得前次作答所有答案內容
		ArrayList<HashMap<String, String>> answerdata = AnswerService
				.queryAnswer(SurveyGuid, SubjectIndex, SampleID, DropView.this);
//		System.out.println("answerdata:"+answerdata);
		HashMap<Integer, String> values = null;

		if (listdata != null) {
			TextAdapter adapter = new TextAdapter(context, listdata, answerdata);
			listview.setAdapter(adapter);
			values = adapter.values1;
			return values;
		}
		return values;
	}

	public class TextAdapter extends BaseAdapter {
		Context context;
		ArrayList<HashMap<String, String>> listData;
		ArrayList<HashMap<String, String>> answerdata;
		HashMap<Integer, String> values1 = new HashMap<Integer, String>();

		public TextAdapter(Context context,
				ArrayList<HashMap<String, String>> listData,
				ArrayList<HashMap<String, String>> answerdata) {
			this.context = context;
			this.listData = listData;
			this.answerdata = answerdata;
			
		}

		/**
		 * 取得資料筆數
		 */
		public int getCount() {
			return listData.size();
		}

		/**
		 * 取得資料列
		 */
		public Object getItem(int position) {
			return listData.get(position);
		}

		/**
		 * 取得資料ID
		 */
		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			
			LayoutInflater mInflater = LayoutInflater.from(context);
			convertView = mInflater.inflate(R.layout.droprow, null);
			
			String menu[] = null;
			
			if (listData.get(position).get("optionPickerValueArray").trim()
					.equals("")) {
				String subject = listData.get(position).get(
						"optionDyPickerSubject");
//				System.out.println("wheel.getId()"+position+":"+subject);
				// 取得前次作答所有答案內容
				/*
				 * by Awei 2013/04/17
				 * 抓陣列，可以取到多個答案
				 */
//				String ans = AnswerService.getAnswer(SurveyGuid, subject,
//				SampleID, context);
//				if (ans == null) {
//					ans = "1";
//				}
				ArrayList<HashMap<String, String>> assignSubjectAnswer = AnswerService
						.queryAnswer(SurveyGuid, subject, SampleID, DropView.this);

				int ans = 1;
				
				if (assignSubjectAnswer.size() != 0) {
					ans = Integer.parseInt(assignSubjectAnswer.get(0).get("Answer"));
				}
				
				String min = listData.get(position).get("optionDyPickerMin");
				String max = listData.get(position).get("optionDyPickerMax");
				int tick = Integer.parseInt(listData.get(position).get("optionDyPickerTick"));
				
				int index = 0;
				if (subject.equals(max)) { //檢誤功能，這裡是根據之前的答案做為最大值
					int minValue = Integer.parseInt(min);
					menu = new String[(ans - minValue + 1) / tick];
					for (int i = minValue; i <= ans; i = i + tick) {
						menu[index++] = String.valueOf(i);
					}
				} else if (subject.equals(min)) { //檢誤功能，這裡是根據之前的答案做為最小值
					int maxValue = Integer.parseInt(max);
					menu = new String[(maxValue - ans + 1) / tick];
					for (int i = ans; i <= maxValue; i = i + tick) {
						menu[index++] = String.valueOf(i);
					}
				} else { //非檢誤功能
					menu = new String[2];
					menu[0] = "0";
					menu[1] = "1";
				}
			} else {
				menu = listData.get(position).get("optionPickerValueArray").split(",");				
			}

			menu = addPleaseSelectTextAtFirst(menu);
			
			WheelView hous = (WheelView) convertView.findViewById(R.id.hour);
			
			hous.setViewAdapter(new ArrayWheelAdapter<String>(context, menu));
			hous.setLabel((String) listData.get(position).get("optionText"));
			hous.setVisibleItems(5);
			hous.addChangingListener(changedListener);
						
			hous.setId(position);

			

			if(!isInitWheelViewFinish){
//				index.put(position,(String) listData.get(position).get("optionPickerValueArray"));
//				values.put(position, menu[0]);

				// 如果之前有作答過，顯示上次作答內容
				if (answerdata.size() != 0) {
					if (!(position > 0 && answerdata.size() < subjectIndex.length)) {
						// 如果1個以上，但答案又不足的話
						if (Double.valueOf(answerdata.get(position).get("Answer")) <= 0.0) {
							values1.put(position, menu[0]);
						} else {
							values1.put(
									position,
									Double.valueOf(answerdata.get(position).get(
											"Answer"))
											- 1 + "");
						}

						for (int i = 0; i < menu.length; i++) {
							if (menu[i].trim().equals(
									answerdata.get(position).get("Answer").trim())) {
								hous.setCurrentItem(i);
								subjectIndex[position] = i;
								break;
							}
						}
					}

				}
				
//				System.out.println("listDataSize:"+listData.size()+"   position:"+position);
				//當已經沒有Wheel元件要初使化，將布林值設定true，訪員選定的答案就不會被改變
				if(position == listData.size()-1){
					isInitWheelViewFinish = true;
					btnNext.setEnabled(true);
				}
			}
						
			hous.setCurrentItem(subjectIndex[position]);
			answers.put(position, menu[subjectIndex[position]]);
			return convertView;
		}
	}
	
	private final OnWheelChangedListener changedListener = new OnWheelChangedListener() {
		public void onChanged(WheelView wheel, int oldValue, int newValue) {
						
			String menu[] = null;
			if (listdata.get(wheel.getId()).get("optionPickerValueArray").trim().equals("")) {
				
				//檢誤上一題的下拉式選單題號
				String subject = listdata.get(wheel.getId()).get("optionDyPickerSubject");
				
				/*
				 * by Awei 2013/04/17
				 * 抓陣列，可以取到多個答案
				 */
//				String ans = AnswerService.getAnswer(SurveyGuid, subject,
//						SampleID, DropView.this);

//				if (ans == null) {
//					ans = "1";
//				}
				
				ArrayList<HashMap<String, String>> assignSubjectAnswer = AnswerService
						.queryAnswer(SurveyGuid, subject, SampleID, DropView.this);
				
				int ans = 1;
				
				if (assignSubjectAnswer.size() != 0) {
					ans = Integer.parseInt(assignSubjectAnswer.get(0).get("Answer"));
				}
				
				String min = listdata.get(wheel.getId()).get("optionDyPickerMin");
				String max = listdata.get(wheel.getId()).get("optionDyPickerMax");
				int tick = Integer.parseInt(listdata.get(wheel.getId()).get("optionDyPickerTick"));
				
				int index = 0;

				if (subject.equals(min)) {//檢誤功能，這裡是根據之前的答案做為最小值
					int maxValue = Integer.parseInt(max);
					menu = new String[(maxValue - ans + 1) / tick];
					for (int i = ans; i <= maxValue; i = i + tick) {
						menu[index++] = String.valueOf(i);
					}
				} else {
					int minValue = Integer.parseInt(min);
					menu = new String[(ans - minValue + 1) / tick];
					for (int i = minValue; i <= ans; i = i + tick) {
						menu[index++] = String.valueOf(i);
					}
				}
			} else {
				menu = listdata.get(wheel.getId()).get("optionPickerValueArray").split(",");
			}
			menu = addPleaseSelectTextAtFirst(menu);
			
			answers.put(wheel.getId(), menu[wheel.getCurrentItem()]);
//			System.out.println("id:"+wheel.getId()+"  Wheel:"+wheel.getCurrentItem());
			
			subjectIndex[wheel.getId()] = wheel.getCurrentItem();
		}
	};

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			this.setResult(RESULT_CANCELED);
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, R.string.Endanswer);
		return super.onCreateOptionsMenu(menu);
	}

	private String[] addPleaseSelectTextAtFirst(String[] aMenu){
		String[] newMenu = new String[aMenu.length+1];
		newMenu[0] = "請選擇";
		
		for(int i=0;i<aMenu.length;i++){
			newMenu[i+1] = aMenu[i];
		}
		return newMenu;
	}
	
	private Boolean checkAnswerHavePleaseSelectText(HashMap<Integer, String> answerMap){
		for(int i=0;i<answerMap.size();i++){
			if("請選擇".equals(answerMap.get(i))){
				return true;
			}
		}
		return false;
	}
}
