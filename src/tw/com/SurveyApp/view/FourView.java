package tw.com.SurveyApp.view;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.R;
import tw.com.SurveyApp.dto.AnswerDto;
import tw.com.SurveyApp.service.AnswerService;
import tw.com.SurveyApp.service.DeleteAnsweredService;
import tw.com.SurveyApp.service.GroupPsaaService;
import tw.com.SurveyApp.service.InterviewService;
import tw.com.SurveyApp.service.OptionService;
import tw.com.SurveyApp.service.SinglePsaaService;
import tw.com.SurveyApp.service.SubjectService;
import tw.com.SurveyApp.service.TipService;
import android.app.Activity;
//import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 四分 題型
 * 
 * @author sakata
 * 
 */
public class FourView extends Activity implements OnClickListener,
		OnTouchListener {
	private String SurveyGuid = "";
	private String SubjectIndex = "";
	private String SubjectTitle = "";
	private String Previous = "";
	private String serial = "";
	private String SampleID = "";
	private ArrayList<HashMap<String, String>> listdata = null;
	private ImageButton btnPicture,btnNext;
	private TextView textTips, textSubjectIndexTitle;
	private TextView textview[];
	private RadioButton radio[];
	private HashMap<Integer, String> index = null;
//	private ProgressDialog progressDialog = null;
	private int temp = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.four);

		index = new HashMap<Integer, String>();

		/*
		 * by Awei 2013/04/16
		 * 把題號跟題目合併在一起
		 */
		textSubjectIndexTitle = (TextView) findViewById(R.id.textSubjectIndexTitle);
		textview = new TextView[4];
		textview[0] = (TextView) findViewById(R.id.textFiveOne);
		textview[1] = (TextView) findViewById(R.id.textFiveTwo);
		textview[2] = (TextView) findViewById(R.id.textFiveThree);
		textview[3] = (TextView) findViewById(R.id.textFiveFour);
		radio = new RadioButton[4];
		radio[0] = (RadioButton) findViewById(R.id.radio1);
		radio[1] = (RadioButton) findViewById(R.id.radio2);
		radio[2] = (RadioButton) findViewById(R.id.radio3);
		radio[3] = (RadioButton) findViewById(R.id.radio4);

		for (int i = 0; i < radio.length; i++) {
			radio[i].setId(i);
			radio[i].setOnClickListener(this);
		}

		btnPicture = (ImageButton) findViewById(R.id.btnPicture);
		textTips = (TextView) findViewById(R.id.textTips);

		ImageButton btnPrevious = (ImageButton) findViewById(R.id.btnPrevious);
		btnNext = (ImageButton) findViewById(R.id.btnNext);
		btnPrevious.setId(R.id.btnPrevious);
		btnNext.setId(R.id.btnNext);

		// 儲存傳入資料
		Bundle extras = getIntent().getExtras();
		SurveyGuid = extras.getString("SurveyGuid");
		SubjectIndex = extras.getString("SubjectIndex");
		SubjectTitle = extras.getString("SubjectTitle");
		Previous = extras.getString("Previous");
		SampleID = extras.getString("SampleID");

		// 設定題號和題目標題
		/*
		 * by Awei 2013/04/16
		 * 把題號跟題目合併在一起
		 */
		textSubjectIndexTitle.setText(SubjectIndex+" : "+SubjectTitle);

		// 如果是第一題的話隱藏「上一題」按鈕
		if (Previous.equals("null")) {
			btnPrevious.setEnabled(false);
//			btnPrevious.setVisibility(View.INVISIBLE);
		}

		LoadData();

		btnPrevious.setOnTouchListener(this);
		btnNext.setOnTouchListener(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		ImageButton ib = (ImageButton) v;
		switch (ib.getId()) {
		case R.id.btnPrevious:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.previousbtn_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.previousbtn);
			}
			break;
		case R.id.btnNext:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.nextbutton_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.nextbutton);
			}
			break;
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		System.out.println("onclick");
		for (int i = 0; i < radio.length; i++) {
			radio[i].setChecked(false);
			radio[i].setButtonDrawable(R.drawable.radiobutton);
			radio[i].setButtonDrawable(R.drawable.radiobuttont);
		}
		RadioButton rb = (RadioButton) v;
		radio[rb.getId()].setChecked(true);
//		radio[rb.getId()].setButtonDrawable(R.drawable.radiobutton_press);
		temp = rb.getId();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				|| keyCode == KeyEvent.KEYCODE_HOME) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * 是否為數字，數字為true，數字為false
	 * 
	 * @param str
	 * @return
	 */
	private boolean StrToInt(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	final Handler cwjHandler = new Handler();

	final Runnable mUpdateResults = new Runnable() {
		public void run() {
			listdata = OptionService.queryOption(SurveyGuid, SubjectIndex,
					FourView.this);

			// 設定「選項」標題
			for (int i = 0; i < listdata.size(); i++) {
				textview[i].setText(listdata.get(i).get("optionText"));
				index.put(i, listdata.get(i).get("optionValue"));
			}

			ArrayList<HashMap<String, String>> answerdata = AnswerService
					.queryAnswer(SurveyGuid, SubjectIndex, SampleID,
							FourView.this);

			// 如果有填寫過的話，顯示最後一次填寫的答案
			if (answerdata != null && answerdata.size() != 0
					&& answerdata.get(0).get("Answer") != null
					&& StrToInt(answerdata.get(0).get("Answer"))) {
				int rIndex = Integer.parseInt(answerdata.get(0).get("Answer")) - 1;
				if (rIndex < 0 || rIndex > radio.length) {
					rIndex = 0;
				}
				radio[rIndex].setChecked(true);
//				radio[rIndex].setButtonDrawable(R.drawable.radiobutton_press);
				temp = rIndex;
			}

			ArrayList<HashMap<String, String>> tipdata = TipService
					.queryAllTip(SurveyGuid, SubjectIndex, FourView.this);

			// 是否有提示卡，有的話顯示提示卡
			if (tipdata.size() == 0) {
				btnPicture.setVisibility(View.GONE);
				textTips.setVisibility(View.GONE);
			} else {
				if (tipdata.get(0).get("tip_image") != null
						&& !tipdata.get(0).get("tip_image").trim().equals("")) {
					textTips.setVisibility(View.GONE);
				} else {
					btnPicture.setVisibility(View.GONE);
					if (tipdata.get(0).get("tip_text") == null
							|| tipdata.get(0).get("tip_text").trim().equals("")) {
						textTips.setVisibility(View.GONE);
					} else {
						textTips.setText(tipdata.get(0).get("tip_text"));
					}
				}
			}
		}
	};

	/**
	 * 載入資料
	 */
	private void LoadData() {
//		progressDialog = ProgressDialog.show(FourView.this, "載入中....",
//				"資料處理中。。。", true);
//
//		new Thread() {
//			public void run() {
//				progressDialog.dismiss();

				cwjHandler.post(mUpdateResults);
//			}
//		}.start();
	}

	/**
	 * 上一題
	 * 
	 * @param v
	 */
	public void previous(View v) {
		Intent intent = new Intent();
		intent.putExtra("SurveyGuid", SurveyGuid);
		intent.putExtra("SubjectIndex", Previous);

		// 查詢「上一題」的「題號]
		SubjectIndex = AnswerService.getSubjectIndex(Previous, SampleID,
				FourView.this);

		intent.putExtra("Previous", SubjectIndex);

		this.setResult(RESULT_OK, intent);

		finish();
		//停止過場動畫
		overridePendingTransition(0, 0);
	}

	/**
	 * 提示卡圖片
	 * 
	 * @param v
	 */
	public void picture(View v) {
		Intent intent = new Intent();
		intent.setClass(FourView.this, TipImageView.class);
		FourView.this.startActivity(intent);
	}

	/**
	 * 取得代碼
	 */
	private void getserial() {
		// 查詢目前題目的代碼
		serial = SubjectService.getSerial(SurveyGuid, SubjectIndex,
				FourView.this);
	}

	/**
	 * 取得題號
	 */
	private void getSubjectIndex(String TableName) {
		// 取得下一題的題號
		if (TableName.equals("Subject")) {
			String Serial = Integer.parseInt(serial) + 1 + "";

			SubjectIndex = SubjectService.getSubjectIndex(SurveyGuid, Serial,
					FourView.this);
		} else if (TableName.equals("SinglePass")) {
			SubjectIndex = SinglePsaaService.getSubjectIndex(SurveyGuid,
					SubjectIndex, FourView.this);
		} else if (TableName.equals("GroupPass")) {
			SubjectIndex = GroupPsaaService.getSubjectIndex(SurveyGuid,
					SubjectIndex, FourView.this);
		}
	}

	/**
	 * 取得題號,給單一跳題，且每個選項有獨立的跳題邏輯用的
	 */
	private void getSubjectIndexByAnswer(String TableName, String nAnswer) {
		if("".equals(nAnswer)){
			this.getSubjectIndex(TableName);
		}else {
			SubjectIndex = SinglePsaaService.getSubjectIndexByAnswer(SurveyGuid,
					SubjectIndex, nAnswer, FourView.this);
			System.out.println("SubjectIndex:"+SubjectIndex);
		}
	}

	/**
	 * 下一題
	 * 
	 * @param v
	 */
	public void next(View v) {
		btnNext.setEnabled(false);
		// 檢查是否有正確作答
		if (temp == -1) {
			Toast.makeText(FourView.this, "請做答", Toast.LENGTH_LONG).show();
			btnNext.setEnabled(true);
			return;
		} else {
			// 刪除上次作答的答案
			AnswerService.deleteAnswer(SurveyGuid, SubjectIndex, SampleID,
					FourView.this);

			// 儲存本次作答的答案
			AnswerDto answerDto = new AnswerDto();
			answerDto.setSurveyGuid(SurveyGuid);
			answerDto.setInterviewerID(InterviewService
					.getInterviewGuid(FourView.this));
			answerDto.setSampleID(SampleID);
			answerDto.setPrevious(Previous);
			answerDto.setSubjectIndex(SubjectIndex);
			answerDto.setAnswer(String.valueOf(temp + 1));

			AnswerService.cmdInsert(answerDto, FourView.this);

			Previous = SubjectIndex;

			ArrayList<HashMap<String, String>> SinglePsaa = SinglePsaaService
					.queryAllSinglePsaa(SurveyGuid, SubjectIndex, FourView.this);

			ArrayList<HashMap<String, String>> GroupPsaa = GroupPsaaService
					.queryAllGroupPsaa(SurveyGuid, SubjectIndex, FourView.this);

			boolean isGP = true;
			boolean isSP = false;

			// 檢查是否有符合「群組跳題」的規則
			if (GroupPsaa.size() != 0) {

				for (int i = 0; i < GroupPsaa.size(); i++) {
					String subjectList = GroupPsaa.get(i).get("subjectList");
					String answerList = GroupPsaa.get(i).get("answerList");

					String answer = AnswerService.getAnswer(SurveyGuid,
							subjectList, SampleID, FourView.this);

					if (answer == null) {
						break;
					}

					if (!answer.equals(answerList)) {
						isGP = false;
					}
				}

				if (isGP) {
					getSubjectIndex("GroupPass");
					
					/*
					 * Add By Awei 2013/05/23
					 * 如果有跳題，就刪除目前題目之後到跳題題目之前，這中間的所有答案。
					 * */
					String nowSubjectSerial = SubjectService.getSerial(SurveyGuid, Previous, FourView.this);
					String passSubjectSerial = SubjectService.getSerial(SurveyGuid, SubjectIndex, FourView.this);
					DeleteAnsweredService.deleteAnswerAnsweredForSingleAndGroupPass(SurveyGuid, SampleID, nowSubjectSerial
							, passSubjectSerial, FourView.this);
				} 
			} else {
				isGP = false;
			}
			
			// 檢查是否有符合「單題跳題」的規則
			if (SinglePsaa.size() != 0 && isGP == false) {
				for (int i = 0; i < SinglePsaa.size(); i++) {
					int SingleAnswer = 0;
					try {
						SingleAnswer = Integer.parseInt(SinglePsaa.get(i).get(
								"answer"));
					} catch (NumberFormatException e) {
						getserial();
						getSubjectIndex("Subject");
					}

					String Answer = AnswerService.getAnswer(SurveyGuid,
							SubjectIndex, SampleID, FourView.this);

					String isselect = SinglePsaa.get(i).get("isselect");

					if (isselect.equals("1")) {
						if (StrToInt(Answer)) {
							if (SingleAnswer == Integer.parseInt(Answer)) {
								// sum++;
								isSP = true;
							}
						}
					} else {
						if (StrToInt(Answer)) {
							if (SingleAnswer != Integer.parseInt(Answer)) {
								// sum++;
								isSP = true;
							}
						}
					}
				}

				if (isSP) {
//					getSubjectIndex("SinglePass");
					
					/*
					 * by Awei 2013/05/23 (後來才加的)
					 * 給單一跳題，且每個選項有獨立的跳題邏輯用的
					 * 改呼叫getSubjectIndexByAnswer，多丟一個目前的答案進去，從DB撈正確的跳題題號給View
					 */
					String Answer = AnswerService.getAnswer(SurveyGuid,
							SubjectIndex, SampleID, FourView.this);
					if (listdata.get(temp).get("optionIsShowTextView") != null
							&& Integer.parseInt(listdata.get(temp).get(
									"optionIsShowTextView")) == 1) {
						Answer = "88";
					}
					getSubjectIndexByAnswer("SinglePass",Answer);
					
					/*
					 * Add By Awei 2013/05/23
					 * 如果有跳題，就刪除目前題目之後到跳題題目之前，這中間的所有答案。
					 * */
					String nowSubjectSerial = SubjectService.getSerial(SurveyGuid, Previous, FourView.this);
					String passSubjectSerial = SubjectService.getSerial(SurveyGuid, SubjectIndex, FourView.this);
					DeleteAnsweredService.deleteAnswerAnsweredForSingleAndGroupPass(SurveyGuid, SampleID, nowSubjectSerial
							, passSubjectSerial, FourView.this);

				}
			}
			
			// 不符單題跳題和群組跳題，取的下一題
			if (isSP == false && isGP == false) {
				getserial();
				getSubjectIndex("Subject");
			}

			Intent intent = new Intent();
			intent.putExtra("SurveyGuid", SurveyGuid);
			intent.putExtra("SubjectIndex", SubjectIndex);
			intent.putExtra("Previous", Previous);

			this.setResult(RESULT_OK, intent);

			finish();
			//停止過場動畫
			overridePendingTransition(0, 0);
		}
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			this.setResult(RESULT_CANCELED);
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, R.string.Endanswer);
		return super.onCreateOptionsMenu(menu);
	}
}
