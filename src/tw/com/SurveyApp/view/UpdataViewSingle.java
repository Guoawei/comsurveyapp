package tw.com.SurveyApp.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tw.com.SurveyApp.R;
import tw.com.SurveyApp.data.JsonConnection;
import tw.com.SurveyApp.service.AnswerService;
import tw.com.SurveyApp.service.GroupOptionService;
import tw.com.SurveyApp.service.InterviewService;
import tw.com.SurveyApp.service.LogService;
import tw.com.SurveyApp.service.OptionService;
import tw.com.SurveyApp.service.SampleService;
import tw.com.SurveyApp.service.StatusService;
import tw.com.SurveyApp.service.SubjectService;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class UpdataViewSingle extends Activity {
	String SurveyGuid = "", SampleID = "";
	Context context = null;
	Button gobackbtn = null;
	TextView updataMassage = null;
	int index = 0;
	String Name = "", AnswerText = "", LogText = "";
	StringBuilder StatusText = new StringBuilder();
	Thread mThread = null;
	
	ProgressBar myProgressBar;

	private ProgressDialog progressDialog = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.updataviewsingle);

		Bundle extras = getIntent().getExtras();
		SurveyGuid = extras.getString("SurveyGuid");
		SampleID = extras.getString("SampleID");

		context = UpdataViewSingle.this;

		gobackbtn = (Button) findViewById(R.id.gobackbtn);
		gobackbtn.setEnabled(false);

		updataMassage = (TextView) findViewById(R.id.updataMassage);
		updataMassage.setMovementMethod(ScrollingMovementMethod.getInstance());
		
		myProgressBar = (ProgressBar) findViewById(R.id.progressbar);

		myProgressBar.setMax(1);
		myProgressBar.setProgress(0);

		progressDialog = ProgressDialog.show(UpdataViewSingle.this, "載入中....",
				"資料處理中。。。", true);

		mThread = new Thread(new Runnable() {
			public void run() {
				try {
					// Thread.sleep(500);

					Message msg = new Message();
					msg.what = 1;

					// 取得樣本姓名
					Name = new SampleService().getName(SampleID, context);
					// 取得 訪員 Guid
					String InterviewGuid = InterviewService.getInterviewGuid(context);

					Answer(SurveyGuid, InterviewGuid, SampleID, context);
					Log(SurveyGuid, InterviewGuid, SampleID, context);
					Status(SurveyGuid, InterviewGuid, SampleID, context);

					mHandler.sendMessage(msg);

					// Thread.sleep(500);

				} catch (Throwable t) {
					System.out.println("t = " + t);
				}
				
				Message msg = new Message();
				msg.what = 2;
				mHandler.sendMessage(msg);
				
				progressDialog.dismiss();
			}
		});

		mThread.start();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				|| keyCode == KeyEvent.KEYCODE_HOME) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:
				StringBuilder text = new StringBuilder();
				text.append(Name + " " + AnswerText + "\n");

				text.append(updataMassage.getText().toString());
				updataMassage.setText(text);
				break;
			case 2:
				gobackbtn.setEnabled(true);
				myProgressBar.setProgress(myProgressBar.getMax());
				Thread.currentThread().interrupt();
				break;
			}
		}
	};

	/**
	 * 回上一頁
	 * 
	 * @param v
	 */
	public void btngoback(View v) {
		mThread.stop();
		mHandler.removeCallbacks(mThread);
		finish();
		//停止過場動畫
		overridePendingTransition(0, 0);
	}

	/**
	 * 上傳操作狀態
	 * 
	 * @param SurveyGuid
	 * @param SampleID
	 * @param context
	 */
	private void Status(String SurveyGuid, String InterviewerGuid,
			String SampleID, Context context) {
		ArrayList<HashMap<String, String>> statuslist = StatusService
				.queryStatus(SurveyGuid, SampleID, context);

		if (statuslist == null || statuslist.size() == 0) {
			return;
		}

		JSONArray SituationList = new JSONArray();

		for (int i = 0; i < statuslist.size(); i++) {
			try {
				JSONObject Situation = new JSONObject();

				Situation.put("SituationCode",
						statuslist.get(i).get("SituationCode"));
				Situation.put("CDateTime", statuslist.get(i).get("CDateTime"));
				SituationList.put(i, Situation);

				String Comment = statuslist.get(i).get("Comment") == null ? ""
						: statuslist.get(i).get("Comment");

				Situation.put("Comment", java.net.URLEncoder.encode(Comment));
			} catch (JSONException e) {
				System.out.println(e);
			}
		}

		JSONObject Status = new JSONObject();

		try {
			Status.put("SurveyGuid", SurveyGuid);
			Status.put("InterviewerGuid", InterviewerGuid);
			Status.put("SampleGuid", SampleID);
			Status.put("SituationList", SituationList);
		} catch (JSONException e) {
			System.out.println(e);
		}

		HttpResponse re = null;
		String temp = null;
		try {
			JsonConnection jc = new JsonConnection();
			re = jc.doPost(JsonConnection.connection + "uploadSurveySituation",
					Status.toString());
			temp = EntityUtils.toString(re.getEntity());
		} catch (ClientProtocolException e) {
			System.out.println(e);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e);
			e.printStackTrace();
		}

		try {
			String ter[] = temp.split(":");

			if (ter[1].equals("Success")) {
				StatusText.append("操作狀態上傳成功 ");
				String ss = String.valueOf(statuslist.size());
				StatusText.append(ss);
				StatusText.append(" 筆");
			} else if (ter[1].equals("Failure")) {
				StatusText.append("操作狀態上傳失敗 ");
				StatusText.append(statuslist.size());
				StatusText.append(" 筆");
			} else if (ter[1].equals("Uploaded")) {
				StatusText.append("操作狀態上傳成功 ");
				StatusText.append(statuslist.size());
				StatusText.append(" 筆");
			} else {
				StatusText.append("操作狀態上傳失敗 ");
			}
		} catch (Exception e) {
			StatusText.append("操作狀態上傳失敗 ");
			System.out.println(e);
		}
	}

	/**
	 * 上傳操作記錄
	 * 
	 * @param SurveyGuid
	 * @param SampleID
	 * @param context
	 */
	private void Log(String SurveyGuid, String InterviewerGuid,
			String SampleID, Context context) {
		ArrayList<HashMap<String, String>> loglist = LogService.queryLog(
				SurveyGuid, SampleID, context);

		if (loglist == null || loglist.size() == 0) {
			return;
		}

		JSONArray LogDateTime = new JSONArray();

		for (int i = 0; i < loglist.size(); i++) {
			try {
				JSONObject LogDate = new JSONObject();

				LogDate.put("StartDateTime", loglist.get(i).get("Start"));
				LogDate.put("EndDateTime", loglist.get(i).get("End"));
				LogDate.put("Sindex", loglist.get(i).get("Sindex"));

				LogDateTime.put(i, LogDate);
			} catch (JSONException e) {
				System.out.println(e);
			}
		}

		JSONObject SampleLog = new JSONObject();

		try {
			SampleLog.put("SurveyGuid", SurveyGuid);
			SampleLog.put("InterviewerGuid", InterviewerGuid);
			SampleLog.put("SampleGuid", SampleID);
			SampleLog.put("LogDateTime", LogDateTime);
		} catch (JSONException e) {
			System.out.println(e);
		}

		HttpResponse re = null;
		String temp = null;
		try {
			JsonConnection jc = new JsonConnection();
			re = jc.doPost(JsonConnection.connection + "uploadSurveyTime",
					SampleLog.toString());
			temp = EntityUtils.toString(re.getEntity());
		} catch (ClientProtocolException e) {
			System.out.println(e);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e);
			e.printStackTrace();
		}

		try {
			String ter[] = temp.split(":");

			if (ter[1].equals("Success")) {
				LogText = "操作記錄上傳成功 " + loglist.size() + " 筆";
			} else if (ter[1].equals("Failure")) {
				LogText = "操作記錄上傳失敗 " + loglist.size() + " 筆";
			} else if (ter[1].equals("Uploaded")) {
				LogText = "操作記錄上傳成功 " + loglist.size() + " 筆";
			} else {
				LogText = "操作記錄上傳失敗 ";
			}
		} catch (Exception e) {
			LogText = "操作記錄上傳失敗";
			System.out.println(e);
		}
	}

	/**
	 * 是否為數字，數字為true，數字為false
	 * 
	 * @param str
	 * @return
	 */
	private boolean StrToInt(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 上傳答案
	 * 
	 * @param SurveyGuid
	 * @param SampleID
	 * @param context
	 */
	private void Answer(String SurveyGuid, String InterviewGuid,
			String SampleID, Context context) {

		ArrayList<HashMap<String, String>> answerlist = AnswerService
				.queryAnswer(SurveyGuid, SampleID, context);

		if (answerlist == null || answerlist.size() == 0) {
			AnswerText = "無答案記錄資料";
			return;
		}

		ArrayList<HashMap<String, String>> subjectlist = SubjectService
				.queryAllSubject(SurveyGuid, context);

		JSONArray InputAnswer = new JSONArray();
		JSONArray SubjectNumber = new JSONArray();

		for (int x = 0; x < subjectlist.size(); x++) {
			String subjectIndex = subjectlist.get(x).get("SubjectIndex");
			String SubjectType = subjectlist.get(x).get("SubjectType");

			StringBuilder answer = new StringBuilder();

			if (SubjectType.equals("M") || SubjectType.equals("G")
					|| SubjectType.equals("P")) {

				String GroupIndex = "";
				String GroupOption = "";

				for (int y = 0; y < answerlist.size(); y++) {
					if (answerlist.get(y).get("SubjectIndex") == null) {
						continue;
					}
					if (answerlist.get(y).get("SubjectIndex")
							.equals(subjectIndex)) {

						if (StrToInt(answerlist.get(y).get("Answer"))) {

							String ans = answerlist.get(y).get("Answer");

							if (SubjectType.equals("G")) {
								GroupOption = subjectlist.get(x).get(
										"GroupOption");
								GroupIndex = new GroupOptionService()
										.queryGroupIndex(SurveyGuid,
												GroupOption, ans, context);
							} else if (SubjectType.equals("M")) {
								GroupIndex = new OptionService()
										.queryOptionIndex(SurveyGuid,
												subjectIndex, ans, context);
							} else if (SubjectType.equals("P")) {
								GroupIndex = String.valueOf(y + 1);
							}

							answer.append(GroupIndex + "$" + ans).append(",");
						} else {
							if (answerlist.get(y).get("Answer") != null
									&& !answerlist.get(y).get("Answer").trim()
											.equals("")) {

								String ans[] = answerlist.get(y).get("Answer")
										.toString().split(":");

								if (SubjectType.equals("G")) {
									GroupOption = subjectlist.get(x).get(
											"GroupOption");
									GroupIndex = new GroupOptionService()
											.queryGroupIndex(SurveyGuid,
													GroupOption, ans[1],
													context);
								} else if (SubjectType.equals("M")) {
									GroupIndex = new OptionService()
											.queryOptionIndex(SurveyGuid,
													subjectIndex, ans[1],
													context);
								} else if (SubjectType.equals("P")) {
									GroupIndex = String.valueOf(y + 1);
								}

								answer.append(
										GroupIndex
												+ "$"
												+ java.net.URLEncoder
														.encode(answerlist.get(
																y)
																.get("Answer")))
										.append(",");
							}
						}
					}
				}
			} else {
				for (int y = 0; y < answerlist.size(); y++) {
					if (answerlist.get(y).get("SubjectIndex") == null) {
						continue;
					}
					if (answerlist.get(y).get("SubjectIndex")
							.equals(subjectIndex)) {
						if (StrToInt(answerlist.get(y).get("Answer"))) {
							answer.append(answerlist.get(y).get("Answer"))
									.append(",");
						} else {
							if (answerlist.get(y).get("Answer") != null
									&& !answerlist.get(y).get("Answer").trim()
											.equals("")) {
								answer.append(
										java.net.URLEncoder.encode(answerlist
												.get(y).get("Answer"))).append(
										",");
							}
						}
					}
				}
			}

			if (answer.length() > 0) {
				answer.delete(answer.length() - 1, answer.length());
			} else {
				answer = null;
			}

			try {
				SubjectNumber.put(x, subjectIndex);
				InputAnswer.put(x, answer);
			} catch (JSONException e) {
				System.out.println(e);
			}
		}

		JSONObject AnwerList = new JSONObject();

		JSONObject SurveyAnswer = new JSONObject();
		try {
			SurveyAnswer.put("SurveyGuid", SurveyGuid);
			SurveyAnswer.put("InterviewerGuid", InterviewGuid);
			SurveyAnswer.put("SampleGuid", SampleID);

			AnwerList.put("SubjectNumber", SubjectNumber);
			AnwerList.put("InputAnswer", InputAnswer);

			SurveyAnswer.put("AnswerList", AnwerList);
		} catch (JSONException e) {
			System.out.println(e);
			e.printStackTrace();
		}

		HttpResponse re;
		String temp = null;
		try {
			JsonConnection jc = new JsonConnection();
			re = jc.doPost(JsonConnection.connection + "uploadSurveyAnswer",
					SurveyAnswer.toString());
			temp = EntityUtils.toString(re.getEntity());
		} catch (ClientProtocolException e) {
			System.out.println(e);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e);
			e.printStackTrace();
		}

		try {
			String ter[] = temp.split(":");

			if (ter[1].equals("Success")) {
				AnswerText = "答案記錄上傳成功 ";
			} else if (ter[1].equals("Failure")) {
				AnswerText = "答案記錄上傳失敗 ";
			} else if (ter[1].equals("Uploaded")) {
				AnswerText = "答案記錄上傳成功 ";
			} else {
				AnswerText = "答案記錄上傳失敗 ";
			}
		} catch (Exception e) {
			AnswerText = "答案記錄上傳失敗";
			System.out.println(e);
		}
	}

}
