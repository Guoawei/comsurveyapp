package tw.com.SurveyApp.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tw.com.SurveyApp.R;
import tw.com.SurveyApp.data.JsonConnection;
import tw.com.SurveyApp.dto.UpdataLogDto;
import tw.com.SurveyApp.service.AnswerService;
import tw.com.SurveyApp.service.GroupOptionService;
import tw.com.SurveyApp.service.InterviewService;
import tw.com.SurveyApp.service.LogService;
import tw.com.SurveyApp.service.OptionService;
import tw.com.SurveyApp.service.SampleService;
import tw.com.SurveyApp.service.StatusService;
import tw.com.SurveyApp.service.SubjectService;
import tw.com.SurveyApp.service.UpdataLogService;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class UpdataView extends Activity {

	ArrayList<HashMap<String, String>> samplelist = null;
	String SurveyGuid = "";
	ProgressBar myProgressBar;
	Context context = null;
	Button gobackbtn = null;
	TextView updataMassage = null;
	int index = 0, SampleLength = 0, UpdataLogLength = 0;
	int iindex = 0;
	String Name = "", AnswerText = "", StatusText = "";
	Thread mThread = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.updataview);

		Bundle extras = getIntent().getExtras();
		SurveyGuid = extras.getString("SurveyGuid");

		context = UpdataView.this;

		samplelist = SampleService.querySample(SurveyGuid, context);

		SampleLength = SampleService.getSampleLength(SurveyGuid,
				UpdataView.this);
		UpdataLogLength = UpdataLogService.getLength(SurveyGuid,
				UpdataView.this);

		myProgressBar = (ProgressBar) findViewById(R.id.progressbar);

//		myProgressBar.setMax((SampleLength - UpdataLogLength) * 3);
		myProgressBar.setMax(samplelist.size());
		myProgressBar.setProgress(0);

//		myProgressBar.setSecondaryProgress(0);

		gobackbtn = (Button) findViewById(R.id.gobackbtn);
		gobackbtn.setEnabled(false);

		updataMassage = (TextView) findViewById(R.id.updataMassage);
		updataMassage.setMovementMethod(ScrollingMovementMethod.getInstance());

		mThread = new Thread(new Runnable() {
			public void run() {
				try {
//					 Thread.sleep(500);

					for (int i = 0; i < samplelist.size(); i++) {
						
						// 取得樣本的id
						String SampleID = samplelist.get(i).get("SampleGuid");

						Name = "";
						AnswerText = "";

						/*
						 * 判斷是否已經全部上傳過了
						 */
						if ((SampleLength - UpdataLogLength) == 0) {
							Message noDataUploadMsg = new Message();
							index = SampleLength - UpdataLogLength;
							noDataUploadMsg.what = 3;

							mHandler.sendMessage(noDataUploadMsg);
							noDataUploadMsg = null;
							break;
						}

						/*
						 * 記錄是否上傳過，上傳過則不重新上傳
						 */
						if (UpdataLogService.cmdIsUpdate(SurveyGuid, SampleID,
								UpdataView.this)) {
							if(i == samplelist.size()-1){
								Message uploadDoneMsg = new Message();
								index = SampleLength - UpdataLogLength;
								uploadDoneMsg.what = 2;
								mHandler.sendMessage(uploadDoneMsg);
								uploadDoneMsg = null;
								break;
							}
							continue;
							
						} else {
							UpdataLogDto dto = new UpdataLogDto();
							dto.setSampleGuid(SampleID);
							dto.setSurveyGuid(SurveyGuid);
							UpdataLogService.cmdInsert(dto, UpdataView.this);
						}

						// 取得樣本姓名
						Name = new SampleService().getName(SampleID, context);

						// 取得 訪員 Guid
						String InterviewGuid = InterviewService
								.getInterviewGuid(context);

						StatusText = "上傳答案中";
						iindex = index * 3 + 1;
						Message answerMsg = new Message();
						answerMsg.what = 4;
						mHandler.sendMessage(answerMsg);
						Answer(SurveyGuid, InterviewGuid, SampleID, context);
						Log(SurveyGuid, InterviewGuid, SampleID, context);
						Status(SurveyGuid, InterviewGuid, SampleID, context);
						answerMsg = null;
						
						Thread.sleep(200);
						
						index++;

						Message msg = new Message();
						msg.what = 1;
						mHandler.sendMessage(msg);
						msg = null;
						
						Thread.sleep(500);

						if (index == samplelist.size()){
							Message updataDoneMsg = new Message();
							updataDoneMsg.what = 2;
							mHandler.sendMessage(updataDoneMsg);
							updataDoneMsg = null;

						}

//						Thread.sleep(500);
					}
				} catch (Throwable t) {
					System.out.println("t = " + t);
				}
			}
		});

		mThread.start();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				|| keyCode == KeyEvent.KEYCODE_HOME) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {

			String message = "";

			String text = updataMassage.getText().toString();

			switch (msg.what) {
			case 1:
				myProgressBar.setProgress(index);

				if (!Name.equals("") && !AnswerText.equals("")) {
					String sampleUploadingText = Name + " " + StatusText;
					String sampleUploadDoneText = sampleUploadingText + " " + "[" + AnswerText + "]";
					String newText = text.replace(sampleUploadingText, sampleUploadDoneText);
					text = newText;
				}

				updataMassage.setText(text);

				break;
			case 2:
				message = "調查結果上傳完成，請按返回鍵至問卷列表。 ";
				gobackbtn.setEnabled(true);
				myProgressBar.setProgress(myProgressBar.getMax());
				Thread.currentThread().interrupt();

				text = message + "\n" + text;

				updataMassage.setText(text);

				break;
			case 3:
				message = "無調查結果需要上傳，請按返回鍵至問卷列表。";
				gobackbtn.setEnabled(true);
				myProgressBar.setProgress(myProgressBar.getMax());
				Thread.currentThread().interrupt();

				if (!Name.equals("") && !AnswerText.equals("")) {
					text = Name + " " + AnswerText + "\n" + text;
				}

				text = message + "\n \n" + text;

				updataMassage.setText(text);

				mHandler.removeCallbacks(mThread);
				break;
			case 4:
				text = Name + " " + StatusText + "\n" + text;
				
				updataMassage.setText(text);
				break;
			}
		}
	};

	public void btngoback(View v) {
		mHandler.removeCallbacks(mThread);
		mThread = null;
		mHandler = null;
		System.gc();
		finish();
		//停止過場動畫
		overridePendingTransition(0, 0);
	}

	/**
	 * 上傳操作狀態
	 * 
	 * @param SurveyGuid
	 * @param SampleID
	 * @param context
	 */
	private void Status(String SurveyGuid, String InterviewerGuid,
			String SampleID, Context context) {
		ArrayList<HashMap<String, String>> statuslist = StatusService
				.queryStatus(SurveyGuid, SampleID, context);

		if (statuslist == null || statuslist.size() == 0) {
			return;
		}

		JSONArray SituationList = new JSONArray();

		for (int i = 0; i < statuslist.size(); i++) {
			try {
				JSONObject Situation = new JSONObject();

				String SituationCode = statuslist.get(i).get("SituationCode") == null ? ""
						: statuslist.get(i).get("SituationCode");
				Situation.put("SituationCode", SituationCode);

				String CDateTime = statuslist.get(i).get("CDateTime") == null ? ""
						: statuslist.get(i).get("CDateTime");
				Situation.put("CDateTime", CDateTime);

				SituationList.put(i, Situation);

				String Comment = statuslist.get(i).get("Comment") == null ? ""
						: statuslist.get(i).get("Comment");

				Situation.put("Comment", java.net.URLEncoder.encode(Comment));
			} catch (JSONException e) {
				System.out.println(e);
			}
		}

		JSONObject Status = new JSONObject();

		try {
			Status.put("SurveyGuid", SurveyGuid);
			Status.put("InterviewerGuid", InterviewerGuid);
			Status.put("SampleGuid", SampleID);
			Status.put("SituationList", SituationList);
		} catch (JSONException e) {
			System.out.println(e);
		}

		HttpResponse re = null;
		String temp = null;
		try {
			JsonConnection jc = new JsonConnection();
			re = jc.doPost(JsonConnection.connection + "uploadSurveySituation",
					Status.toString());
			temp = EntityUtils.toString(re.getEntity());
		} catch (ClientProtocolException e) {
			System.out.println(e);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e);
			e.printStackTrace();
		}

//		temp = ":Success";

		try {
			String ter[] = temp.split(":");

			if (ter[1].equals("Success")) {
				if (AnswerText.equals("")) {
					AnswerText = "上傳成功 ";
				}
			} else if (ter[1].equals("Failure")) {
				if (AnswerText.equals("")) {
					AnswerText = "上傳失敗 ";
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * 上傳操作記錄
	 * 
	 * @param SurveyGuid
	 * @param SampleID
	 * @param context
	 */
	private void Log(String SurveyGuid, String InterviewerGuid,
			String SampleID, Context context) {
		ArrayList<HashMap<String, String>> loglist = LogService.queryLog(
				SurveyGuid, SampleID, context);

		if (loglist == null || loglist.size() == 0) {
			return;
		}

		JSONArray LogDateTime = new JSONArray();

		for (int i = 0; i < loglist.size(); i++) {
			try {
				JSONObject LogDate = new JSONObject();

				String Start = loglist.get(i).get("Start") == null ? ""
						: loglist.get(i).get("Start");
				LogDate.put("StartDateTime", Start);

				String End = loglist.get(i).get("End") == null ? "" : loglist
						.get(i).get("End");
				LogDate.put("EndDateTime", End);

				String Sindex = loglist.get(i).get("Sindex") == null ? ""
						: loglist.get(i).get("Sindex");
				LogDate.put("Sindex", Sindex);

				LogDateTime.put(i, LogDate);
			} catch (JSONException e) {
				System.out.println(e);
			}
		}

		JSONObject SampleLog = new JSONObject();

		try {
			SampleLog.put("SurveyGuid", SurveyGuid);
			SampleLog.put("InterviewerGuid", InterviewerGuid);
			SampleLog.put("SampleGuid", SampleID);
			SampleLog.put("LogDateTime", LogDateTime);
		} catch (JSONException e) {
			System.out.println(e);
		}

		try {
			JsonConnection jc = new JsonConnection();
			jc.doPost(JsonConnection.connection + "uploadSurveyTime",
					SampleLog.toString());
		} catch (ClientProtocolException e) {
			System.out.println(e);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * 是否為數字，數字為true，數字為false
	 * 
	 * @param str
	 * @return
	 */
	private boolean StrToInt(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 上傳答案
	 * 
	 * @param SurveyGuid
	 * @param SampleID
	 * @param context
	 */
	private void Answer(String SurveyGuid, String InterviewGuid,
			String SampleID, Context context) {

		ArrayList<HashMap<String, String>> answerlist = AnswerService
				.queryAnswer(SurveyGuid, SampleID, context);

		if (answerlist == null || answerlist.size() == 0) {
			AnswerText = "沒有答案，不上傳";			
			return;
		}

		ArrayList<HashMap<String, String>> subjectlist = SubjectService
				.queryAllSubject(SurveyGuid, context);

		JSONArray InputAnswer = new JSONArray();
		JSONArray SubjectNumber = new JSONArray();

		for (int x = 0; x < subjectlist.size(); x++) {
			String subjectIndex = subjectlist.get(x).get("SubjectIndex") == null ? ""
					: subjectlist.get(x).get("SubjectIndex");

			String SubjectType = subjectlist.get(x).get("SubjectType") == null ? ""
					: subjectlist.get(x).get("SubjectType");

			StringBuilder answer = new StringBuilder();

			if (SubjectType.equals("M") || SubjectType.equals("G")
					|| SubjectType.equals("P")) {

				String GroupIndex = "";
				String GroupOption = "";

				for (int y = 0; y < answerlist.size(); y++) {
					if (answerlist.get(y).get("SubjectIndex") == null) {
						continue;
					}
					if (answerlist.get(y).get("SubjectIndex")
							.equals(subjectIndex)) {

						if (StrToInt(answerlist.get(y).get("Answer"))) {

							String ans = answerlist.get(y).get("Answer");

							if (SubjectType.equals("G")) {
								GroupOption = subjectlist.get(x).get(
										"GroupOption");
								GroupIndex = new GroupOptionService()
										.queryGroupIndex(SurveyGuid,
												GroupOption, ans, context);
							} else if (SubjectType.equals("M")) {
								GroupIndex = new OptionService()
										.queryOptionIndex(SurveyGuid,
												subjectIndex, ans, context);
							} else if (SubjectType.equals("P")) {
								GroupIndex = String.valueOf(y + 1);
							}

							answer.append(GroupIndex + "$" + ans).append(",");
						} else {
							if (answerlist.get(y).get("Answer") != null
									&& !answerlist.get(y).get("Answer").trim()
											.equals("")) {

								String ans[] = answerlist.get(y).get("Answer")
										.toString().split(":");

								if (SubjectType.equals("G")) {
									GroupOption = subjectlist.get(x).get(
											"GroupOption");
									GroupIndex = new GroupOptionService()
											.queryGroupIndex(SurveyGuid,
													GroupOption, ans[1],
													context);
								} else if (SubjectType.equals("M")) {
									GroupIndex = new OptionService()
											.queryOptionIndex(SurveyGuid,
													subjectIndex, ans[1],
													context);
								} else if (SubjectType.equals("P")) {
									GroupIndex = String.valueOf(y + 1);
								}

								answer.append(
										GroupIndex
												+ "$"
												+ java.net.URLEncoder
														.encode(answerlist.get(
																y)
																.get("Answer")))
										.append(",");
							}
						}
					}
				}
			} else {
				for (int y = 0; y < answerlist.size(); y++) {
					if (answerlist.get(y).get("SubjectIndex") == null) {
						continue;
					}
					if (answerlist.get(y).get("SubjectIndex")
							.equals(subjectIndex)) {
						if (StrToInt(answerlist.get(y).get("Answer"))) {
							answer.append(answerlist.get(y).get("Answer"))
									.append(",");
						} else {
							if (answerlist.get(y).get("Answer") != null
									&& !answerlist.get(y).get("Answer").trim()
											.equals("")) {
								answer.append(
										java.net.URLEncoder.encode(answerlist
												.get(y).get("Answer"))).append(
										",");
							}
						}
					}
				}
			}

			if (answer.length() > 0) {
				answer.delete(answer.length() - 1, answer.length());
			} else {
				answer = null;
			}

			try {
				SubjectNumber.put(x, subjectIndex);
				InputAnswer.put(x, answer);
			} catch (JSONException e) {
				System.out.println(e);
			}
		}

		JSONObject AnwerList = new JSONObject();

		JSONObject SurveyAnswer = new JSONObject();
		try {
			SurveyAnswer.put("SurveyGuid", SurveyGuid);
			SurveyAnswer.put("InterviewerGuid", InterviewGuid);
			SurveyAnswer.put("SampleGuid", SampleID);

			AnwerList.put("SubjectNumber", SubjectNumber);
			AnwerList.put("InputAnswer", InputAnswer);

			SurveyAnswer.put("AnswerList", AnwerList);
			System.out.println(SurveyAnswer);
		} catch (JSONException e) {
			System.out.println(e);
			e.printStackTrace();
		}

		HttpResponse re;
		String temp = null;
		try {
			JsonConnection jc = new JsonConnection();
			re = jc.doPost(JsonConnection.connection + "uploadSurveyAnswer",
					SurveyAnswer.toString());
			temp = EntityUtils.toString(re.getEntity());
		} catch (ClientProtocolException e) {
			System.out.println(e);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e);
			e.printStackTrace();
		}

//		 temp = ":Success";
		System.out.println(temp);
		try {
			String ter[] = temp.split(":");

			if (ter[1].equals("Success")) {
				// AnswerText = "答案記錄上傳成功 " + answerlist.size() + " 筆";
				AnswerText = "上傳成功 ";
				// Toast.makeText(context, ter[0] + " 答案上傳成功", 1).show();
			} else if (ter[1].equals("Failure")) {
				// AnswerText = "答案記錄上傳失敗 " + answerlist.size() + " 筆";
				AnswerText = "上傳失敗 ";
				// Toast.makeText(context, ter[0] + " 答案上傳失敗", 1).show();
			} else if (ter[1].equals("Uploaded")) {
				AnswerText = "上傳成功 ";
				// Toast.makeText(context, ter[0] + " 已經上傳過了", 1).show();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
