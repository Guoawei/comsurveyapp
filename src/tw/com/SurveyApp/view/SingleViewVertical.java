package tw.com.SurveyApp.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import tw.com.SurveyApp.R;
import tw.com.SurveyApp.dto.AnswerDto;
import tw.com.SurveyApp.service.AnswerService;
import tw.com.SurveyApp.service.DeleteAnsweredService;
import tw.com.SurveyApp.service.DynamicOptionService;
import tw.com.SurveyApp.service.GroupPsaaService;
import tw.com.SurveyApp.service.InterviewService;
import tw.com.SurveyApp.service.OptionService;
import tw.com.SurveyApp.service.SampleService;
import tw.com.SurveyApp.service.SinglePsaaService;
import tw.com.SurveyApp.service.SubjectService;
import tw.com.SurveyApp.service.TipService;
import android.app.Activity;
//import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class SingleViewVertical extends Activity implements OnTouchListener {
	private ListView listview = null;
	private String SurveyGuid = "";
	private String SubjectIndex = "";
	private String SubjectTitle = "";
	private String Previous = "";
	private String serial = "";
	private String SampleID = "";
	private String Other = "";
	private ArrayList<HashMap<String, String>> listdata = null;
	private ImageButton btnPrevious, btnNext;
	private ImageButton btnPreviousPage, btnNextPage;
	private ImageButton btnPicture;
	private TextView textTips, textSubjectIndexTitle;
	private HashMap<Integer, Boolean> values = new HashMap<Integer, Boolean>();
	private HashMap<Integer, String> index = new HashMap<Integer, String>();
//	private ProgressDialog progressDialog = null;
	private int temp = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.subject);

		/*
		 * by Awei 2013/04/16
		 * 把題號跟題目合併在一起
		 */
		textSubjectIndexTitle = (TextView) findViewById(R.id.textSubjectIndexTitle);

		btnPrevious = (ImageButton) findViewById(R.id.btnPrevious);
		btnNext = (ImageButton) findViewById(R.id.btnNext);
		btnPreviousPage = (ImageButton) findViewById(R.id.btnPreviousPage);
		btnNextPage = (ImageButton) findViewById(R.id.btnNextPage);

		btnPrevious.setId(R.id.btnPrevious);
		btnNext.setId(R.id.btnNext);
		btnPreviousPage.setId(R.id.btnPreviousPage);
		btnNextPage.setId(R.id.btnNextPage);

		btnPicture = (ImageButton) findViewById(R.id.btnPicture);
		textTips = (TextView) findViewById(R.id.textTips);

		btnPreviousPage.setVisibility(View.GONE);
		btnNextPage.setVisibility(View.GONE);

		// 處理ListView的資料
		listview = (ListView) findViewById(R.id.listViewFormText);
		/*
		 * Add By Awei 
		 * 2013/05/26
		 * V 1.1.19
		 * 新增捲軸永久顯示
		 */
		listview.setScrollbarFadingEnabled(false);

		Bundle extras = getIntent().getExtras();
		SurveyGuid = extras.getString("SurveyGuid");
		SubjectIndex = extras.getString("SubjectIndex");
		SubjectTitle = extras.getString("SubjectTitle");
		Previous = extras.getString("Previous");
		SampleID = extras.getString("SampleID");

		textSubjectIndexTitle.setText(SubjectIndex+" : "+SubjectTitle);

		// 如果是第一題的話隱藏「上一題」按鈕
		if (Previous == null || Previous.equals("null")) {
			btnPrevious.setEnabled(false);
		}

		LoadData();

		btnPrevious.setOnTouchListener(this);
		btnNext.setOnTouchListener(this);
		btnPreviousPage.setOnTouchListener(this);
		btnNextPage.setOnTouchListener(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		ImageButton ib = (ImageButton) v;
		switch (ib.getId()) {
		case R.id.btnPrevious:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.previousbtn_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.previousbtn);
			}
			break;
		case R.id.btnNext:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.nextbutton_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.nextbutton);
			}
			break;
		case R.id.btnPreviousPage:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.previousbtn_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.previousbtn);
			}
			break;
		case R.id.btnNextPage:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.nextbutton_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.nextbutton);
			}
			break;
		}
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				|| keyCode == KeyEvent.KEYCODE_HOME) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	final Handler cwjHandler = new Handler();

	final Runnable mUpdateResults = new Runnable() {
		public void run() {
			listdata = SubjectService.queryAllSubject(SurveyGuid,
					SingleViewVertical.this);

			ArrayList<HashMap<String, String>> tipdata = TipService
					.queryAllTip(SurveyGuid, SubjectIndex,
							SingleViewVertical.this);

			// 是否有提示卡，有的話顯示提示卡
			if (tipdata.size() == 0) {
				btnPicture.setVisibility(View.GONE);
				textTips.setVisibility(View.GONE);
			} else {
				if (tipdata.get(0).get("tip_image") != null
						&& !tipdata.get(0).get("tip_image").trim().equals("")) {
					textTips.setVisibility(View.GONE);
				} else {
					btnPicture.setVisibility(View.GONE);
					if (tipdata.get(0).get("tip_text") == null
							|| tipdata.get(0).get("tip_text").trim().equals("")) {
						textTips.setVisibility(View.GONE);
					} else {
						textTips.setText(tipdata.get(0).get("tip_text"));
					}
				}
			}

			// 顯示ListView
			setSingleRow(SingleViewVertical.this, listview, SurveyGuid,
					SubjectIndex);
		}
	};

	/**
	 * 載入資料
	 */
	private void LoadData() {
//		progressDialog = ProgressDialog.show(SingleViewVertical.this,
//				"載入中....", "資料處理中。。。", true);
//
//		new Thread() {
//			public void run() {
//				progressDialog.dismiss();

				cwjHandler.post(mUpdateResults);
//			}
//		}.start();
	}

	/**
	 * 上一題
	 * 
	 * @param v
	 */
	public void previous(View v) {
		Intent intent = new Intent();
		intent.putExtra("SurveyGuid", SurveyGuid);
		intent.putExtra("SubjectIndex", Previous);

		// 查詢「上一題」的「題號]
		SubjectIndex = AnswerService.getPreviousString(Previous,
				SingleViewVertical.this);

		intent.putExtra("Previous", SubjectIndex);

		this.setResult(RESULT_OK, intent);

		finish();
		//停止過場動畫
		overridePendingTransition(0, 0);
	}

	/**
	 * 提示卡圖片
	 * 
	 * @param v
	 */
	public void picture(View v) {
		Intent intent = new Intent();
		intent.putExtra("SurveyGuid", SurveyGuid);
		intent.putExtra("SubjectIndex", SubjectIndex);
		intent.setClass(SingleViewVertical.this, TipImageView.class);
		SingleViewVertical.this.startActivity(intent);
	}

	/**
	 * 取得代碼
	 */
	private void getserial() {
		// 查詢目前題目的代碼
		serial = SubjectService.getSerial(SurveyGuid, SubjectIndex,
				SingleViewVertical.this);
	}

	/**
	 * 取得題號
	 */
	private void getSubjectIndex(String TableName) {
		// 取得下一題的題號
		if (TableName.equals("Subject")) {
			String Serial = Integer.parseInt(serial) + 1 + "";
			SubjectIndex = SubjectService.getSubjectIndex(SurveyGuid, Serial,
					SingleViewVertical.this);
		} else if (TableName.equals("SinglePass")) {
			SubjectIndex = SinglePsaaService.getSubjectIndex(SurveyGuid,
					SubjectIndex, SingleViewVertical.this);
		} else if (TableName.equals("GroupPsaa")) {
			SubjectIndex = GroupPsaaService.getSubjectIndex(SurveyGuid,
					SubjectIndex, SingleViewVertical.this);
		}
	}

	/**
	 * 取得題號,給單一跳題，且每個選項有獨立的跳題邏輯用的
	 */
	private void getSubjectIndexByAnswer(String TableName, String nAnswer) {
		if("".equals(nAnswer)){
			this.getSubjectIndex(TableName);
		}else {
			SubjectIndex = SinglePsaaService.getSubjectIndexByAnswer(SurveyGuid,
					SubjectIndex, nAnswer, SingleViewVertical.this);
			System.out.println("SubjectIndex:"+SubjectIndex);
		}
	}
	/**
	 * 下一題
	 * 
	 * @param v
	 */
	public void next(View v) {
		btnNext.setEnabled(false);
		// 檢查是否有正確作答
		boolean Accurate = false;
		Collection<Boolean> collection = values.values();
		Iterator<Boolean> iterator = collection.iterator();
		while (iterator.hasNext()) {
			if (iterator.next()) {
				// 如果有正確作答的話
				AnswerService.deleteAnswer(SurveyGuid, SubjectIndex, SampleID,
						SingleViewVertical.this);

				AnswerDto answerDto = new AnswerDto();
				answerDto.setSurveyGuid(SurveyGuid);
				answerDto.setInterviewerID(InterviewService
						.getInterviewGuid(SingleViewVertical.this));
				answerDto.setSampleID(SampleID);
				answerDto.setPrevious(Previous);
				answerDto.setSubjectIndex(SubjectIndex);

				if (listdata.get(temp).get("optionIsShowTextView") != null
						&& Integer.parseInt(listdata.get(temp).get(
								"optionIsShowTextView")) == 1) {
					answerDto.setAnswer("T:" + Other);
				} else {
					answerDto.setAnswer(index.get(temp) + "");
				}

				// 儲存本次作答的答案
				AnswerService.cmdInsert(answerDto, SingleViewVertical.this);

				Accurate = true;
				break;
			}
		}
		if (!Accurate) {
			Toast.makeText(SingleViewVertical.this, "請做答", Toast.LENGTH_LONG)
					.show();
			btnNext.setEnabled(true);
			return;
		}

		Previous = SubjectIndex;

		ArrayList<HashMap<String, String>> SinglePsaa = SinglePsaaService
				.queryAllSinglePsaa(SurveyGuid, SubjectIndex,
						SingleViewVertical.this);

		ArrayList<HashMap<String, String>> GroupPsaa = GroupPsaaService
				.queryAllGroupPsaa(SurveyGuid, SubjectIndex,
						SingleViewVertical.this);

		boolean isGP = true;
		boolean isSP = false;

		// 檢查是否有符合「群組跳題」的規則
		if (GroupPsaa.size() != 0) {

			for (int i = 0; i < GroupPsaa.size(); i++) {
				String subjectList = GroupPsaa.get(i).get("subjectList");
				String answerList = GroupPsaa.get(i).get("answerList");

				String answer = AnswerService.getAnswer(SurveyGuid,
						subjectList, SampleID, SingleViewVertical.this);

				if (answer == null) {
					break;
				}

				if (!answer.equals(answerList)) {
					isGP = false;
				}
			}

			if (isGP) {
				getSubjectIndex("GroupPsaa");
				
				/*
				 * Add By Awei 2013/05/23
				 * 如果有跳題，就刪除目前題目之後到跳題題目之前，這中間的所有答案。
				 * */
				String nowSubjectSerial = SubjectService.getSerial(SurveyGuid, Previous, SingleViewVertical.this);
				String passSubjectSerial = SubjectService.getSerial(SurveyGuid, SubjectIndex, SingleViewVertical.this);
				DeleteAnsweredService.deleteAnswerAnsweredForSingleAndGroupPass(SurveyGuid, SampleID, nowSubjectSerial
						, passSubjectSerial, SingleViewVertical.this);

			} else {
				getserial();
				getSubjectIndex("Subject");
			}
		} else {
			isGP = false;
		}

		// 檢查是否有符合「單題跳題」的規則
		if (SinglePsaa.size() != 0 && isGP == false) {
			for (int i = 0; i < SinglePsaa.size(); i++) {
				int SingleAnswer = Integer.parseInt(SinglePsaa.get(i).get(
						"answer"));

				String Answer = AnswerService.getAnswer(SurveyGuid,
						SubjectIndex, SampleID, SingleViewVertical.this);

				String isselect = SinglePsaa.get(i).get("isselect");

				if (isselect.equals("1")) {
					if (StrToInt(Answer)) {
						if (SingleAnswer == Integer.parseInt(Answer)) {
							// sum++;
							isSP = true;
						}
					}
				} else {
					if (StrToInt(Answer)) {
						if (SingleAnswer != Integer.parseInt(Answer)) {
							// sum++;
							isSP = true;
						}
					}
				}
			}

			if (isSP) {
				
				/*
				 * by Awei 2013/04/13
				 * 給單一跳題，且每個選項有獨立的跳題邏輯用的
				 * 改呼叫getSubjectIndexByAnswer，多丟一個目前的答案進去，從DB撈正確的跳題題號給View
				 */
				
//				getSubjectIndex("SinglePass");
				String Answer = AnswerService.getAnswer(SurveyGuid,
						SubjectIndex, SampleID, SingleViewVertical.this);
				if (listdata.get(temp).get("optionIsShowTextView") != null
						&& Integer.parseInt(listdata.get(temp).get(
								"optionIsShowTextView")) == 1) {
					Answer = "88";
				}
				getSubjectIndexByAnswer("SinglePass",Answer);
				
				/*
				 * Add By Awei 2013/05/23
				 * 如果有跳題，就刪除目前題目之後到跳題題目之前，這中間的所有答案。
				 * */
				String nowSubjectSerial = SubjectService.getSerial(SurveyGuid, Previous, SingleViewVertical.this);
				String passSubjectSerial = SubjectService.getSerial(SurveyGuid, SubjectIndex, SingleViewVertical.this);
				DeleteAnsweredService.deleteAnswerAnsweredForSingleAndGroupPass(SurveyGuid, SampleID, nowSubjectSerial
						, passSubjectSerial, SingleViewVertical.this);

			}
		}

		// 不符單題跳題和群組跳題，取的下一題
		if (isSP == false && isGP == false) {
			getserial();
			getSubjectIndex("Subject");
		}

		Intent intent = new Intent();
		intent.putExtra("SurveyGuid", SurveyGuid);
		intent.putExtra("SubjectIndex", SubjectIndex);
		intent.putExtra("Previous", Previous);

		this.setResult(RESULT_OK, intent);

		finish();
		//停止過場動畫
		overridePendingTransition(0, 0);
	}

	public void setSingleRow(Context context, ListView listview,
			String SurveyGuid, String SubjectIndex) {

		String Dyoptionguid = SubjectService.getDynamicOption(SurveyGuid,
				SubjectIndex, SingleViewVertical.this);

		listdata = new ArrayList<HashMap<String, String>>();

		if (Dyoptionguid == null || Dyoptionguid.equals("")
				|| Dyoptionguid.equals("null")) {

			listdata = OptionService.queryOption(SurveyGuid, SubjectIndex,
					SingleViewVertical.this);
		} else {
			String subject_accordingKey = DynamicOptionService
					.getDynamicOption("subject_accordingKey", Dyoptionguid,
							SingleViewVertical.this);

			if (subject_accordingKey.trim().equals("sampleZipCode")) {
				String ZipCode = SampleService.getZipCode(SampleID,
						SingleViewVertical.this);

				listdata = DynamicOptionService.queryAllDynamicOption(
						Dyoptionguid, ZipCode, SingleViewVertical.this);

			} else if (subject_accordingKey.trim().equals("beforeAnswer")) {
				String subject_subjectIndex = DynamicOptionService
						.getDynamicOption("subject_subjectIndex", Dyoptionguid,
								SingleViewVertical.this);

				String befanswer = AnswerService
						.getAnswer(SurveyGuid, subject_subjectIndex, SampleID,
								SingleViewVertical.this);

				listdata = DynamicOptionService.queryAllDynamicOption(
						Dyoptionguid, befanswer, SingleViewVertical.this);
			} else {
				listdata = DynamicOptionService.queryAllDynamicOption(
						Dyoptionguid, SingleViewVertical.this);
			}
		}

		String answer = AnswerService.getAnswer(SurveyGuid, SubjectIndex,
				SampleID, SingleViewVertical.this);

		if (listdata != null) {
			SingleAdapter adapter = new SingleAdapter(listdata, answer);
			listview.setAdapter(adapter);
		}
	}

	/**
	 * 是否為數字，數字為true，數字為false
	 * 
	 * @param str
	 * @return
	 */
	private boolean StrToInt(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public class SingleAdapter extends BaseAdapter implements
			OnFocusChangeListener, OnClickListener {
		private ArrayList<HashMap<String, String>> listData;
		private String Answer;

		RadioButton singleRadioButton;
		EditText singleAfeText;

		public SingleAdapter(ArrayList<HashMap<String, String>> listData,
				String Answer) {
			this.listData = listData;
			this.Answer = Answer;
		}

		public int getCount() {
			return listData.size();
		}

		public Object getItem(int position) {
			return listData.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		private void setView(int objectIndex, int value) {

			singleRadioButton.setId(value);
			singleRadioButton.setVisibility(View.VISIBLE);

			if (listData.get(value).get("optionIsShowTextView") != null
					&& Integer.parseInt(listData.get(value).get(
							"optionIsShowTextView")) == 1) {
				if (listData.get(value).get("optionTextViewPosition").trim()
						.equals("front")) {
				} else {
					singleAfeText.setVisibility(View.VISIBLE);
					singleAfeText.setId(value);
				}
			}

			if ((String) listData.get(value).get("optionText") == null) {
				singleRadioButton.setText((String) listData.get(value).get(
						"text"));

				index.put(value, (String) listData.get(value).get("value"));

				if (Answer != null && !Answer.equals("")) {
					if (StrToInt(Answer)) {
						if (Integer.parseInt(Answer) == Integer.parseInt(index
								.get(value))) {
							temp = value;
							values.put(temp, true);
							singleRadioButton.setChecked(true);

							singleRadioButton
									.setButtonDrawable(R.drawable.radiobutton_press);
						}
					} else {
						if (Answer != null && !Answer.equals("")) {
							if (listData.get(value).get("optionIsShowTextView") != null
									&& Integer.parseInt(listData.get(value)
											.get("optionIsShowTextView")) == 1) {
								String a = Answer.substring(2);

								Other = a;
								singleAfeText.setText(a);

								temp = value;
								values.put(temp, true);
								singleRadioButton.setChecked(true);
								singleRadioButton
										.setButtonDrawable(R.drawable.radiobutton_press);
							}
						}
					}
				}
			} else {
				singleRadioButton.setText((String) listData.get(value).get(
						"optionText"));

				index.put(value, (String) listData.get(value)
						.get("optionValue"));

				if (Answer != null && !Answer.equals("")) {
					if (StrToInt(Answer)) {
						if (Integer.parseInt(Answer) == Integer.parseInt(index
								.get(value))) {
							temp = value;
							values.put(temp, true);

							singleRadioButton.setChecked(true);
							singleRadioButton
									.setButtonDrawable(R.drawable.radiobutton_press);
						}
					} else {
						if (Answer != null && !Answer.equals("")) {
							if (listData.get(value).get("optionIsShowTextView") != null
									&& Integer.parseInt(listData.get(value)
											.get("optionIsShowTextView")) == 1) {
								String s[] = Answer.split(":");

								if (s.length >= 2) {
									String a = s[1];

									Other = a;
									singleAfeText.setText(a);
								}

								temp = value;
								values.put(temp, true);
								singleRadioButton.setChecked(true);
								singleRadioButton
										.setButtonDrawable(R.drawable.radiobutton_press);
							}
						}
					}
				}
			}
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			convertView = SingleViewVertical.this.getLayoutInflater().inflate(
					R.layout.singlerowvertical, null);

			singleRadioButton = (RadioButton) convertView
					.findViewById(R.id.singleRadioButton1);
			singleAfeText = (EditText) convertView
					.findViewById(R.id.singleAfetext1);

			setView(0, position);

			singleRadioButton.setOnClickListener(this);
			singleAfeText.setOnFocusChangeListener(this);
			singleAfeText.addTextChangedListener(textWatcher);

			if (temp == position) {
				singleRadioButton.setChecked(true);
				singleRadioButton
						.setButtonDrawable(R.drawable.radiobutton_press);
			}

			return convertView;
		}

		TextWatcher textWatcher = new TextWatcher() {

			public void afterTextChanged(Editable arg0) {
				Other = arg0.toString();
			}

			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}

			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
			}

		};

		public void onClick(View v) {
			Answer = null;

			for (int i = 0; i < listData.size(); i++) {
				RadioButton tempButton = (RadioButton) SingleViewVertical.this
						.findViewById(i);
				if (tempButton != null) {
					tempButton.setChecked(false);
					tempButton.setButtonDrawable(R.drawable.radiobutton);
					values.put(i, false);
				}
			}

			if (v.getId() != 88) {
				RadioButton tempButton = (RadioButton) SingleViewVertical.this
						.findViewById(88);
				if (tempButton != null) {
					tempButton.setChecked(false);
					tempButton.setButtonDrawable(R.drawable.radiobutton);
					values.put(88, false);
				}

				EditText et = (EditText) SingleViewVertical.this
						.findViewById(88);
				if (et != null) {
					et.setFocusable(false);
				}
			}

			RadioButton rb = (RadioButton) v;
			rb.setChecked(true);
			rb.setButtonDrawable(R.drawable.radiobutton_press);

			values.put(v.getId(), true);
			temp = v.getId();
		}

		public void onFocusChange(View v, boolean hasFocus) {
			EditText option = (EditText) v;
			Other = option.getText().toString();
			RadioButton tempButton = (RadioButton) SingleViewVertical.this
					.findViewById(option.getId());

			if (hasFocus) {
				for (int i = 0; i < listdata.size(); i++) {
					RadioButton rr = (RadioButton) SingleViewVertical.this
							.findViewById(i);
					if (rr != null) {
						rr.setChecked(false);
						rr.setButtonDrawable(R.drawable.radiobutton);
					}
					values.put(i, false);
				}

				tempButton.setChecked(true);
				tempButton.setButtonDrawable(R.drawable.radiobutton_press);
				values.put(option.getId(), true);
				/*
				 * by Awei 2013/04/20
				 * 加下面這行
				 * 沒加的話，直接選文字輸入框輸入文字後，按下下一題，會Crash
				 */
				temp = option.getId();
			} else {
				tempButton.setChecked(false);
				tempButton.setButtonDrawable(R.drawable.radiobutton);
				values.put(option.getId(), false);
			}
		}
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			this.setResult(RESULT_CANCELED);
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, R.string.Endanswer);
		return super.onCreateOptionsMenu(menu);
	}
}
