package tw.com.SurveyApp.view;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import tw.com.SurveyApp.R;
import tw.com.SurveyApp.dto.LogDto;
import tw.com.SurveyApp.dto.QuestionnaireDto;
import tw.com.SurveyApp.dto.SampleDto;
import tw.com.SurveyApp.service.AnswerService;
import tw.com.SurveyApp.service.DeleteAnsweredService;
import tw.com.SurveyApp.service.DynamicOptionByAnswerService;
import tw.com.SurveyApp.service.LogService;
import tw.com.SurveyApp.service.QuestionnaireService;
import tw.com.SurveyApp.service.SampleService;
import tw.com.SurveyApp.service.SubjectService;
import tw.com.SurveyApp.service.UpdataLogService;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class StartView extends Activity {
	private String SurveyGuid = "";
	private String SampleID = "";
	private String SubjectIndex = "";
	private String Previous = "";
	private String serial = "";
	private ArrayList<HashMap<String, String>> listdata = null;
	Intent intent = new Intent();

	private Button btnBegananswer, btnEndanswer;
	private TextView textGreetingText, textThankText;

	private static final int START_VIEW = 0;

	private String SampleLogID = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.start);

		btnBegananswer = (Button) findViewById(R.id.btnBegananswer);
		btnEndanswer = (Button) findViewById(R.id.btnEndanswer);

		btnEndanswer.setVisibility(View.GONE);

		Bundle extras = getIntent().getExtras();
		SurveyGuid = extras.getString("SurveyGuid");
		SampleID = extras.getString("SampleGuid");

		listdata = SubjectService.queryAllSubject(SurveyGuid, StartView.this);

		String GreetingText = QuestionnaireService.getGreetingText(SurveyGuid,
				StartView.this);

		textGreetingText = (TextView) findViewById(R.id.textGreetingText);
		textGreetingText.setText(GreetingText);

		String ThankText = QuestionnaireService.getThankText(SurveyGuid,
				StartView.this);

		textThankText = (TextView) findViewById(R.id.textThankText);
		textThankText.setText(ThankText);
		textThankText.setVisibility(View.GONE);

		SubjectIndex = listdata.get(0).get("SubjectIndex");
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				|| keyCode == KeyEvent.KEYCODE_HOME) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	public void first(View v) {
		if (listdata != null) {

			UpdataLogService.cmdDelete(SurveyGuid, SampleID, StartView.this);

			int CompleteLength = AnswerService.getLength(SampleID,
					StartView.this);

			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			ArrayList<HashMap<String, String>> loglist = new LogService()
					.queryLogdata(SurveyGuid, SampleID, StartView.this);

			int max = 0;
			if (loglist != null) {
				for (int i = 0; i < loglist.size(); i++) {
					int m = Integer.parseInt(loglist.get(i).get("Sindex"));

					if (max <= m) {
						max = m + 1;
					}
				}
			} else {
				max = 1;
			}

			LogDto logDto = new LogDto();
			logDto.setSampleGuid(SampleID);
			logDto.setSurveyGuid(SurveyGuid);
			logDto.setSindex(max + "");
			logDto.setStart(df.format(calendar.getTime()));
			logDto.setEnd(df.format(calendar.getTime()));

			SampleLogID = LogService.cmdInsert(logDto, StartView.this);

			if (CompleteLength != 0) {
				SubjectIndex = AnswerService.getLastSubjectIndex(SampleID,
						StartView.this);

				Previous = AnswerService.getPrevious(SubjectIndex, SampleID,
						StartView.this);

				ArrayList<HashMap<String, String>> subjectDto = SubjectService
						.queryAllSubject(SurveyGuid, SubjectIndex,
								StartView.this);

				if (subjectDto.get(0).get("SubjectType").equals("T")) {
					intent.setClass(StartView.this, FromTextView.class);
				} else if (subjectDto.get(0).get("SubjectType").equals("S")) {
					if (subjectDto.get(0).get("OptionOutputForm") == null) {
						intent.setClass(StartView.this, SingleView.class);
					} else if (subjectDto.get(0).get("OptionOutputForm")
							.equals("Vertical")) {
						intent.setClass(StartView.this,
								SingleViewVertical.class);
					} else {
						intent.setClass(StartView.this, SingleView.class);
					}
				} else if (subjectDto.get(0).get("SubjectType").equals("M")) {
					if (subjectDto.get(0).get("OptionOutputForm") == null) {
						intent.setClass(StartView.this, MultView.class);
					} else if (subjectDto.get(0).get("OptionOutputForm")
							.equals("Vertical")) {
						intent.setClass(StartView.this, MultViewVertical.class);
					} else {
						intent.setClass(StartView.this, MultView.class);
					}
				} else if (subjectDto.get(0).get("SubjectType").equals("FI")) {
					intent.setClass(StartView.this, FiveView.class);
				} else if (subjectDto.get(0).get("SubjectType").equals("FO")) {
					intent.setClass(StartView.this, FourView.class);
				} else if (subjectDto.get(0).get("SubjectType").equals("G")) {
					if (subjectDto.get(0).get("OptionOutputForm") == null) {
						intent.setClass(StartView.this, GroupView.class);
					} else if (subjectDto.get(0).get("OptionOutputForm")
							.equals("Vertical")) {
						intent.setClass(StartView.this, GroupViewVertical.class);
					} else {
						intent.setClass(StartView.this, GroupView.class);
					}
				} else if (subjectDto.get(0).get("SubjectType").equals("P")) {
					intent.setClass(StartView.this, DropView.class);
				}

				intent.putExtra("SurveyGuid", SurveyGuid);
				intent.putExtra("SubjectIndex", SubjectIndex);
				intent.putExtra("SubjectType",
						subjectDto.get(0).get("SubjectType"));
				intent.putExtra("SubjectTitle",
						subjectDto.get(0).get("SubjectTitle"));
				intent.putExtra("SampleID", SampleID);
				intent.putExtra("Previous", Previous);
			} else {
				HashMap<String, String> single = listdata.get(0);

				if (single.get("SubjectType").equals("T")) {
					intent.setClass(StartView.this, FromTextView.class);
				} else if (single.get("SubjectType").equals("S")) {
					if (single.get("OptionOutputForm") == null) {
						intent.setClass(StartView.this, SingleView.class);
					} else if (single.get("OptionOutputForm")
							.equals("Vertical")) {
						intent.setClass(StartView.this,
								SingleViewVertical.class);
					} else {
						intent.setClass(StartView.this, SingleView.class);
					}
				} else if (single.get("SubjectType").equals("M")) {
					if (single.get("OptionOutputForm") == null) {
						intent.setClass(StartView.this, MultView.class);
					} else if (single.get("OptionOutputForm")
							.equals("Vertical")) {
						intent.setClass(StartView.this, MultViewVertical.class);
					} else {
						intent.setClass(StartView.this, MultView.class);
					}
				} else if (single.get("SubjectType").equals("Fi")) {
					intent.setClass(StartView.this, FiveView.class);
				} else if (single.get("SubjectType").equals("FO")) {
					intent.setClass(StartView.this, FourView.class);
				} else if (single.get("SubjectType").equals("G")) {
					if (single.get("OptionOutputForm") == null) {
						intent.setClass(StartView.this, GroupView.class);
					} else if (single.get("OptionOutputForm")
							.equals("Vertical")) {
						intent.setClass(StartView.this, GroupViewVertical.class);
					} else {
						intent.setClass(StartView.this, GroupView.class);
					}
				} else if (single.get("SubjectType").equals("P")) {
					intent.setClass(StartView.this, DropView.class);
				}

				intent.putExtra("SurveyGuid", SurveyGuid);
				intent.putExtra("SubjectIndex", SubjectIndex);
				intent.putExtra("SubjectType", single.get("SubjectType"));
				intent.putExtra("SubjectTitle", single.get("SubjectTitle"));
				intent.putExtra("SampleID", SampleID);
				intent.putExtra("Previous", "null");
			}

			btnBegananswer.setVisibility(View.GONE);
			btnEndanswer.setVisibility(View.GONE);
			textGreetingText.setVisibility(View.GONE);
			textThankText.setVisibility(View.GONE);

			this.startActivityForResult(intent, START_VIEW);
			//停止過場動畫
			overridePendingTransition(0, 0);
		} else {
			Toast.makeText(StartView.this, "錯誤找不到題目", Toast.LENGTH_LONG).show();
		}
	}

	public void end(View v) {
		this.setResult(RESULT_OK);
		finish();
		//停止過場動畫
		overridePendingTransition(0, 0);
	}

	private void returnBack() {
		this.setResult(RESULT_CANCELED);
		finish();
		//停止過場動畫
		overridePendingTransition(0, 0);
	}

	private void CompletedQuestion() {
		btnEndanswer.setVisibility(View.VISIBLE);
		btnBegananswer.setVisibility(View.GONE);
		textThankText.setVisibility(View.VISIBLE);
		textGreetingText.setVisibility(View.GONE);
		// 取得日期
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		int month = now.get(Calendar.MONTH) + 1;
		int day = now.get(Calendar.DAY_OF_MONTH);

		// 回寫到「樣本」資料表中，更新「完成問卷日期」
		SampleDto sampleDto = new SampleDto();
		sampleDto.setAnswerDate(year + "-" + month + "-" + day);

		SampleService
				.cmdUpdata(sampleDto, SampleID, SurveyGuid, StartView.this);

		String completedRow = String.valueOf(SampleService.getLength(
				SurveyGuid, StartView.this));

		QuestionnaireDto questionaireDto = new QuestionnaireDto();
		questionaireDto.setCompletedRow(completedRow);

		QuestionnaireService.cmdUpdata(questionaireDto, SurveyGuid,
				StartView.this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == START_VIEW) {
			if (resultCode == RESULT_OK) {
				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat df = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");

				LogDto logDto = new LogDto();
				logDto.setEnd(df.format(calendar.getTime()));

				LogService.cmdUpdata(logDto, SampleLogID, StartView.this);

				Bundle extras = data.getExtras();
				SubjectIndex = extras.getString("SubjectIndex");
				Previous = extras.getString("Previous");

				if (SubjectIndex == null || SubjectIndex.equals("null")
						|| SubjectIndex.trim().equals("")) {
					CompletedQuestion();
				}

				ArrayList<HashMap<String, String>> subjectDto = SubjectService
						.queryAllSubject(SurveyGuid, SubjectIndex,
								StartView.this);

				if (subjectDto.size() == 0) {
					return;
				}

				boolean dob = DynamicOptitonByAnswer();
				while (dob) {
					// 取得代碼
					serial = SubjectService.getSerial(SurveyGuid, SubjectIndex,
							StartView.this);
					// 取得題號
					String Serial = Integer.parseInt(serial) + 1 + "";
					SubjectIndex = SubjectService.getSubjectIndex(SurveyGuid,
							Serial, StartView.this);
					if (SubjectIndex == null || SubjectIndex.equals("null")) {
						dob = false;
						break;
					}
					
					dob = DynamicOptitonByAnswer();
					subjectDto = SubjectService.queryAllSubject(SurveyGuid,
							SubjectIndex, StartView.this);
				}
				
				if (SubjectIndex == null || SubjectIndex.equals("null")) {
					CompletedQuestion();
				} else {

					if (subjectDto.get(0).get("SubjectType").equals("T")) {
						intent.setClass(StartView.this, FromTextView.class);
					} else if (subjectDto.get(0).get("SubjectType").equals("S")) {
						if (subjectDto.get(0).get("OptionOutputForm") == null) {
							intent.setClass(StartView.this, SingleView.class);
						} else if (subjectDto.get(0).get("OptionOutputForm")
								.equals("Vertical")) {
							intent.setClass(StartView.this,
									SingleViewVertical.class);
						} else {
							intent.setClass(StartView.this, SingleView.class);
						}
					} else if (subjectDto.get(0).get("SubjectType").equals("M")) {
						if (subjectDto.get(0).get("OptionOutputForm") == null) {
							intent.setClass(StartView.this, MultView.class);
						} else if (subjectDto.get(0).get("OptionOutputForm")
								.equals("Vertical")) {
							intent.setClass(StartView.this,
									MultViewVertical.class);
						} else {
							intent.setClass(StartView.this, MultView.class);
						}
					} else if (subjectDto.get(0).get("SubjectType")
							.equals("FI")) {
						intent.setClass(StartView.this, FiveView.class);
					} else if (subjectDto.get(0).get("SubjectType")
							.equals("FO")) {
						intent.setClass(StartView.this, FourView.class);
					} else if (subjectDto.get(0).get("SubjectType").equals("G")) {
						if (subjectDto.get(0).get("OptionOutputForm") == null) {
							intent.setClass(StartView.this, GroupView.class);
						} else if (subjectDto.get(0).get("OptionOutputForm")
								.equals("Vertical")) {
							intent.setClass(StartView.this,
									GroupViewVertical.class);
						} else {
							intent.setClass(StartView.this, GroupView.class);
						}
					} else if (subjectDto.get(0).get("SubjectType").equals("P")) {
						intent.setClass(StartView.this, DropView.class);
					}

					intent.putExtra("SurveyGuid", SurveyGuid);
					intent.putExtra("SubjectSerial",
							subjectDto.get(0).get("serial"));
					intent.putExtra("SubjectIndex", SubjectIndex);
					intent.putExtra("SubjectTitle",
							subjectDto.get(0).get("SubjectTitle"));
					intent.putExtra("Previous", Previous);
//					System.out.println("SubjectIndexQQ:"+SubjectIndex);
					this.startActivityForResult(intent, START_VIEW);
					//停止過場動畫
					overridePendingTransition(0, 0);
				}
			} else if (resultCode == RESULT_CANCELED) {
				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat df = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");

				LogDto logDto = new LogDto();
				logDto.setEnd(df.format(calendar.getTime()));

				LogService.cmdUpdata(logDto, SampleLogID, StartView.this);

				
//				StringBuilder whereClause = new StringBuilder();
//				whereClause.append(" serial = ").append(SampleLogID);
//
//				LogService.cmdUpdata(logDto, whereClause.toString(),
//						StartView.this);

				returnBack();
			}
		}
	}

	private boolean DynamicOptitonByAnswer() {
		ArrayList<HashMap<String, String>> subjectDto = SubjectService
				.queryAllSubject(SurveyGuid, SubjectIndex, StartView.this);

		if (subjectDto.get(0).get("DynamicOptionAnswer") != null
				&& !subjectDto.get(0).get("DynamicOptionAnswer").trim()
						.equals("")
				&& !subjectDto.get(0).get("DynamicOptionAnswer").trim()
						.equals("null")) {
			String DyaGuid = subjectDto.get(0).get("DynamicOptionAnswer");

			ArrayList<HashMap<String, String>> DynamicOptionAnswer = DynamicOptionByAnswerService
					.queryAllDyamicOption(SurveyGuid, DyaGuid, StartView.this);
			
			
			int andLogicTotalCount = DynamicOptionByAnswerService.getAndLogicLength(DyaGuid, StartView.this);
			int sum = 0;
			System.out.println("size:"+DynamicOptionAnswer.size() + "andCount:"+andLogicTotalCount);
			boolean isJump = false;
			for (int i = 0; i < DynamicOptionAnswer.size(); i++) {
				String SubjectNumber = DynamicOptionAnswer.get(i).get(
						"SubjectNumber");
				String TargetAnswer = DynamicOptionAnswer.get(i).get(
						"TargetAnswer");
				String IsOr = DynamicOptionAnswer.get(i).get(
						"IsOr");
//				System.out.println("SubjectNumber:"+SubjectNumber);
//				System.out.println("TargetAnswer:"+TargetAnswer);
//				System.out.println("IsOr:"+IsOr);
				HashMap<String, String> Answer = AnswerService.getAnswerArray(SurveyGuid,
						SubjectNumber, SampleID, StartView.this);

				String IsShow = DynamicOptionAnswer.get(i).get("IsShow");
				
				if(Answer != null) {
					for(int answerCount=0;answerCount<Answer.size();answerCount++){
						if (TargetAnswer.trim().equals(Answer.get(answerCount+"").trim())) { 
							if(IsShow.trim().equals("1")){
								isJump = false;
								break;
							}else if(IsShow.trim().equals("0")){ 
								//滿足以上條件不需回答此題,就要跳
								if(IsOr.trim().equals("0")){
									/*
									 * by Awei 2013/04/13
									 * (若F1和F3二題皆回答0天)-->皆，2個條件都要滿足，才達成用sum++
									 * 第一期的題目有這樣的題型
									 */
									sum++;
								}else if(IsOr.trim().equals("1")){
									isJump = true;
									break;
								}
							}
						}else {
							if(IsShow.trim().equals("1") && answerCount==Answer.size()-1){
								isJump = true;
								break;
							}
						}
					}
				}
				
			}

			if (sum == andLogicTotalCount) {
				isJump = true;
			}
			if (isJump){
				DeleteAnsweredService.deleteAnswerAnsweredForDynamicOptionAnswer(SurveyGuid, SampleID, SubjectIndex, StartView.this);
				return true;
			}
		} else {
			return false;
		}
		return false;
	}

	
	
}
