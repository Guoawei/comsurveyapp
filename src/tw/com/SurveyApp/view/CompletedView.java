package tw.com.SurveyApp.view;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.R;
import tw.com.SurveyApp.dto.SampleDto;
import tw.com.SurveyApp.service.AnswerService;
import tw.com.SurveyApp.service.DBService;
import tw.com.SurveyApp.service.QuestionnaireService;
import tw.com.SurveyApp.service.SampleService;
import tw.com.SurveyApp.service.SubjectService;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * 樣本列表
 * 
 * @author sakata
 * 
 */
public class CompletedView extends Activity implements OnTouchListener {
	private ListView listview = null;
	private String SurveyGuid = "";
	private String SampleID = "";
	private ArrayList<HashMap<String, String>> listdata = null;
	private LinearLayout llbg;
	private ProgressDialog progressDialog = null;
	private ImageButton logoutbutton, btnUpload, btnUploadSingle, btnlock,
			btnbegananswer;
	Thread mThread = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.completedlist);

		progressDialog = ProgressDialog.show(CompletedView.this, "載入中....",
				"資料處理中。。。", true);
		
		btnbegananswer = (ImageButton) findViewById(R.id.btnbegananswer);
		btnlock = (ImageButton) findViewById(R.id.btnlock);
		btnUpload = (ImageButton) findViewById(R.id.btnUpload);
		btnUploadSingle = (ImageButton) findViewById(R.id.btnUploadSingle);
		logoutbutton = (ImageButton) findViewById(R.id.logoutbutton);

		btnbegananswer.setId(R.id.btnbegananswer);
		btnlock.setId(R.id.btnlock);
		btnUpload.setId(R.id.btnUpload);
		btnUploadSingle.setId(R.id.btnUploadSingle);
		logoutbutton.setId(R.id.logoutbutton);

		btnbegananswer.setEnabled(false);
		btnlock.setEnabled(false);
		btnUploadSingle.setEnabled(false);
		btnbegananswer
				.setImageResource(R.drawable.progressnavbutton_start_lock);
		btnlock.setImageResource(R.drawable.progressnavbutton_lock_lock);
		btnUploadSingle
				.setImageResource(R.drawable.progressnavbutton_singleupload_lock);

		if (haveInternet()) {
			btnUpload.setEnabled(true);
			btnUpload.setImageResource(R.drawable.progressnavbutton_allupload);
		} else {
			btnUpload.setEnabled(false);
			btnUpload.setImageResource(R.drawable.progressnavbutton_allupload_lock);
		}
		
//		btnUpload.setEnabled(true);
		
		// 設定標題文字
		Bundle extras = getIntent().getExtras();
		SurveyGuid = extras.getString("SurveyGuid");

		LoadData();

		logoutbutton.setOnTouchListener(this);
		btnUpload.setOnTouchListener(this);
		btnUploadSingle.setOnTouchListener(this);
		btnlock.setOnTouchListener(this);
		btnbegananswer.setOnTouchListener(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		ImageButton ib = (ImageButton) v;
		switch (ib.getId()) {
		case R.id.logoutbutton:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setImageResource(R.drawable.logoutbutton_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setImageResource(R.drawable.logoutbutton);
			}
			break;
		case R.id.btnUpload:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setImageResource(R.drawable.progressnavbutton_allupload_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v)
						.setImageResource(R.drawable.progressnavbutton_allupload);
			}
			break;
		case R.id.btnUploadSingle:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setImageResource(R.drawable.progressnavbutton_singleupload_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v)
						.setImageResource(R.drawable.progressnavbutton_singleupload);
			}
			break;
		case R.id.btnlock:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setImageResource(R.drawable.progressnavbutton_lock_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v)
						.setImageResource(R.drawable.progressnavbutton_lock);
			}
			break;
		case R.id.btnbegananswer:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setImageResource(R.drawable.progressnavbutton_start_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v)
						.setImageResource(R.drawable.progressnavbutton_start);
			}
			break;
		}
		return false;
	}

	/**
	 * 查詢是否有Wifi，如果有回傳true, 如果沒有回傳false
	 * 
	 * @return
	 */
	private boolean isWifi() {
		ConnectivityManager connectivityManager = (ConnectivityManager) CompletedView.this
				.getSystemService(LoginView.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
		if (activeNetInfo != null
				&& activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {
			return true;
		}
		return false;
	}
	
	private boolean haveInternet()
    {
    	boolean result = false;
    	ConnectivityManager connManager = (ConnectivityManager) getSystemService(LoginView.CONNECTIVITY_SERVICE); 
    	NetworkInfo info=connManager.getActiveNetworkInfo();
    	if (info == null || !info.isConnected())
    	{
    		result = false;
    	}
    	else 
    	{
    		if (!info.isAvailable())
    		{
    			result =false;
    		}
    		else
    		{
    			result = true;
    		}
    	}
    	
    	return result;
    }

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				|| keyCode == KeyEvent.KEYCODE_HOME) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	final Handler cwjHandler = new Handler();

	final Runnable mUpdateResults = new Runnable() {
		public void run() {
			String Topic = QuestionnaireService.getTitle(SurveyGuid,
					CompletedView.this);
			TextView textTopic = (TextView) findViewById(R.id.textTopic);
			textTopic.setText(Topic);

			// 處理ListView的資料
			listview = (ListView) findViewById(R.id.listViewCompleted);
			// 顯示ListView
			setCompletedListView(CompletedView.this, listview, SurveyGuid);
		}
	};

	/**
	 * 載入資料
	 */
	private void LoadData() {
//		progressDialog = ProgressDialog.show(CompletedView.this, "載入中....",
//				"資料處理中。。。", true);
//
//		mThread = new Thread() {
//			public void run() {
//				progressDialog.dismiss();
//				progressDialog = null;
				cwjHandler.post(mUpdateResults);
//			}
//		};
//
//		mThread.start();
	}

	/**
	 * 回首頁-按鈕
	 * 
	 * @param v
	 */
	public void end(View v) {
		finish();
		//停止過場動畫
		overridePendingTransition(0, 0);
	}

	/**
	 * 開始做答-按鈕
	 * 
	 * @param v
	 */
	public void began(View v) {
		Intent intent = new Intent();
		intent.setClass(CompletedView.this, StatusLog.class);
		intent.putExtra("SurveyGuid", SurveyGuid);
		intent.putExtra("SampleGuid", SampleID);
		this.startActivityForResult(intent, 0);
		//停止過場動畫
		overridePendingTransition(0, 0);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 0) {
			if (resultCode == 0) {
				listdata = null;
				// 顯示ListView
				LoadData();
			}
		}
	}

	/**
	 * 上傳全部樣本-按鈕
	 * 
	 * @param v
	 */
	public void update(View v) {
		Intent intent = new Intent();
		intent.setClass(CompletedView.this, UpdataView.class);
		intent.putExtra("SurveyGuid", SurveyGuid);
		this.startActivity(intent);
		//停止過場動畫
		overridePendingTransition(0, 0);
	}

	/**
	 * 上傳單一樣本-按鈕
	 * 
	 * @param v
	 */
	public void updatesingle(View v) {
		Intent intent = new Intent();
		intent.setClass(CompletedView.this, UpdataViewSingle.class);
		intent.putExtra("SurveyGuid", SurveyGuid);
		intent.putExtra("SampleID", SampleID);
		this.startActivity(intent);
		//停止過場動畫
		overridePendingTransition(0, 0);
	}

	/**
	 * 鎖定-按鈕
	 * 
	 * @param v
	 */
	public void lock(View v) {
		// 建立提示息框
		Builder MyBuilder = new AlertDialog.Builder(CompletedView.this);
		MyBuilder.setTitle("訊息");
		MyBuilder.setMessage("鎖定後就不能再更改答案，是否確定？");

		// 建立按鈕事件
		OnClickListener okClick = new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				// 當按下「確定」鍵
				if (which == -1) {
					// 更改樣本狀態為鎖定
					SampleDto dto = new SampleDto();
					dto.setIsupdate("1");

					SampleService.cmdUpdata(dto, SampleID, SurveyGuid,
							CompletedView.this);

					btnbegananswer.setEnabled(false);
					btnlock.setEnabled(false);
					btnbegananswer.setImageResource(R.drawable.progressnavbutton_start_lock);
					btnlock.setImageResource(R.drawable.progressnavbutton_lock_lock);

					// 重新載入
					LoadData();
				}
			}
		};

		// 建立訊息按鈕並且顯示
		MyBuilder.setPositiveButton("確定", okClick);
		MyBuilder.setNegativeButton("取消", okClick);
		MyBuilder.show();
	}

	public void setCompletedListView(Context context, ListView listview,
			String QuestionarireID) {

		// 顯示ListView
		setCompletedRow(CompletedView.this, listview, QuestionarireID);

		// 建立ListView的Click事件
		listview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {

				HashMap<String, String> single = listdata.get(position);

				SurveyGuid = single.get("SurveyGuid");
				SampleID = single.get("SampleGuid");

				btnUploadSingle.setEnabled(true);
				btnUploadSingle.setImageResource(R.drawable.progressnavbutton_singleupload);

				// 是否鎖定
				boolean complete = SampleService.getIsUpdata(SurveyGuid,
						SampleID, CompletedView.this);

				if (!complete) {
					if (llbg != null) {
						llbg.setBackgroundResource(R.color.unfocused);
					}

					btnbegananswer.setEnabled(true);
					btnbegananswer.setImageResource(R.drawable.progressnavbutton_start);

					// 取得作答筆數
					int length = AnswerService.getLength(SampleID,
							CompletedView.this);

					// 至少要回答一題，才可以鎖定
					if (length > 0) {
						btnlock.setEnabled(true);
						btnlock.setImageResource(R.drawable.progressnavbutton_lock);
					} else {
						btnlock.setEnabled(false);
						btnlock.setImageResource(R.drawable.progressnavbutton_lock_lock);
					}

					llbg = (LinearLayout) arg1;

					arg1.setBackgroundResource(R.color.focused);
				} else {
					if (llbg != null) {
						llbg.setBackgroundResource(R.color.unfocused);
					}

					btnbegananswer.setEnabled(false);
					btnlock.setEnabled(false);
					btnbegananswer.setImageResource(R.drawable.progressnavbutton_start_lock);
					btnlock.setImageResource(R.drawable.progressnavbutton_lock_lock);

					llbg = (LinearLayout) arg1;

					arg1.setBackgroundResource(R.color.focused);
				}
			}
		});
	}

	/**
	 * 顯示ListView
	 * 
	 * @param context
	 * @param listview
	 * @param QuestionarireID
	 * @return
	 */
	public HashMap<Integer, String> setCompletedRow(Context context,
			ListView listview, String QuestionarireID) {

		// 查詢「樣本」資料
		listdata = DBService.querySample(QuestionarireID, CompletedView.this);

		HashMap<Integer, String> values = null;

		if (listdata != null) {
			CompletedAdapter adapter = new CompletedAdapter(context, listdata);
			listview.setAdapter(adapter);
			values = adapter.values;
			return values;
		}
		return values;
	}

	public class CompletedAdapter extends BaseAdapter {
		Context context;
		ArrayList<HashMap<String, String>> listData;
		HashMap<Integer, String> values = new HashMap<Integer, String>();

		int MaxLength = 0;

		public CompletedAdapter(Context context,
				ArrayList<HashMap<String, String>> listData) {
			this.context = context;
			this.listData = listData;

			MaxLength = SubjectService
					.getLength(SurveyGuid, CompletedView.this);
		}

		/**
		 * 取得資料筆數
		 */
		public int getCount() {
			return listData.size();
		}

		/**
		 * 取得資料列
		 */
		public Object getItem(int position) {
			return listData.get(position);
		}

		/**
		 * 取得資料ID
		 */
		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			LayoutInflater mInflater = LayoutInflater.from(context);
			convertView = mInflater.inflate(R.layout.completedlistrow, null);

			// 儲存「樣本名稱」
			TextView textAuthor = (TextView) convertView
					.findViewById(R.id.textAuthor);
			textAuthor.setText((String) listData.get(position).get("name"));
			// 儲存「樣本資料上傳否」
			TextView textUploaded = (TextView) convertView
					.findViewById(R.id.textUploaded);
			String isupdata = Integer.parseInt((String) listData.get(position)
					.get("isupdate")) == 0 ? "否" : "是";
			textUploaded.setText(isupdata);
			// 儲存「樣本問卷版本」
			TextView textVersions = (TextView) convertView
					.findViewById(R.id.textVersions);
			textVersions.setText((String) listData.get(position).get("Ver"));
			// 儲存「樣本作答日期」
			TextView textAnswerDate = (TextView) convertView
					.findViewById(R.id.textAnswerDate);
			textAnswerDate.setText((String) listData.get(position).get(
					"AnswerDate"));

			ProgressBar pbCompleted = (ProgressBar) convertView
					.findViewById(R.id.pbCompleted);

			pbCompleted.setMax(MaxLength);

			String AnswerDate = (String) listData.get(position).get(
					"AnswerDate");

			// 儲存完成進度
			if (AnswerDate != null && !AnswerDate.trim().equals("")) {
				pbCompleted.setProgress(MaxLength);
			} else {
				String SampleID = (String) listData.get(position).get(
						"SampleGuid");
				int CompleteLength = AnswerService.getLength(SampleID,
						CompletedView.this);

				pbCompleted.setProgress(CompleteLength);
			}

			return convertView;
		}
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		progressDialog.dismiss();
	}

}
