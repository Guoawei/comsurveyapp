package tw.com.SurveyApp.view;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.util.ByteArrayBuffer;

import tw.com.SurveyApp.R;
import tw.com.SurveyApp.data.JsonConnection;
import tw.com.SurveyApp.dto.StatusDto;
import tw.com.SurveyApp.service.InterviewService;
import tw.com.SurveyApp.service.JsonService;
import tw.com.SurveyApp.service.StatusService;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 登入畫面
 * 
 * @author sakata
 * 
 */
public class LoginView extends Activity implements OnTouchListener {

	private ArrayList<HashMap<String, String>> listdata = null;
	private ImageButton loigin;
	private EditText email, editPassword;
	private Handler mHandler;
	private TextView versionNameText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.login);

		/*
		 * 取得Version Name
		 * */
		versionNameText = (TextView) findViewById(R.id.VersionText);
		try
		{
		    String app_ver = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;
		    versionNameText.setText("Ver "+app_ver);
		}
		catch (NameNotFoundException e)
		{
		    System.out.println(e);
		}
		
		
		mHandler = new Handler();

		loigin = (ImageButton) findViewById(R.id.butlogin);
		loigin.setId(R.id.butlogin);

		email = (EditText) findViewById(R.id.editAccount);
		editPassword = (EditText) findViewById(R.id.editPassword);

		listdata = InterviewService.queryAll(LoginView.this);

		if (listdata.size() == 1) {
			email.setText(InterviewService.getEmail(LoginView.this));
			email.setEnabled(false);
			editPassword.requestFocus();
		}

//		int Length = new StatusService().getLength(LoginView.this);
		StatusService.cmdDelete(LoginView.this);
//		if (Length == 0) {
			StatusDto statusDto = new StatusDto();

			String StatusAll = new StatusService().getStatus();
			String StatusList[] = StatusAll.split(":");
			for (String sl : StatusList) {
				String Status[] = sl.split(",");

				statusDto.setStatusNumber(Status[0]);
				statusDto.setName(Status[1]);
				statusDto.setStep(Status[2]);

				StatusService.cmdInsert(statusDto, LoginView.this);
			}
//		}

		loigin.setOnTouchListener(this);

		checkUpdate.start();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		ImageButton ib = (ImageButton) v;
		switch (ib.getId()) {
		case R.id.butlogin:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v).setImageResource(R.drawable.login_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setImageResource(R.drawable.login);
			}
			break;
		}
		return false;
	}

	private String toHexString(byte[] in) {
		StringBuilder hexString = new StringBuilder();
		for (int i = 0; i < in.length; i++) {
			String hex = Integer.toHexString(0xFF & in[i]);
			if (hex.length() == 1) {
				hexString.append('0');
			}
			hexString.append(hex);
		}
		return hexString.toString();
	}

	public void Loigin(View v) {

		String Email = email.getText().toString();
		String Password = editPassword.getText().toString();

		String result = "";

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");

			md.update(Password.getBytes());

			result = toHexString(md.digest());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		listdata = InterviewService.queryAll(LoginView.this);

		if (listdata == null || listdata.size() == 0) {
			/*
			 * Edit by Awei 20130503
			 * 換掉判斷網路的方式 
			 */
			//if (isWifi()) {
			if (haveInternet()) {	
				LoginWifi(result, Email, Password);
			} else {
				Toast.makeText(LoginView.this, "請在有網路的環境下進行第一次登入", Toast.LENGTH_LONG)
						.show();
			}
		} else {
			/*
			 * Edit by Awei 20130503
			 * 換掉判斷網路的方式
			 * 若第一次登入過，第二次不用網路也可以登入 
			 */
			//if (isWifi()) {  
//			if (haveInternet()) 
//			{
//				LoginWifi(result, Email, Password);
//			} else {
				LoginNoWifi(Email, Password);
//			}
		}
	}

	private void LoginNoWifi(String Email, String Password) {
		// 取得訪員密碼
		String paw = InterviewService.getPassword(Email, LoginView.this);
		if (paw != null && paw.equals(Password)) {

			loigin.setEnabled(false);

			Intent intent = new Intent();
			intent.setClass(LoginView.this, QuestionnaireView.class);

			String Guid = InterviewService.getInterviewGuid(Email,
					LoginView.this);

			intent.putExtra("Guid", Guid);
			this.startActivity(intent);
			//停止過場動畫
			overridePendingTransition(0, 0);
		} else {
			Toast.makeText(LoginView.this, "帳號密碼錯誤", Toast.LENGTH_LONG)
					.show();
		}
	}

	private void LoginWifi(String result, String Email, String Password) {
		String Guid = "";

		JsonService js = new JsonService();
		String strResult = "";

		StringBuilder url = new StringBuilder();
		url.append(JsonConnection.connection).append("login?").append("email=")
				.append(Email).append("&").append("pwd=").append(result);
		
		JsonConnection jc = new JsonConnection();
		strResult = jc.connServerForResult(url.toString());
		
		String status = js.readTrue(strResult);
		
		if (!status.equals("0")) {
			Toast.makeText(LoginView.this, "帳號 密碼 錯誤", Toast.LENGTH_LONG).show();
		} else {
			listdata = InterviewService.queryAll(LoginView.this);

			if (listdata == null || listdata.size() == 0) {
				Guid = js.readInterViewe(strResult, Password, LoginView.this);
			} else {
				Guid = listdata.get(0).get("Guid");
			}

			loigin.setEnabled(false);

			Intent intent = new Intent();
			intent.setClass(LoginView.this, QuestionnaireView.class);
			intent.putExtra("Guid", Guid);
			this.startActivity(intent);
			//停止過場動畫
			overridePendingTransition(0, 0);
		}
	}

	/**
	 * 查詢是否有Wifi，如果有回傳true, 如果沒有回傳false
	 * 
	 * @return
	 */
	private boolean isWifi() {
		ConnectivityManager connectivityManager = (ConnectivityManager) LoginView.this
				.getSystemService(LoginView.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
		if (activeNetInfo != null
				&& activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {
			return true;
		}
		return false;
	}
	
	private boolean haveInternet()
    {
    	boolean result = false;
    	ConnectivityManager connManager = (ConnectivityManager) getSystemService(LoginView.CONNECTIVITY_SERVICE); 
    	NetworkInfo info=connManager.getActiveNetworkInfo();
    	if (info == null || !info.isConnected())
    	{
    		result = false;
    	}
    	else 
    	{
    		if (!info.isAvailable())
    		{
    			result =false;
    		}
    		else
    		{
    			result = true;
    		}
    	}
    	
    	return result;
    }

	private Thread checkUpdate = new Thread() {
		public void run() {
			try {

				JsonService js = new JsonService();
				String strResult = "";

				StringBuilder url = new StringBuilder();
				url.append(JsonConnection.connection).append("updateApp");

				JsonConnection jc = new JsonConnection();
				strResult = jc.connServerForResult(url.toString());

				String doc = js.readTrue(strResult);

				// UpdateAppDto dto = js.getUpdateApp(strResult,
				// LoginView.this);

				if (!doc.equals("0")) {
					Toast.makeText(LoginView.this, "檢查更新資料失敗",
							Toast.LENGTH_LONG).show();
				} else {
					URL updateURL = new URL(
							new JsonConnection().getAppVersion());
					URLConnection conn = updateURL.openConnection();
					InputStream is = conn.getInputStream();
					BufferedInputStream bis = new BufferedInputStream(is);
					ByteArrayBuffer baf = new ByteArrayBuffer(50);

					int current = 0;
					while ((current = bis.read()) != -1) {
						baf.append((byte) current);
					}

					final String s = new String(baf.toByteArray());
					int curVersion = getPackageManager().getPackageInfo(
							"tw.com.SurveyApp", 0).versionCode;

					int newVersion = Integer.valueOf(s);

					if (newVersion > curVersion) {
						mHandler.post(showUpdate);
					}
				}
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	};

	private Runnable showUpdate = new Runnable() {
		public void run() {
			new AlertDialog.Builder(LoginView.this)
					.setTitle("Update Available")
					.setMessage("An update for your SurveyApp!")
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									Intent intent = new Intent(
											Intent.ACTION_VIEW);
									Uri uri = Uri
											.fromFile(new File(
													new JsonConnection()
															.getAppUpdata()));
									intent.setDataAndType(uri,
											"application/vnd.android.package-archive");
									startActivity(intent);
									//停止過場動畫
									overridePendingTransition(0, 0);
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
								}
							}).show();
		}
	};
}
