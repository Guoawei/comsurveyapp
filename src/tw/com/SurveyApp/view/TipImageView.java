package tw.com.SurveyApp.view;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import tw.com.SurveyApp.R;
import tw.com.SurveyApp.service.TipService;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher.ViewFactory;

public class TipImageView extends Activity {

	// 圖片索引
	private int index = 0;
	// 顯示的圖片資源
	private List<Drawable> list = new ArrayList<Drawable>();
	private String SurveyGuid;
	private String SubjectIndex;
	private TextView textTip;
	private String TipText[];

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle extras = getIntent().getExtras();
		SurveyGuid = extras.getString("SurveyGuid");
		SubjectIndex = extras.getString("SubjectIndex");

		// 全屏設置
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		this.setContentView(R.layout.tipimage);

		// 得到ImageSwitcher對象
		final ImageSwitcher is = (ImageSwitcher) findViewById(R.id.image);
		
		textTip = (TextView) findViewById(R.id.textTip);

		ArrayList<HashMap<String, String>> listdata = TipService.queryAllTip(
				SurveyGuid, SubjectIndex, TipImageView.this);
		
		TipText = new String[listdata.size()];

		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {

			for (int i = 0; i < listdata.size(); i++) {
				String filename = listdata.get(i).get("tip_image");
				String path = Environment.getExternalStorageDirectory()
						.getAbsolutePath() + "/download_test/" + filename;
				File file = new File(path);
				list.add(Drawable.createFromPath(file.getAbsolutePath()));
				
				TipText[i] = listdata.get(i).get("tip_text");
			}
		}
		
		textTip.setText(TipText[0]);

		// 必須設置switcher 的 ViewFactory
		is.setFactory(new ViewFactory() {

			public View makeView() {
				return new ImageView(TipImageView.this);
			}

		});

		if (list.size() > 0) {
			is.setImageDrawable(list.get(0));
		}

		// 設置點擊事件
		is.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				index++;
				if (index >= list.size()) {
					index = 0;
				}
				is.setImageDrawable(list.get(index));
				textTip.setText(TipText[index]);
			}
		});

		// 設置切入動畫
		is.setInAnimation(AnimationUtils.loadAnimation(getApplicationContext(),
				android.R.anim.slide_in_left));

		// 設置切出動畫
		is.setInAnimation(AnimationUtils.loadAnimation(getApplicationContext(),
				android.R.anim.slide_out_right));
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME){
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}