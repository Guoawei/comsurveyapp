package tw.com.SurveyApp.view;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.R;
import tw.com.SurveyApp.dto.AnswerDto;
import tw.com.SurveyApp.service.AnswerService;
import tw.com.SurveyApp.service.DeleteAnsweredService;
import tw.com.SurveyApp.service.InterviewService;
import tw.com.SurveyApp.service.OptionService;
import tw.com.SurveyApp.service.SinglePsaaService;
import tw.com.SurveyApp.service.SubjectService;
import tw.com.SurveyApp.service.TipService;
import android.app.Activity;
//import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MultView extends Activity implements OnTouchListener {
	private ListView listview = null;
	private String SurveyGuid = "";
	private String SubjectIndex = "";
	private String SubjectTitle = "";
	private String Previous = "";
	private String serial = "";
	private String SampleID = "";
	private ArrayList<HashMap<String, String>> listdata = null;
	private ImageButton btnPrevious, btnNext;
	private ImageButton btnPreviousPage, btnNextPage;
	private ImageButton btnPicture;
	private TextView textTips, textSubjectIndexTitle;
	private HashMap<Integer, Boolean> values = new HashMap<Integer, Boolean>();
	private HashMap<Integer, String> index = new HashMap<Integer, String>();
	private String[] str;
//	private ProgressDialog progressDialog = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.subject);

		/*
		 * by Awei 2013/04/16
		 * 把題號跟題目合併在一起
		 */
		textSubjectIndexTitle = (TextView) findViewById(R.id.textSubjectIndexTitle);

		btnPrevious = (ImageButton) findViewById(R.id.btnPrevious);
		btnNext = (ImageButton) findViewById(R.id.btnNext);
		btnPreviousPage = (ImageButton) findViewById(R.id.btnPreviousPage);
		btnNextPage = (ImageButton) findViewById(R.id.btnNextPage);

		btnPrevious.setId(R.id.btnPrevious);
		btnNext.setId(R.id.btnNext);
		btnPreviousPage.setId(R.id.btnPreviousPage);
		btnNextPage.setId(R.id.btnNextPage);

		btnPicture = (ImageButton) findViewById(R.id.btnPicture);
		textTips = (TextView) findViewById(R.id.textTips);

		btnPreviousPage.setVisibility(View.GONE);
		btnNextPage.setVisibility(View.GONE);

		// 處理ListView的資料
		listview = (ListView) findViewById(R.id.listViewFormText);
		/*
		 * Add By Awei 
		 * 2013/05/26
		 * V 1.1.19
		 * 新增捲軸永久顯示
		 */
		listview.setScrollbarFadingEnabled(false);

		Bundle extras = getIntent().getExtras();
		SurveyGuid = extras.getString("SurveyGuid");
		SubjectIndex = extras.getString("SubjectIndex");
		SubjectTitle = extras.getString("SubjectTitle");
		Previous = extras.getString("Previous");
		SampleID = extras.getString("SampleID");

		/*
		 * by Awei 2013/04/16
		 * 把題號跟題目合併在一起
		 */
		textSubjectIndexTitle.setText(SubjectIndex+" : "+SubjectTitle);

		// 如果是第一題的話隱藏「上一題」按鈕
		if (Previous.equals("null")) {
			btnPrevious.setEnabled(false);
			// btnPrevious.setVisibility(View.INVISIBLE);
		}

		LoadData();

		btnPrevious.setOnTouchListener(this);
		btnNext.setOnTouchListener(this);
		btnPreviousPage.setOnTouchListener(this);
		btnNextPage.setOnTouchListener(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		ImageButton ib = (ImageButton) v;
		switch (ib.getId()) {
		case R.id.btnPrevious:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.previousbtn_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.previousbtn);
			}
			break;
		case R.id.btnNext:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.nextbutton_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.nextbutton);
			}
			break;
		case R.id.btnPreviousPage:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.previousbtn_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.previousbtn);
			}
			break;
		case R.id.btnNextPage:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.nextbutton_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.nextbutton);
			}
			break;
		}
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				|| keyCode == KeyEvent.KEYCODE_HOME) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	final Handler cwjHandler = new Handler();

	final Runnable mUpdateResults = new Runnable() {
		public void run() {
			listdata = SubjectService
					.queryAllSubject(SurveyGuid, MultView.this);

			ArrayList<HashMap<String, String>> tipdata = TipService
					.queryAllTip(SurveyGuid, SubjectIndex, MultView.this);

			// 是否有提示卡，有的話顯示提示卡
			if (tipdata.size() == 0) {
				btnPicture.setVisibility(View.GONE);
				textTips.setVisibility(View.GONE);
			} else {
				if (tipdata.get(0).get("tip_image") != null
						&& !tipdata.get(0).get("tip_image").trim().equals("")) {
					textTips.setVisibility(View.GONE);
				} else {
					btnPicture.setVisibility(View.GONE);
					if (tipdata.get(0).get("tip_text") == null
							|| tipdata.get(0).get("tip_text").trim().equals("")) {
						textTips.setVisibility(View.GONE);
					} else {
						textTips.setText(tipdata.get(0).get("tip_text"));
					}
				}
			}

			// 顯示ListView
			setMultRow(listview, SurveyGuid, SubjectIndex);

			// 如果是第一題的話隱藏「上一題」按鈕
			if (Previous.equals("null")) {
				btnPrevious.setVisibility(View.INVISIBLE);
			}
		}
	};

	/**
	 * 載入資料
	 */
	private void LoadData() {
//		progressDialog = ProgressDialog.show(MultView.this, "載入中....",
//				"資料處理中。。。", true);
//
//		new Thread() {
//			public void run() {
//				progressDialog.dismiss();

				cwjHandler.post(mUpdateResults);
//			}
//		}.start();
	}

	/**
	 * 上一題
	 * 
	 * @param v
	 */
	public void previous(View v) {
		Intent intent = new Intent();
		intent.putExtra("SurveyGuid", SurveyGuid);
		intent.putExtra("SubjectIndex", Previous);

		// 查詢「上一題」的「題號]
		SubjectIndex = AnswerService.getSubjectIndex(Previous, SampleID,
				MultView.this);

		intent.putExtra("Previous", SubjectIndex);

		this.setResult(RESULT_OK, intent);

		finish();
		//停止過場動畫
		overridePendingTransition(0, 0);
	}

	/**
	 * 提示卡圖片
	 * 
	 * @param v
	 */
	public void picture(View v) {
		Intent intent = new Intent();
		intent.putExtra("SurveyGuid", SurveyGuid);
		intent.putExtra("SubjectIndex", SubjectIndex);
		intent.setClass(MultView.this, TipImageView.class);
		MultView.this.startActivity(intent);
	}

	/**
	 * 取得代碼
	 */
	private void getserial() {
		// 查詢目前題目的代碼
		serial = SubjectService.getSerial(SurveyGuid, SubjectIndex,
				MultView.this);
	}

	/**
	 * 取得題號
	 */
	private void getSubjectIndex(String TableName) {
		// 取得下一題的題號
		if (TableName.equals("Subject")) {
			String Serial = Integer.parseInt(serial) + 1 + "";

			SubjectIndex = SubjectService.getSubjectIndex(SurveyGuid, Serial,
					MultView.this);
		} else if (TableName.equals("SinglePass")) {
			System.out.println("be SubjectIndex:"+SubjectIndex);
//			if(SubjectIndex.equals("N6a") && SurveyGuid.equals("cddb12bd2907f770829c9ca2617d74af")){
//				SubjectIndex = "N6b";
//			}else if(SubjectIndex.equals("P1a") && SurveyGuid.equals("cddb12bd2907f770829c9ca2617d74af")){
//				SubjectIndex = "QA1";
//			}else if(SubjectIndex.equals("P2a") && SurveyGuid.equals("cddb12bd2907f770829c9ca2617d74af")){
//					SubjectIndex = "QA1";
//			}else {
				SubjectIndex = SinglePsaaService.getSubjectIndex(SurveyGuid,
						SubjectIndex, MultView.this);
//			}
			
			System.out.println("SubjectIndex:"+SubjectIndex);
		}
	}
	
	/**
	 * 取得題號,給單一跳題，且每個選項有獨立的跳題邏輯用的
	 */
	private void getSubjectIndexByAnswer(String TableName, String nAnswer) {
		if("".equals(nAnswer)){
			this.getSubjectIndex(TableName);
		}else {
			SubjectIndex = SinglePsaaService.getSubjectIndexByAnswer(SurveyGuid,
					SubjectIndex, nAnswer, MultView.this);
			System.out.println("SubjectIndex:"+SubjectIndex);
		}
	}

	/**
	 * 下一題
	 * 
	 * @param v
	 */
	public void next(View v) {
		btnNext.setEnabled(false);
		// 檢查是否有正確作答
		boolean nextSubject = true;
		
		for (int i = 0; i < values.size(); i++) {
			if (values.get(i)) {
				break;
			}
			if (i == values.size() - 1) {
				Toast.makeText(MultView.this, "請做答", Toast.LENGTH_LONG).show();
				btnNext.setEnabled(true);
				return;
			}
		}

		// 如果有正確作答的話
		if (nextSubject) {
			AnswerService.deleteAnswer(SurveyGuid, SubjectIndex, SampleID,
					MultView.this);
			for (int i = 0; i < values.size(); i++) {
				if (values.get(i)) {
					AnswerDto answerDto = new AnswerDto();
					answerDto.setSurveyGuid(SurveyGuid);
					answerDto.setInterviewerID(InterviewService
							.getInterviewGuid(MultView.this));
					answerDto.setSampleID(SampleID);
					answerDto.setPrevious(Previous);
					answerDto.setSubjectIndex(SubjectIndex);

					if (listdata.get(i).get("optionIsShowTextView") != null &&
							Integer.parseInt(listdata.get(i).get("optionIsShowTextView")) == 1) {
						answerDto
								.setAnswer(str[Integer.parseInt(index.get(i))]);
					} else {
						answerDto.setAnswer(index.get(i) + "");
					}

					// 儲存本次作答的答案
					AnswerService.cmdInsert(answerDto, MultView.this);
				}
			}

			Previous = SubjectIndex;

			HashMap<String, String> SinglePsaa = SinglePsaaService
					.querySinglePsaaAnswer(SurveyGuid, SubjectIndex,
							MultView.this);
			
			// 檢查是否有符合「單題跳題」的規則
			if (SinglePsaa.size() == 0) {
				getserial();
				getSubjectIndex("Subject");
			} else {
				String isselect = SinglePsaaService.getIsSelect(SurveyGuid,
						SubjectIndex, MultView.this);

				boolean jumpto = false;
				for (int i = 0; i < SinglePsaa.size(); i++) {
					
					int Answer = Integer.parseInt(SinglePsaa.get(i + ""));
					System.out.println("Answer:"+Answer);
					System.out.println("index:"+index.get(0));
					if (isselect.equals("0")) {
						if (!values.get(Answer - 1)) {
//							jumpto = true;
//							break;
							for (int k = 0; k < listdata.size(); k++) {
//								System.out.println("answer:"+listdata.get(k).get("optionValue"));
								System.out.println("index:"+index.get(k)+" "+values.get(k));
								if(Integer.parseInt(listdata.get(k).get("optionValue"))==Answer){
									if(!values.get(k)){
										jumpto = true;
										System.out.println("answer:"+listdata.get(k).get("optionValue")+"  Answer:"+Answer);
									}
									break;
								}
							}
						}
					} else {
						if (values.get(Answer - 1)) {
							for (int k = 0; k < listdata.size(); k++) {
									System.out.println("answer:"+listdata.get(k).get("optionValue"));
									if(Integer.parseInt(listdata.get(k).get("optionValue"))==Answer){
										jumpto = true;
										System.out.println("answer:"+listdata.get(k).get("optionValue")+"  Answer:"+Answer);
										break;
									}
							}
						}
						
					}
				}
		
				if (jumpto) {
					/*
					 * by Awei 2013/04/13
					 * 給單一跳題，且每個選項有獨立的跳題邏輯用的
					 * 改呼叫getSubjectIndexByAnswer，多丟一個目前的答案進去，從DB撈正確的跳題題號給View
					 */
					
					getSubjectIndex("SinglePass");
					

//					String Answer = AnswerService.getAnswer(SurveyGuid,
//							SubjectIndex, SampleID, MultView.this);
//					for (int i = 0; i < values.size(); i++) {
//						if (listdata.get(i).get("optionIsShowTextView") != null
//								&& Integer.parseInt(listdata.get(i).get(
//										"optionIsShowTextView")) == 1) {
//							Answer = "88";
//						}
//					} 
//					getSubjectIndexByAnswer("SinglePass",Answer);	

					/*
					 * Add By Awei 2013/05/23
					 * 如果有跳題，就刪除目前題目之後到跳題題目之前，這中間的所有答案。
					 * */
					String nowSubjectSerial = SubjectService.getSerial(SurveyGuid, Previous, MultView.this);
					String passSubjectSerial = SubjectService.getSerial(SurveyGuid, SubjectIndex, MultView.this);
					DeleteAnsweredService.deleteAnswerAnsweredForSingleAndGroupPass(SurveyGuid, SampleID, nowSubjectSerial
							, passSubjectSerial, MultView.this);
					
				} else {
					getserial();
					getSubjectIndex("Subject");
				}
			}
			System.out.println("ee-SubjectIndex:"+SubjectIndex);
			Intent intent = new Intent();
			intent.putExtra("SurveyGuid", SurveyGuid);
			intent.putExtra("SubjectIndex", SubjectIndex);
			intent.putExtra("Previous", Previous);

			this.setResult(RESULT_OK, intent);

			finish();
			//停止過場動畫
			overridePendingTransition(0, 0);
		}
	}

	public void setMultRow(ListView listview, String SurveyGuid,
			String SubjectIndex) {
		System.out.println("multrow");
		String SubjectIsRandom = SubjectService.getSubjectIsRandom(SurveyGuid,
				SubjectIndex, MultView.this);

		if (Integer.parseInt(SubjectIsRandom) == 0) {
			listdata = OptionService.queryOption(SurveyGuid, SubjectIndex,
					MultView.this);
		} else {
			listdata = OptionService.queryRANDOMOption(SurveyGuid,
					SubjectIndex, MultView.this);
		}

		ArrayList<HashMap<String, String>> answerdata = AnswerService
				.queryAnswer(SurveyGuid, SubjectIndex, SampleID, MultView.this);

		str = new String[100];
		for (int i = 0; i < listdata.size(); i++) {
			values.put(i, false);
		}

		for (int i = 0; i < answerdata.size(); i++) {
			if (answerdata.get(i).get("Answer") != null
					&& answerdata.get(i).get("Answer").trim().equals("")) {
				String a[] = answerdata.get(i).get("Answer").toString()
						.split(":");
				if (a.length >= 2) {
					str[Integer.parseInt(a[1])] = answerdata.get(i).get(
							"Answer");
				}
			}
		}

		if (listdata != null) {
			MultAdapter adapter = new MultAdapter(listdata, answerdata);
			listview.setAdapter(adapter);
		}
	}

	/**
	 * 是否為數字，數字為true，數字為false
	 * 
	 * @param str
	 * @return
	 */
	private boolean StrToInt(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public class MultAdapter extends BaseAdapter implements
			OnCheckedChangeListener, OnFocusChangeListener {
		private ArrayList<HashMap<String, String>> listData;
		private ArrayList<HashMap<String, String>> answerdata;

		private CheckBox[] checkBoxOption;
		private EditText[] multAfeText;
		private TextView[] multTextView;

		public MultAdapter(ArrayList<HashMap<String, String>> listData,
				ArrayList<HashMap<String, String>> answerdata) {
			this.listData = listData;
			this.answerdata = answerdata;
		}

		public MultAdapter(ArrayList<HashMap<String, String>> listData) {
			this.listData = listdata;
			this.answerdata = null;
		}

		public int getCount() {
			return listData.size() / 2 + 1;
		}

		public Object getItem(int position) {
			return listData.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		private void setView(int objectIndex, int value) {
			checkBoxOption[objectIndex].setText((String) listData.get(value)
					.get("optionText"));
			checkBoxOption[objectIndex].setId(value);
			checkBoxOption[objectIndex].setVisibility(View.VISIBLE);

			index.put(value, (String) listData.get(value).get("optionValue"));

			if (listData.get(value).get("optionIsShowTextView") != null
					&& Integer.parseInt(listData.get(value).get(
							"optionIsShowTextView")) == 1) {
				if (listData.get(value).get("optionTextViewPosition").trim()
						.equals("front")) {
				} else {
//					if(objectIndex / 2 == 0){ //讓偶數變奇數
//						objectIndex ++;
//					}
					multAfeText[objectIndex].setVisibility(View.VISIBLE);
					multTextView[objectIndex].setVisibility(View.GONE);
					multAfeText[objectIndex].setId(value);
//					multAfeText[objectIndex].setWidth(0);
//					multTextView[objectIndex].setWidth(0);
				}
			}

			for (int i = 0; i < answerdata.size(); i++) {
				if (answerdata.get(i).get("Answer") != null
						&& !answerdata.get(i).get("Answer").trim().equals("")) {
					if (StrToInt(answerdata.get(i).get("Answer"))) {
						if (Integer.parseInt(answerdata.get(i).get("Answer")) == Integer
								.parseInt(index.get(value))) {
							values.put(value, true);

							checkBoxOption[objectIndex].setChecked(values
									.get(value));
							if (values.get(value)) {
								checkBoxOption[objectIndex]
										.setButtonDrawable(R.drawable.checkbox_selected);
							} else {
								checkBoxOption[objectIndex]
										.setButtonDrawable(R.drawable.checkbox);
							}
						}
					} else {
						if (listData.get(value).get("optionIsShowTextView") != null
								&& Integer.parseInt(listData.get(value).get(
										"optionIsShowTextView")) == 1) {

							String s[] = answerdata.get(i).get("Answer")
									.split(":");

							if (Integer.parseInt(s[1]) == Integer
									.parseInt(index.get(value))) {
								checkBoxOption[objectIndex].setChecked(true);
								checkBoxOption[objectIndex]
										.setButtonDrawable(R.drawable.checkbox_selected);
								if (s.length == 3) {
									multAfeText[objectIndex].setText(s[2]);
								}
								values.put(value, true);
								str[88] = answerdata.get(i).get("Answer");
							}
						}
					}
				}
			}
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			convertView = MultView.this.getLayoutInflater().inflate(
					R.layout.multirow, null);

			checkBoxOption = new CheckBox[2];
			checkBoxOption[0] = (CheckBox) convertView
					.findViewById(R.id.checkBoxOption1);
			checkBoxOption[1] = (CheckBox) convertView
					.findViewById(R.id.checkBoxOption2);

			multAfeText = new EditText[2];
			multAfeText[0] = (EditText) convertView
					.findViewById(R.id.multAfetext1);
			multAfeText[1] = (EditText) convertView
					.findViewById(R.id.multAfetext2);

			multTextView = new TextView[2];
			multTextView[0] = (TextView) convertView
					.findViewById(R.id.multTextView1);
			multTextView[1] = (TextView) convertView
					.findViewById(R.id.multTextView2);

			for (int i = 0; i < 2; i++) {
				if (listdata.size() > position * 2 + i) {
					setView(i, position * 2 + i);
					checkBoxOption[i].setChecked(values.get(position * 2 + i));
					if (values.get(position * 2 + i)) {
						checkBoxOption[i]
								.setButtonDrawable(R.drawable.checkbox_selected);
					} else {
						checkBoxOption[i]
								.setButtonDrawable(R.drawable.checkbox);
					}
					checkBoxOption[i].setOnCheckedChangeListener(this);
					multAfeText[i].addTextChangedListener(textWatcher);
					multAfeText[i].setOnFocusChangeListener(this);
				}
			}

			return convertView;
		}

		int temp = 0;

		TextWatcher textWatcher = new TextWatcher() {

			public void afterTextChanged(Editable arg0) {
				for (int i = 0; i < answerdata.size(); i++) {
					if (!StrToInt(answerdata.get(i).get("Answer"))) {
						answerdata.remove(i);
						break;
					}
				}

				str[temp] = "T:" + temp + ":" + arg0.toString();

				HashMap<String, String> check = new HashMap<String, String>();
				check.put("Answer", "T:" + temp + ":" + arg0.toString());
				answerdata.add(check);
			}

			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}

			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {

			}

		};

		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			CheckBox tempButton = (CheckBox) MultView.this
					.findViewById(buttonView.getId());
			tempButton.setChecked(isChecked);
			if (isChecked) {
				tempButton.setButtonDrawable(R.drawable.checkbox_selected);
			} else {
				tempButton.setButtonDrawable(R.drawable.checkbox);
			}

			for (int i = 0; i < answerdata.size(); i++) {
				if (StrToInt(answerdata.get(i).get("Answer"))) {
					if (Integer.parseInt(answerdata.get(i).get("Answer")) == Integer
							.parseInt(index.get(buttonView.getId()))) {
						answerdata.remove(i);
					}
				} else {
					if (answerdata.get(i).get("Answer") != null
							&& !answerdata.get(i).get("Answer").trim()
									.equals("")) {
						String id[] = answerdata.get(i).get("Answer")
								.toString().split(":");

						if (Integer.parseInt(id[1]) == Integer.parseInt(index
								.get(buttonView.getId()))) {
							answerdata.remove(i);
						}
					}
				}
			}

			values.put(buttonView.getId(), isChecked);
			if (!isChecked) {
				str[buttonView.getId()] = "";
			}
		}

		public void onFocusChange(View v, boolean hasFocus) {
			EditText option = (EditText) v;
			
			/*
			 * By Awei 2013/04/13
			 * 選擇答案的時候會造成當機，註解後就不會當了
			 * */
//			CheckBox tempButton = (CheckBox) MultView.this.findViewById(option
//					.getId());
//			tempButton.setButtonDrawable(R.drawable.checkbox_selected);
//			tempButton.setChecked(true);
			
			values.put(option.getId(), true);

			temp = Integer.parseInt(index.get(option.getId()));
		}
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			this.setResult(RESULT_CANCELED);
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, R.string.Endanswer);
		return super.onCreateOptionsMenu(menu);
	}
}
