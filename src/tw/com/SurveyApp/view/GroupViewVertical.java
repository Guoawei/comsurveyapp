package tw.com.SurveyApp.view;

import java.util.ArrayList;
import java.util.HashMap;

import tw.com.SurveyApp.R;
import tw.com.SurveyApp.dto.AnswerDto;
import tw.com.SurveyApp.service.AnswerService;
import tw.com.SurveyApp.service.GroupOptionService;
import tw.com.SurveyApp.service.InterviewService;
import tw.com.SurveyApp.service.SinglePsaaService;
import tw.com.SurveyApp.service.SubjectService;
import tw.com.SurveyApp.service.TipService;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class GroupViewVertical extends Activity implements OnTouchListener {
	private ListView listview = null;
	private String SurveyGuid = "";
	private String SubjectIndex = "";
	private String SubjectTitle = "";
	private String Previous = "";
	private String serial = "";
	private String SampleID = "";
	private String goGuid = "";
	private ArrayList<HashMap<String, String>> listdata = null;
	private ImageButton btnPrevious, btnNext;
	private ImageButton btnPreviousPage, btnNextPage;
	private ImageButton btnPicture;
	private TextView textTips, textSubjectIndexTitle;
	private HashMap<Integer, Boolean> values = null;
	private HashMap<Integer, String> index = null;
	private HashMap<Integer, String> id = null;
	private boolean title = false;
	private ArrayList<HashMap<String, String>> answerdata;
	private ProgressDialog progressDialog = null;
	private String[] str;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.subject);

		values = new HashMap<Integer, Boolean>();
		index = new HashMap<Integer, String>();
		id = new HashMap<Integer, String>();

		/*
		 * by Awei 2013/04/16
		 * 把題號跟題目合併在一起
		 */
		textSubjectIndexTitle = (TextView) findViewById(R.id.textSubjectIndexTitle);

		btnPrevious = (ImageButton) findViewById(R.id.btnPrevious);
		btnNext = (ImageButton) findViewById(R.id.btnNext);
		btnPreviousPage = (ImageButton) findViewById(R.id.btnPreviousPage);
		btnNextPage = (ImageButton) findViewById(R.id.btnNextPage);

		btnPrevious.setId(R.id.btnPrevious);
		btnNext.setId(R.id.btnNext);
		btnPreviousPage.setId(R.id.btnPreviousPage);
		btnNextPage.setId(R.id.btnNextPage);

		btnNext.setVisibility(View.GONE);

		btnPicture = (ImageButton) findViewById(R.id.btnPicture);
		textTips = (TextView) findViewById(R.id.textTips);

		btnPreviousPage.setVisibility(View.GONE);

		// 處理ListView的資料
		listview = (ListView) findViewById(R.id.listViewFormText);
		listview.setLongClickable(false);
		listview.setClickable(false);

		Bundle extras = getIntent().getExtras();
		SurveyGuid = extras.getString("SurveyGuid");
		SubjectIndex = extras.getString("SubjectIndex");
		SubjectTitle = extras.getString("SubjectTitle");
		Previous = extras.getString("Previous");
		SampleID = extras.getString("SampleID");

		/*
		 * by Awei 2013/04/16
		 * 把題號跟題目合併在一起
		 */
		textSubjectIndexTitle.setText(SubjectIndex+" : "+SubjectTitle);

		// 如果是第一題的話隱藏「上一題」按鈕
		if (Previous.equals("null")) {
			btnPrevious.setEnabled(false);
		}

		LoadData();

		btnPrevious.setOnTouchListener(this);
		btnNext.setOnTouchListener(this);
		btnPreviousPage.setOnTouchListener(this);
		btnNextPage.setOnTouchListener(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		ImageButton ib = (ImageButton) v;
		switch (ib.getId()) {
		case R.id.btnPrevious:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.previousbtn_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.previousbtn);
			}
			break;
		case R.id.btnNext:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.nextbutton_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.nextbutton);
			}
			break;
		case R.id.btnPreviousPage:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.previousbtn_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.previousbtn);
			}
			break;
		case R.id.btnNextPage:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// 更改為按下時的背景圖片
				((ImageButton) v)
						.setBackgroundResource(R.drawable.nextbutton_press);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				// 更改為抬起時的背景圖片
				((ImageButton) v).setBackgroundResource(R.drawable.nextbutton);
			}
			break;
		}
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				|| keyCode == KeyEvent.KEYCODE_HOME) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	final Handler cwjHandler = new Handler();

	final Runnable mUpdateResults = new Runnable() {
		public void run() {
			goGuid = SubjectService.getGroupOption(SurveyGuid, SubjectIndex,
					GroupViewVertical.this);

			ArrayList<HashMap<String, String>> tipdata = TipService
					.queryAllTip(SurveyGuid, SubjectIndex,
							GroupViewVertical.this);

			// 是否有提示卡，有的話顯示提示卡
			if (tipdata.size() == 0) {
				btnPicture.setVisibility(View.GONE);
				textTips.setVisibility(View.GONE);
			} else {
				if (tipdata.get(0).get("tip_image") != null
						&& !tipdata.get(0).get("tip_image").trim().equals("")) {
					textTips.setVisibility(View.GONE);
				} else {
					btnPicture.setVisibility(View.GONE);
					if (tipdata.get(0).get("tip_text") == null
							|| tipdata.get(0).get("tip_text").trim().equals("")) {
						textTips.setVisibility(View.GONE);
					} else {
						textTips.setText(tipdata.get(0).get("tip_text"));
					}
				}
			}

			String type[] = { "0" };

			answerdata = AnswerService.queryAnswer(SurveyGuid, SubjectIndex,
					SampleID, GroupViewVertical.this);

			// 顯示ListView
			setGroupRow(listview, SurveyGuid, goGuid, type);

			listview.setOnItemClickListener(new OnItemClickListener() {

				public void onItemClick(AdapterView<?> arg0, View view,
						int position, long arg3) {

					values.put(position, !values.get(position));
				}

			});
		}
	};

	/**
	 * 載入資料
	 */
	private void LoadData() {
		progressDialog = ProgressDialog.show(GroupViewVertical.this, "載入中....",
				"資料處理中。。。", true);

		new Thread() {
			public void run() {
				progressDialog.dismiss();

				cwjHandler.post(mUpdateResults);
			}
		}.start();
	}

	/**
	 * 儲存答案
	 */
	private void saveAnswer() {
		for (int i = 0; i < 100; i++) {
			if (values.get(i) != null && values.get(i)) {
				if (index.get(i) != null && !index.get(i).trim().equals("")) {
					// 儲存本次作答的答案
					AnswerDto answerDto = new AnswerDto();
					answerDto.setSurveyGuid(SurveyGuid);
					answerDto.setInterviewerID(InterviewService
							.getInterviewGuid(GroupViewVertical.this));
					answerDto.setSampleID(SampleID);
					answerDto.setPrevious(Previous);
					answerDto.setSubjectIndex(SubjectIndex);

					// 目前的寫入答案是選項的DB Index，不是該GroupOption的value值，等調查完後，要修改。
					// By Awei 2012/07/24
					if (str[Integer.parseInt(index.get(i))] != null
							&& !str[Integer.parseInt(index.get(i))].trim()
									.equals("")
							&& !str[Integer.parseInt(index.get(i))].trim()
									.equals("null")) {
						answerDto
								.setAnswer(str[Integer.parseInt(index.get(i))]);
						// System.out.println("下一題："+str[Integer.parseInt(index.get(i))]);
					} else {
						answerDto.setAnswer(id.get(i));
						// System.out.println("else下一題："+id.get(i));
					}

					AnswerService.cmdInsert(answerDto, GroupViewVertical.this);
				}
			}
		}
	}

	/**
	 * 上一頁
	 * 
	 * @param v
	 */
	public void previouspage(View v) {
		String[] newtype = { "0" };

		title = false;

		saveAnswer();

		answerdata = AnswerService.queryAnswer(SurveyGuid, SubjectIndex,
				SampleID, GroupViewVertical.this);

		setGroupRow(listview, SurveyGuid, goGuid, newtype);

		btnNextPage.setVisibility(View.VISIBLE);
		btnNext.setVisibility(View.GONE);
		btnPrevious.setVisibility(View.VISIBLE);
		btnPreviousPage.setVisibility(View.GONE);
	}

	/**
	 * 上一題
	 * 
	 * @param v
	 */
	public void previous(View v) {
		Intent intent = new Intent();
		intent.putExtra("SurveyGuid", SurveyGuid);
		intent.putExtra("SubjectIndex", Previous);

		// 查詢「上一題」的「題號]
		SubjectIndex = AnswerService.getSubjectIndex(Previous, SampleID,
				GroupViewVertical.this);

		intent.putExtra("Previous", SubjectIndex);

		this.setResult(RESULT_OK, intent);

		finish();
	}

	/**
	 * 提示卡圖片
	 * 
	 * @param v
	 */
	public void picture(View v) {
		Intent intent = new Intent();
		intent.putExtra("SurveyGuid", SurveyGuid);
		intent.putExtra("SubjectIndex", SubjectIndex);
		intent.setClass(GroupViewVertical.this, TipImageView.class);
		GroupViewVertical.this.startActivity(intent);
	}

	/**
	 * 取得代碼
	 */
	private void getserial() {
		// 查詢目前題目的代碼
		serial = SubjectService.getSerial(SurveyGuid, SubjectIndex,
				GroupViewVertical.this);
	}

	/**
	 * 取得題號
	 */
	private void getSubjectIndex(String TableName) {
		// 取得下一題的題號
		if (TableName.equals("Subject")) {
			String Serial = Integer.parseInt(serial) + 1 + "";

			SubjectIndex = SubjectService.getSubjectIndex(SurveyGuid, Serial,
					GroupViewVertical.this);
		} else if (TableName.equals("SinglePass")) {
			SubjectIndex = SinglePsaaService.getSubjectIndex(TableName,
					SubjectIndex, GroupViewVertical.this);
		}
	}

	/**
	 * 下一題
	 * 
	 * @param v
	 */
	public void next(View v) {
		// 檢查是否有正確作答
		boolean nextSubject = true;
		for (int i = 0; i < 100; i++) {
			if (values.get(i) != null && values.get(i)) {
				break;
			}
			if (i == values.size() - 1) {
				Toast.makeText(GroupViewVertical.this, "請做答", Toast.LENGTH_LONG)
						.show();
				return;
			}
		}

		// 如果有正確作答的話
		if (nextSubject) {
			saveAnswer();

			Previous = SubjectIndex;

			HashMap<String, String> SinglePsaa = SinglePsaaService
					.querySinglePsaaAnswer(SurveyGuid, SubjectIndex,
							GroupViewVertical.this);

			// 檢查是否有符合「單題跳題」的規則
			if (SinglePsaa.size() == 0) {
				getserial();
				getSubjectIndex("Subject");
			} else {
				String isselect = SinglePsaaService.getIsSelect(SurveyGuid,
						SubjectIndex, GroupViewVertical.this);

				boolean jumpto = true;
				for (int i = 0; i < SinglePsaa.size(); i++) {
					int Answer = Integer.parseInt(SinglePsaa.get(i + ""));
					if (isselect.equals("0")) {
						if (!values.get(Answer - 1)) {
							jumpto = false;
							break;
						}
					} else {
						if (values.get(Answer - 1)) {
							jumpto = false;
							break;
						}
					}
				}

				if (jumpto) {
					getSubjectIndex("SinglePass");
				} else {
					getserial();
					getSubjectIndex("Subject");
				}
			}

			Intent intent = new Intent();
			intent.putExtra("SurveyGuid", SurveyGuid);
			intent.putExtra("SubjectIndex", SubjectIndex);
			intent.putExtra("Previous", Previous);

			this.setResult(RESULT_OK, intent);

			finish();
		}
	}

	/**
	 * 下一頁
	 * 
	 * @param v
	 */
	public void nextpage(View v) {
		boolean nextPage = true;
		for (int i = 0; i < values.size(); i++) {
			if (values.get(i)) {
				break;
			}
			if (i == values.size() - 1) {
				Toast.makeText(GroupViewVertical.this, "請做答", Toast.LENGTH_LONG)
						.show();
				return;
			}
		}

		// 如果有正確作答的話
		if (nextPage) {
			StringBuilder type = new StringBuilder();

			AnswerService.deleteAnswer(SurveyGuid, SubjectIndex, SampleID,
					GroupViewVertical.this);

			StringBuilder ID = new StringBuilder();

			for (int i = 0; i < values.size(); i++) {
				if (values.get(i)) {
					// 儲存本次作答的答案
					AnswerDto answerDto = new AnswerDto();
					answerDto.setSurveyGuid(SurveyGuid);
					answerDto.setInterviewerID(InterviewService
							.getInterviewGuid(GroupViewVertical.this));
					answerDto.setSampleID(SampleID);
					answerDto.setPrevious(Previous);
					answerDto.setSubjectIndex(SubjectIndex);

					if (listdata.size() > i
							&& listdata.get(i).get("isShowTextView") != null
							&& Integer.parseInt(listdata.get(i).get(
									"isShowTextView")) == 1) {
						answerDto.setAnswer(str[i]);
					} else {
						answerDto.setAnswer(id.get(i));
					}

					AnswerService.cmdInsert(answerDto, GroupViewVertical.this);

					ID.append(index.get(i) + ",");
				}
			}

			for (int i = 0; i < values.size(); i++) {
				if (values.get(i)) {
					type.append(index.get(i)).append(",");
				}
			}

			index = new HashMap<Integer, String>();

			String[] newtype = type.toString().split(",");

			setGroupRowNext(listview, SurveyGuid, goGuid, newtype);

			btnNextPage.setVisibility(View.GONE);
			btnNext.setVisibility(View.VISIBLE);
			btnPrevious.setVisibility(View.GONE);
			btnPreviousPage.setVisibility(View.VISIBLE);
		}
	}

	public void setGroupRowNext(ListView listview, String SurveyGuid,
			String goGuid, String[] type) {

		listdata = new ArrayList<HashMap<String, String>>();

		ArrayList<HashMap<String, String>> titledata = new ArrayList<HashMap<String, String>>();

		for (String tt : type) {
			ArrayList<HashMap<String, String>> title = GroupOptionService
					.queryTitle(SurveyGuid, goGuid, tt, GroupViewVertical.this);
			for (int i = 0; i < title.size(); i++) {
				titledata.add(title.get(i));
			}
			ArrayList<HashMap<String, String>> option = GroupOptionService
					.queryOption(SurveyGuid, goGuid, tt, GroupViewVertical.this);

			for (int i = 0; i < option.size(); i++) {
				listdata.add(option.get(i));
			}

			if (option.size() % 2 != 0) {
				for (int i = 0; i < 2 - option.size() % 2; i++) {
					listdata.add(null);
				}
			}
		}

		str = new String[100];

		for (int i = 0; i < answerdata.size(); i++) {
			if (answerdata.get(i).get("Answer") != null
					&& !answerdata.get(i).get("Answer").trim().equals("")) {
				String a[] = answerdata.get(i).get("Answer").toString()
						.split(":");
				if (a.length >= 2) {
					str[Integer.parseInt(a[1])] = answerdata.get(i).get(
							"Answer");
				}
			}
		}

		for (int i = 0; i < listdata.size(); i++) {
			values.put(i, false);
		}

		if (listdata != null) {
			title = true;

			GroupAdapter adapter = new GroupAdapter(listdata, titledata);

			listview.setAdapter(adapter);
		}
	}

	public void setGroupRow(ListView listview, String SurveyGuid,
			String goGuid, String[] type) {

		listdata = GroupOptionService.queryAllGroupOption(SurveyGuid, goGuid,
				type, GroupViewVertical.this);

		str = new String[100];

		for (int i = 0; i < listdata.size(); i++) {
			values.put(i, false);
		}

		for (int i = 0; i < answerdata.size(); i++) {
			if (answerdata.get(i).get("Answer") != null
					&& !answerdata.get(i).get("Answer").trim().equals("")) {
				String a[] = answerdata.get(i).get("Answer").toString()
						.split(":");
				if (a.length >= 2) {
					if (str.length >= Integer.parseInt(a[1])) {
						str[Integer.parseInt(a[1])] = answerdata.get(i).get(
								"Answer");
					}
				}
			}
		}

		if (listdata != null) {
			GroupAdapter adapter = new GroupAdapter(listdata, answerdata);
			listview.setAdapter(adapter);
		}

	}

	/**
	 * 是否為數字，數字為true，數字為false
	 * 
	 * @param str
	 * @return
	 */
	private boolean StrToInt(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public class GroupAdapter extends BaseAdapter implements
			OnCheckedChangeListener, OnFocusChangeListener {
		private ArrayList<HashMap<String, String>> listData;
		private CheckBox groupCheckBox;
		private EditText groupText;
		private TextView typetitle;
		private LinearLayout titleLayout;
		private ArrayList<HashMap<String, String>> titledata;
		private int t = 0;

		public GroupAdapter(ArrayList<HashMap<String, String>> listData,
				ArrayList<HashMap<String, String>> titledata) {
			this.listData = listData;
			this.titledata = titledata;
		}

		public GroupAdapter(ArrayList<HashMap<String, String>> listData) {
			this.listData = listdata;
		}

		public int getCount() {
			return listData.size();
		}

		public Object getItem(int position) {
			return listData.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		private void setGroupTitle(int value) {
			titleLayout.setVisibility(View.VISIBLE);

			String GroupType = (String) listData.get(value).get("GroupType");

			if (t != Integer.parseInt(GroupType)) {
				t = Integer.parseInt(GroupType);

				for (int i = 0; i < titledata.size(); i++) {
					if (Integer.parseInt(titledata.get(i).get("GroupValue")) == t) {
						typetitle.setText((String) titledata.get(i).get(
								"GroupText"));
						break;
					}
				}
			}
		}

		private void setView(int objectIndex, int value) {

			if (listdata.get(value) == null
					|| listdata.get(value).get("GroupValue") == null) {
				return;
			}

			int GroupValue = Integer.parseInt(listdata.get(value).get(
					"GroupValue"));
			String GroupText = (String) listData.get(value).get("GroupText");
			String GroupId = (String) listData.get(value).get("GroupId");

			if (title) {
				setGroupTitle(value);
			}

			groupCheckBox.setId(value);
			groupCheckBox.setVisibility(View.VISIBLE);
			groupCheckBox.setText(GroupText);

			if (listData.get(value).get("isShowTextView") != null
					&& Integer.parseInt(listData.get(value).get(
							"isShowTextView")) == 1) {
				if (listData.get(value).get("textViewPosition").trim()
						.equals("front")) {

				} else {
					groupText.setId(value);
					groupText.setVisibility(View.VISIBLE);
				}
			}

			id.put(value, GroupId);
			index.put(value, GroupValue + "");

			for (int i = 0; i < answerdata.size(); i++) {
				if (answerdata.get(i).get("Answer") != null
						&& !answerdata.get(i).get("Answer").trim().equals("")) {

					if (StrToInt(answerdata.get(i).get("Answer"))) {

						if (id.get(value) != null
								&& Integer.parseInt(answerdata.get(i).get(
										"Answer")) == Integer.parseInt(id
										.get(value))) {
							values.put(value, true);

							groupCheckBox.setChecked(values.get(value));
							if (values.get(value)) {
								groupCheckBox
										.setButtonDrawable(R.drawable.checkbox_selected);
							} else {
								groupCheckBox
										.setButtonDrawable(R.drawable.checkbox);
							}
						}
					} else {
						if (listData.get(value).get("optionIsShowTextView") != null
								&& Integer.parseInt(listData.get(value).get(
										"optionIsShowTextView")) == 1) {

							String s[] = answerdata.get(i).get("Answer")
									.split(":");

							if (Integer.parseInt(s[1]) == groupCheckBox.getId()) {
								groupCheckBox.setChecked(true);
								groupCheckBox
										.setButtonDrawable(R.drawable.checkbox_selected);
								if (s.length == 3) {
									groupText.setText(s[2]);
								}
								values.put(Integer.parseInt(s[1]), true);
							}
						}
					}
				}
			}

			groupCheckBox.setChecked(values.get(value));
			if (values.get(value)) {
				groupCheckBox.setButtonDrawable(R.drawable.checkbox_selected);
			} else {
				groupCheckBox.setButtonDrawable(R.drawable.checkbox);
			}
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			convertView = GroupViewVertical.this.getLayoutInflater().inflate(
					R.layout.grouprowvertical, null);

			groupCheckBox = (CheckBox) convertView
					.findViewById(R.id.groupCheckBox1);

			groupText = (EditText) convertView.findViewById(R.id.groupText1);

			typetitle = (TextView) convertView.findViewById(R.id.typetitle);

			titleLayout = (LinearLayout) convertView.findViewById(R.id.title);

			if (position < listdata.size()) {
				setView(0, position);
			}

			groupCheckBox.setOnCheckedChangeListener(this);
			groupText.addTextChangedListener(textWatcher);
			groupText.setOnFocusChangeListener(this);

			return convertView;
		}

		int temp = 0;

		TextWatcher textWatcher = new TextWatcher() {

			public void afterTextChanged(Editable arg0) {
				for (int i = 0; i < answerdata.size(); i++) {
					if (!StrToInt(answerdata.get(i).get("Answer"))) {
						answerdata.remove(i);
						break;
					}
				}

				str[temp] = "T:" + temp + ":" + arg0.toString();
				HashMap<String, String> check = new HashMap<String, String>();
				check.put("Answer", "T:" + temp + ":" + arg0.toString());
				answerdata.add(check);
			}

			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
			}

		};

		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			CheckBox tempButton = (CheckBox) GroupViewVertical.this
					.findViewById(buttonView.getId());

			if (tempButton == null) {
				return;
			}

			tempButton.setChecked(isChecked);
			if (isChecked) {
				tempButton.setButtonDrawable(R.drawable.checkbox_selected);
			} else {
				tempButton.setButtonDrawable(R.drawable.checkbox);
			}

			for (int i = 0; i < answerdata.size(); i++) {
				if (StrToInt(answerdata.get(i).get("Answer"))) {
					if (Integer.parseInt(answerdata.get(i).get("Answer")) == Integer
							.parseInt(index.get(buttonView.getId()))) {
						answerdata.remove(i);
						break;
					}
				} else {

					String id[] = answerdata.get(i).get("Answer").toString()
							.split(":");

					if (Integer.parseInt(id[1]) == Integer.parseInt(index
							.get(buttonView.getId()))) {
						answerdata.remove(i);
						break;
					}
				}
			}

			values.put(buttonView.getId(), isChecked);
		}

		public void onFocusChange(View v, boolean hasFocus) {
			EditText option = (EditText) v;
			CheckBox tempButton = (CheckBox) GroupViewVertical.this
					.findViewById(option.getId());
			tempButton.setChecked(true);
			tempButton.setButtonDrawable(R.drawable.checkbox_selected);

			values.put(option.getId(), true);
			temp = Integer.parseInt(index.get(option.getId()));
		}
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			this.setResult(RESULT_CANCELED);
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, R.string.Endanswer);
		return super.onCreateOptionsMenu(menu);
	}
}
