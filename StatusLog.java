package tw.com.SurveyApp.view;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import tw.com.SurveyApp.R;
import tw.com.SurveyApp.dto.StatusDataDto;
import tw.com.SurveyApp.service.StatusService;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class StatusLog extends Activity {
	private ListView listview = null;
	private ProgressDialog progressDialog = null;
	private ArrayList<HashMap<String, String>> listdata = null;
	private String Step = "0";
	private String SurveyGuid, SampleID;
	private static final int START_VIEW = 0;
	private String Explain = "";
	private Button backbtn, statusBtn;
	private EditText statusText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.statuslog);

		backbtn = (Button) findViewById(R.id.backbtn);
		backbtn.setVisibility(View.GONE);

		statusBtn = (Button) findViewById(R.id.statusBtn);
		statusText = (EditText) findViewById(R.id.statusText);
		statusBtn.setVisibility(View.GONE);
		statusText.setVisibility(View.GONE);

		statusText.addTextChangedListener(textWatcher);

		Bundle extras = getIntent().getExtras();
		SurveyGuid = extras.getString("SurveyGuid");
		SampleID = extras.getString("SampleGuid");

		LoadData();
	}

	TextWatcher textWatcher = new TextWatcher() {

		public void afterTextChanged(Editable arg0) {
			Explain = arg0.toString();
		}

		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
		}

		public void onTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
		}

	};

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				|| keyCode == KeyEvent.KEYCODE_HOME) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	final Handler cwjHandler = new Handler();

	final Runnable mUpdateResults = new Runnable() {
		public void run() {
			// 處理ListView的資料
			listview = (ListView) findViewById(R.id.statusloglistView);
			// 顯示ListView
			setCompletedListView(listview);
		}
	};

	/**
	 * 載入資料
	 */
	private void LoadData() {
		progressDialog = ProgressDialog.show(StatusLog.this, "載入中....",
				"資料處理中。。。", true);

		new Thread() {
			public void run() {
				progressDialog.dismiss();

				cwjHandler.post(mUpdateResults);
			}
		}.start();
	}

	public void setCompletedListView(ListView listview) {

		// 顯示ListView
		setCompletedRow(listview);

		// 建立ListView的Click事件
		listview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				HashMap<String, String> single = listdata.get(position);

				Step = single.get("StatusNumber");

				String StatusNumber = single.get("StatusNumber");

				StatusDataDto statusdataDto = new StatusDataDto();

				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat df = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				
				if (single.get("Step").equals("0")) {
					if (StatusNumber.equals("1")) {
						statusdataDto.setSampleGuid(SampleID);
						statusdataDto.setSurveyGuid(SurveyGuid);
						statusdataDto.setSituationCode(StatusNumber);
						statusdataDto.setCDateTime(Explain);
						statusdataDto.setCDateTime(df.format(calendar.getTime()));

						StatusService.cmdInsertData(statusdataDto,
								StatusLog.this);

						Intent intent = new Intent();
						intent.setClass(StatusLog.this, StartView.class);
						intent.putExtra("SurveyGuid", SurveyGuid);
						intent.putExtra("SampleGuid", SampleID);
						startActivityForResult(intent, START_VIEW);
					} else if(StatusNumber.equals("6")){
						statusBtn.setVisibility(View.VISIBLE);
						statusText.setVisibility(View.VISIBLE);
						backbtn.setVisibility(View.VISIBLE);
						LoadData();
					} else {
						backbtn.setVisibility(View.VISIBLE);
						LoadData();
					}
				} else {
					if (!StatusNumber.equals("98")) {
						statusdataDto.setSampleGuid(SampleID);
						statusdataDto.setSurveyGuid(SurveyGuid);
						statusdataDto.setSituationCode(StatusNumber);
						statusdataDto.setComment("");
						statusdataDto.setCDateTime(df.format(calendar.getTime()));

						StatusService.cmdInsertData(statusdataDto,
								StatusLog.this);

						finish();
					}
				}

			}
		});
	}

	/**
	 * 反回第一層
	 * 
	 * @param v
	 */
	public void btnback(View v) {
		Step = "0";
		backbtn.setVisibility(View.GONE);

		statusBtn.setVisibility(View.GONE);
		statusText.setVisibility(View.GONE);

		LoadData();
	}

	/**
	 * 寫入說明
	 * 
	 * @param v
	 */
	public void status(View v) {
		StatusDataDto statusdataDto = new StatusDataDto();

		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		statusdataDto.setSampleGuid(SampleID);
		statusdataDto.setSurveyGuid(SurveyGuid);
		statusdataDto.setSituationCode("98");
		statusdataDto.setComment(Explain);
		statusdataDto.setCDateTime(df.format(calendar.getTime()));

		StatusService.cmdInsertData(statusdataDto, StatusLog.this);

		finish();
	}

	/**
	 * 顯示ListView
	 * 
	 * @param context
	 * @param listview
	 * @param QuestionarireID
	 * @return
	 */
	public HashMap<Integer, String> setCompletedRow(ListView listview) {
		// 查詢「狀態」資料
		listdata = new StatusService().queryStatus(Step, StatusLog.this);

		HashMap<Integer, String> values = null;

		if (listdata != null) {
			CompletedAdapter adapter = new CompletedAdapter(StatusLog.this,
					listdata);
			listview.setAdapter(adapter);
			values = adapter.values;
			return values;
		}
		return values;
	}

	public class CompletedAdapter extends BaseAdapter {
		Context context;
		ArrayList<HashMap<String, String>> listData;
		HashMap<Integer, String> values = new HashMap<Integer, String>();
		EditText statusText;

		public CompletedAdapter(Context context,
				ArrayList<HashMap<String, String>> listData) {
			this.context = context;
			this.listData = listData;
		}

		/**
		 * 取得資料筆數
		 */
		public int getCount() {
			return listData.size();
		}

		/**
		 * 取得資料列
		 */
		public Object getItem(int position) {
			return listData.get(position);
		}

		/**
		 * 取得資料ID
		 */
		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			LayoutInflater mInflater = LayoutInflater.from(context);

			convertView = mInflater.inflate(R.layout.statuslogrow, null);

			// 狀態編號
			TextView textStatusNumber = (TextView) convertView
					.findViewById(R.id.textStatusNumber);
			// 狀態內容
			TextView textName = (TextView) convertView
					.findViewById(R.id.textName);

			if (listData.get(position).get("StatusNumber").equals("")) {
				textStatusNumber.setText("98");
			} else {
				textStatusNumber.setText((String) listData.get(position).get(
						"StatusNumber"));
			}

			if (listData.get(position).get("Name").equals("")) {
				textName.setText("其他無法歸類之原因，請說明");
			} else {
				textName.setText((String) listData.get(position).get("Name"));
			}

			return convertView;
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == START_VIEW) {
			if (resultCode == RESULT_OK) {
				finish();
			}
		}
	}

}
